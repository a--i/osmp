﻿CREATE TABLE T_PASSPORT
(
  ID           VARCHAR(32) PRIMARY KEY NOT NULL ,
  STATE        NUMERIC(10) NOT NULL,
  ACTIONDATE   DATE        NOT NULL,
  ACTIONUSERID VARCHAR(32) NOT NULL,
  REFCOUNT     NUMERIC(10) NOT NULL,
  ISSUEPLACE   VARCHAR(4000),
  PASSNUMBER  VARCHAR(20),
  ISSUEDATE    DATE,
  PASSSERIES   VARCHAR(20),
  VERSIONID    NUMERIC(10) NOT NULL,
  STORAGEID    VARCHAR(32) NOT NULL,
  CREATEDATE   DATE        NOT NULL,
  CREATEUSERID VARCHAR(32) NOT NULL,
  DOCTYPE      VARCHAR(32) REFERENCES VDENUMITEMS (ID),
  DATETO       DATE
);