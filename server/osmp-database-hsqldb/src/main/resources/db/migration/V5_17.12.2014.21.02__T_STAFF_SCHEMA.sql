﻿CREATE TABLE T_STAFF
(
  ID                   VARCHAR(32),
  STATE                NUMERIC(10)               NOT NULL,
  ACTIONDATE           DATE                     NOT NULL,
  ACTIONUSERID         VARCHAR(32)              NOT NULL,
  REFCOUNT             NUMERIC(10)               NOT NULL,
  DEPARTMENT_STAFFS_1  VARCHAR(32),
  ISMEDICALRECORDHIDE  NUMERIC(2),
  VERSIONID            NUMERIC(10)               NOT NULL,
  STORAGEID            VARCHAR(32)              NOT NULL,
  CREATEDATE           DATE                     NOT NULL,
  CREATEUSERID         VARCHAR(32)              NOT NULL,
  CODE                 VARCHAR(255),
  POSITION_STAFFS_1    VARCHAR(32),
  INTERVAL             NUMERIC(22)
)