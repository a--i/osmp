CREATE TABLE T_POSITION
(
  ID                VARCHAR(32),
  STATE             NUMERIC(10)                  NOT NULL,
  ACTIONDATE        DATE                        NOT NULL,
  ACTIONUSERID      VARCHAR(32)                 NOT NULL,
  VERSIONID         NUMERIC(10)                  NOT NULL,
  STORAGEID         VARCHAR(32)                 NOT NULL,
  CREATEDATE        DATE                        NOT NULL,
  CREATEUSERID      VARCHAR(32)                 NOT NULL,
  REFCOUNT          NUMERIC(10)                  NOT NULL,
  POSITIONCATEGORY  VARCHAR(32),
  POSITIONNAME      VARCHAR(255)
)
