package mis.osmp.database.migration.config;

import mis.osmp.database.migration.DatabaseMigration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class DatabaseMigrationConfiguration {
  @Bean
  public DatabaseMigration databaseMigration() {
    return new DatabaseMigration();
  }
}
