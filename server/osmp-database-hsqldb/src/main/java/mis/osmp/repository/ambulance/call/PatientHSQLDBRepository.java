package mis.osmp.repository.ambulance.call;

import mis.osmp.repository.OSMPNamedJdbcTemplateRepository;
import mis.osmp.repository.ambulance.call.dto.PatientDTO;
import mis.osmp.repository.ambulance.call.filter.PatientFilter;
import mis.osmp.repository.map.OSMPBeanPropertyRowMapper;
import mis.osmp.repository.pagination.Page;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;

@Repository
public class PatientHSQLDBRepository extends OSMPNamedJdbcTemplateRepository implements PatientRepository {

  @Override
  public List<PatientDTO> getPatients(PatientFilter filter, Page page) {

    final NamedParameterJdbcTemplate jdbcTemplate = getJdbcTemplate();
    final int startIndex = page.getPageNumber() * page.getRecordsOnPage();
    final HashMap<String, Object> paramMap = new HashMap<>();
    paramMap.put("startIndex", startIndex);
    paramMap.put("pageSize", page.getRecordsOnPage());
    String query = "select pat.*," +
        " p.ID as personDto_id, " +
        " p.FIRSTNAME as personDto_firstName, " +
        " p.LASTNAME as personDto_lastName, " +
        " p.MIDDLENAME as personDto_middleName, " +
        " p.BIRTHDATE as personDto_birthdate, " +
        " p.SEX as personDto_sex, " +
        " p.mobilephone as personDto_mobilephone, " +
        " p.homephone as personDto_homephone, " +
        " p.workphone as personDto_workphone, " +
        " p.email as personDto_email, " +
        " doc.id as personDto_passportDto_id, " +
        " doc.PASSSERIES as personDto_passportDto_passSeries, " +
        " doc.PASSNUMBER as personDto_passportDto_passNumber, " +
        " doc.issuePlace as personDto_passportDto_issuePlace, " +
        " doc.issueDate as personDto_passportDto_issueDate, " +
        " doc.doctype as personDto_passportDto_doctype, " +
        " doc.dateTo as personDto_passportDto_dateTo, " +
        " doc1.id as privilegeDocumentDto_id, " +
        " doc1.PASSSERIES as privilegeDocumentDto_passSeries, " +
        " doc1.PASSNUMBER as privilegeDocumentDto_passNumber, " +
        " doc1.issuePlace as privilegeDocumentDto_issuePlace, " +
        " doc1.issueDate as privilegeDocumentDto_issueDate, " +
        " doc1.doctype as privilegeDocumentDto_doctype, " +
        " doc1.dateTo as privilegeDocumentDto_dateTo " +
        " from" +
        "      T_PATIENT       pat " +
        " LEFT JOIN T_PERSON p ON p.ID = pat.PERSON" +
        " LEFT JOIN T_PASSPORT doc ON doc.ID = p.PASSPORT" +
        " LEFT JOIN T_PASSPORT doc1 ON doc1.ID = pat.PRIVILEGEDOCUMENT " +
        "  where 1=1";

    if (!StringUtils.isEmpty(filter.getFirstName())) {
      query += " and UPPER(p.Firstname) like :firstname";
      paramMap.put("firstname", "%" + filter.getFirstName().toUpperCase() + "%");
    }

    if (!StringUtils.isEmpty(filter.getLastName())) {
      query += " and UPPER(p.Lastname) like :lastname";
      paramMap.put("lastname", "%" + filter.getLastName().toUpperCase() + "%");
    }

    if (!StringUtils.isEmpty(filter.getMiddleName())) {
      query += " and UPPER(p.Middlename) like :middlename";
      paramMap.put("middlename", "%" + filter.getMiddleName().toUpperCase() + "%");
    }

    if (page.getSort() != null && !StringUtils.isEmpty(page.getSort().getFieldName())) {
      String[] fields = page.getSort().getFieldName().split(",");
      String[] ord = page.getSort().getOrder().split(",");
      String order = null;

      for (int i = 0; i < fields.length; i++) {
        if (StringUtils.isEmpty(order)) {
          order = " order by ";
        }

        order += fields[i] + " ";
        if (i < ord.length && ord[i].equals("desc")) {
          order += "desc, ";
        } else {
          order += "asc, ";
        }
      }

      if (order != null) {
        order = order.substring(0, order.length() - 2);
        query += order;
      }
    }

    query += " LIMIT :pageSize OFFSET :startIndex";

    return jdbcTemplate.query(query, paramMap, new OSMPBeanPropertyRowMapper<>(PatientDTO.class));
  }

  @Override
  public PatientDTO getPatient(String id) {
    final NamedParameterJdbcTemplate jdbcTemplate = getJdbcTemplate();
    final HashMap<String, Object> paramMap = new HashMap<>();
    paramMap.put("id", id);
    String query = "select pat.*," +
        " p.ID as personDto_id, " +
        " p.FIRSTNAME as personDto_firstName, " +
        " p.LASTNAME as personDto_lastName, " +
        " p.MIDDLENAME as personDto_middleName, " +
        " p.BIRTHDATE as personDto_birthdate, " +
        " p.SEX as personDto_sex, " +
        " p.mobilephone as personDto_mobilephone, " +
        " p.homephone as personDto_homephone, " +
        " p.workphone as personDto_workphone, " +
        " p.email as personDto_email, " +
        " doc.id as personDto_passportDto_id, " +
        " doc.PASSSERIES as personDto_passportDto_passSeries, " +
        " doc.PASSNUMBER as personDto_passportDto_passNumber, " +
        " doc.issuePlace as personDto_passportDto_issuePlace, " +
        " doc.issueDate as personDto_passportDto_issueDate, " +
        " doc.doctype as personDto_passportDto_doctype, " +
        " doc.dateTo as personDto_passportDto_dateTo " +
        " from" +
        "      T_PATIENT       pat " +
        " LEFT JOIN T_PERSON p ON p.ID = pat.PERSON" +
        " LEFT JOIN T_PASSPORT doc ON doc.ID = p.PASSPORT" +
        " WHERE pat.Id = :id";


    return jdbcTemplate.queryForObject(query, paramMap, new OSMPBeanPropertyRowMapper<>(PatientDTO.class));
  }
}
