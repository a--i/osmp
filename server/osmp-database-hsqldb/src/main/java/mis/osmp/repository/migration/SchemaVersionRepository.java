package mis.osmp.repository.migration;


import mis.osmp.repository.OSMPNamedJdbcTemplateRepository;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

@Repository
public class SchemaVersionRepository extends OSMPNamedJdbcTemplateRepository {
  public List<String> getExecutedScriptNames() {
    final NamedParameterJdbcTemplate jdbcTemplate = getJdbcTemplate();
    final String query = "SELECT \"script\" FROM \"schema_version\" WHERE \"success\"=1";
    try {
      return jdbcTemplate.queryForList(query, Collections.emptyMap(), String.class);
    } catch (Exception e) {
      return Collections.emptyList();
    }
  }
}
