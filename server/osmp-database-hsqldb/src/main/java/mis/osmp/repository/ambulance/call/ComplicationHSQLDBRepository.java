package mis.osmp.repository.ambulance.call;

import mis.osmp.repository.OSMPNamedJdbcTemplateRepository;
import mis.osmp.repository.ambulance.call.dto.ComplicationDTO;
import mis.osmp.repository.ambulance.call.filter.ComplicationFilter;
import mis.osmp.repository.map.OSMPBeanPropertyRowMapper;
import mis.osmp.repository.pagination.Page;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;

@Repository
public class ComplicationHSQLDBRepository extends OSMPNamedJdbcTemplateRepository implements ComplicationRepository {

  @Override
  public List<ComplicationDTO> getComplications(ComplicationFilter filter, Page page) {

    final NamedParameterJdbcTemplate jdbcTemplate = getJdbcTemplate();
    final int startIndex = page.getPageNumber() * page.getRecordsOnPage();
    final HashMap<String, Object> paramMap = new HashMap<>();
    paramMap.put("startIndex", startIndex);
    paramMap.put("pageSize", page.getRecordsOnPage());
    String query = "select c.*" +
        " from T_COMPLICATION  c " +
        "  where 1=1";

    if (!StringUtils.isEmpty(filter.getName())) {
      query += " and UPPER(c.Name) like :name";
      paramMap.put("name", "%" + filter.getName().toUpperCase() + "%");
    }

    query += " ORDER BY c.NAME " +
        "LIMIT :pageSize OFFSET :startIndex";

    return jdbcTemplate.query(query, paramMap, new OSMPBeanPropertyRowMapper<>(ComplicationDTO.class));
  }

  @Override
  public ComplicationDTO getComplication(String id) {
    final NamedParameterJdbcTemplate jdbcTemplate = getJdbcTemplate();
    final HashMap<String, Object> paramMap = new HashMap<>();
    paramMap.put("id", id);
    String query = "SELECT c.*" +
        " FROM" +
        "      T_COMPLICATION  c " +
        " WHERE c.Id = :id";

    return jdbcTemplate.queryForObject(query, paramMap, new OSMPBeanPropertyRowMapper<>(ComplicationDTO.class));
  }
}
