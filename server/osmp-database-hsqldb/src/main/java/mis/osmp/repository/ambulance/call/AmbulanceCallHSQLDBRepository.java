package mis.osmp.repository.ambulance.call;

import mis.osmp.repository.OSMPNamedJdbcTemplateRepository;
import mis.osmp.repository.ambulance.call.dto.AmbulanceCallCreatePackageDTO;
import mis.osmp.repository.ambulance.call.dto.AmbulanceCallDTO;
import mis.osmp.repository.ambulance.call.dto.AmbulanceCallRowDTO;
import mis.osmp.repository.ambulance.call.filter.AmbulanceCallsFilter;
import mis.osmp.repository.map.OSMPBeanPropertyRowMapper;
import mis.osmp.repository.map.OSMPInsertRowBeanPropertyMapper;
import mis.osmp.repository.map.OSMPUpdateRowBeanPropertyMapper;
import mis.osmp.repository.pagination.Page;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Repository
public class AmbulanceCallHSQLDBRepository extends OSMPNamedJdbcTemplateRepository implements AmbulanceCallRepository {

  @Override
  public List<AmbulanceCallDTO> getCalls(AmbulanceCallsFilter filter, Page page) {

    final NamedParameterJdbcTemplate jdbcTemplate = getJdbcTemplate();
    final int startIndex = page.getPageNumber() * page.getRecordsOnPage();
    final HashMap<String, Object> paramMap = new HashMap<>();
    paramMap.put("startIndex", startIndex);
    paramMap.put("pageSize", page.getRecordsOnPage());
    String query = "select " +
        "      amb.id                      id, " +
        "      amb.createuserid            userid, " +
        "      amb.createuserid            rwuserid, " +
        "      amb.state                   state, " +
        "      amb.code                    code, " +
        "      amb.calldatetime            calldatetime, " +
        "      amb.callpassdatetime        callpassdatetime, " +
        "      amb.departuredatetime       departuredatetime, " +
        "      amb.patientarrivaldatetime  patientarrivaldatetime, " +
        "      amb.departurereturndatetime departurereturndatetime, " +
        "      ptn.unidentified            unidentified, " +
        "      prs.lastname + ' ' + prs.firstname + ' ' + prs.middlename + ' (' + to_char(prs.birthdate, 'DD.MM.YYYY') + ' г.р.)'   patient, " +
        "      prs.lastname                lastname, " +
        "      prs.firstname               firstname, " +
        "      prs.middlename              middlename, " +
        "      amb.callreason              callreason, " +
        "      amb.contactphone            contactphone, " +
        "      vd_ctp.name                 calltype ," +
        "      vd_cst.name                 callstatus, " +
        "      vd_num.name                 departureteamnumber, " +
        "      vd_ptp.name                 callpasstype, " +
        "      res.id                      resultId, " +
        "      res.id                      rwresultId, " +
        "      res.name                    result, " +
        "      mkb.id                      diagnosisId, " +
        "      mkb.id                      rwdiagnosisId, " +
        "      mkb.diagnosename            diagnosis, " +
        "      mkb.code                    mkbcode, " +
        "      amb.hospitalization         hospitalization, " +
        "      amb.patientwalking          patientwalking, " +
        "      amb.signature               sgnId " +
        " from" +
        "      t_ambulancecall       amb " +
        "    left join t_ambulancecallresult res on amb.ambulancecallresult_a_1 = res.id " +
        "    left join t_refmkbdiagnosis     mkb on amb.diagnosismkb            = mkb.id " +
        "    left join t_patient             ptn on amb.patient_ambulancecall_1 = ptn.id " +
        "    left join t_person              prs on ptn.person                  = prs.id " +
        "    left join t_staffdesignation    std on amb.staffdesignation_ambu_1 = std.id " +
        "    left join vdenumitems        vd_ctp on amb.calltype                = vd_ctp.id " +
        "    left join vdenumitems        vd_cst on amb.callstatus              = vd_cst.id " +
        "    left join vdenumitems        vd_num on amb.departureteamnumber     = vd_num.id " +
        "    left join vdenumitems        vd_ptp on amb.callpasstype            = vd_ptp.id " +
        "  where 1=1";

    if (!StringUtils.isEmpty(filter.getCode())) {
      query += " and UPPER(amb.code) like :code";
      paramMap.put("code", "%" + filter.getCode().toUpperCase() + "%");
    }

    if (!StringUtils.isEmpty(filter.getSignatureStatus())) {
      if (filter.getSignatureStatus().equals("0")) {
        query += " and amb.SIGNATURE is null";
      }

      if (filter.getSignatureStatus().equals("1")) {
        query += " and amb.SIGNATURE is not null";
      }
    }

    if (!StringUtils.isEmpty(filter.getFirstName())) {
      query += " and UPPER(prs.Firstname) like :firstname";
      paramMap.put("firstname", "%" + filter.getFirstName().toUpperCase() + "%");
    }

    if (!StringUtils.isEmpty(filter.getLastName())) {
      query += " and UPPER(prs.Lastname) like :lastname";
      paramMap.put("lastname", "%" + filter.getLastName().toUpperCase() + "%");
    }

    if (!StringUtils.isEmpty(filter.getMiddleName())) {
      query += " and UPPER(prs.Middlename) like :middlename";
      paramMap.put("middlename", "%" + filter.getMiddleName().toUpperCase() + "%");
    }

    if (!StringUtils.isEmpty(filter.getDateFrom())) {
      query += " and amb.CALLDATETIME >= :dateFrom";
      paramMap.put("dateFrom", filter.getDateFrom());
    }

    if (!StringUtils.isEmpty(filter.getDateTo())) {
      query += " and amb.CALLDATETIME <= :dateTo";
      paramMap.put("dateTo", filter.getDateTo());
    }

    if (!StringUtils.isEmpty(filter.getAmbulanceCallResult())) {
      query += " and UPPER(amb.AMBULANCECALLRESULT_A_1) = :ambulanceCallResult";
      paramMap.put("ambulanceCallResult", filter.getAmbulanceCallResult().toUpperCase());
    }

    if (page.getSort() != null && !StringUtils.isEmpty(page.getSort().getFieldName())) {
      String[] fields = page.getSort().getFieldName().split(",");
      String[] ord = page.getSort().getOrder().split(",");
      String order = null;

      for (int i = 0; i < fields.length; i++) {
        if (StringUtils.isEmpty(order)) {
          order = " order by ";
        }

        order += fields[i] + " ";
        if (i < ord.length && ord[i].equals("desc")) {
          order += "desc nulls last, ";
        } else {
          order += "asc nulls first, ";
        }
      }

      if (order != null) {
        order = order.substring(0, order.length() - 2);
        query += order;
      }
    }

    query += " LIMIT :pageSize OFFSET :startIndex";
    List<AmbulanceCallDTO> res = jdbcTemplate.query(query, paramMap, new OSMPBeanPropertyRowMapper<>(AmbulanceCallDTO.class));
    return res;
  }

  @Override
  @Transactional
  public void updateCall(final AmbulanceCallRowDTO ambulanceCallPackage) {

    final NamedParameterJdbcTemplate jdbcTemplate = getJdbcTemplate();

    final HashMap<String, String> map = createColumnMap();
    map.put("id", null);
    final OSMPUpdateRowBeanPropertyMapper<AmbulanceCallRowDTO> sqlMapper = new OSMPUpdateRowBeanPropertyMapper<>("T_AMBULANCECALL", ambulanceCallPackage, map);
    String query = sqlMapper.createUpdateSqlWithNamedParameters().toString();
    query += " where id=:id";
    jdbcTemplate.update(query, new BeanPropertySqlParameterSource(ambulanceCallPackage));

    final String delComplQry = "delete from LINK_T_AMBULANCECALL_T_COMPLICATION where DOCAID = :ambId";
    final HashMap<String, Object> paramMap2 = new HashMap() {{
      put("ambId", ambulanceCallPackage.getId());
    }};
    jdbcTemplate.update(delComplQry, paramMap2);

    if (!CollectionUtils.isEmpty(ambulanceCallPackage.getComplicationList())) {

      saveComplications(ambulanceCallPackage.getId(), ambulanceCallPackage.getComplicationList());

    }

  }

  void saveComplications(final String id, final List<String> complications) {
    final NamedParameterJdbcTemplate jdbcTemplate = getJdbcTemplate();
    final String complicationQuery = "insert into LINK_T_AMBULANCECALL_T_COMPLICATION " +
        "(DOCAID, DOCBID) values (:ambId, :cId)";

    final HashMap<String, Object> paramMap2 = new HashMap() {{
      put("ambId", id);
    }};
    for (String compl : complications) {

      if (paramMap2.containsKey("cId"))
        paramMap2.replace("cId", compl);
      else
        paramMap2.put("cId", compl);
      jdbcTemplate.update(complicationQuery, paramMap2);
    }


  }

  HashMap<String, String> createColumnMap() {
    final HashMap<String, String> columnMap = new HashMap<String, String>();
    columnMap.put("patientAmbulanceCall", "PATIENT_AMBULANCECALL_1");
    columnMap.put("staffDesignation", "STAFFDESIGNATION_AMBU_1");
    columnMap.put("ambulanceCallResult", "AMBULANCECALLRESULT_A_1");
    columnMap.put("complicationList", null);
    return columnMap;
  }

  @Override
  @Transactional
  public String createCall(final AmbulanceCallCreatePackageDTO ambulanceCallPackage) {

    final NamedParameterJdbcTemplate jdbcTemplate = getJdbcTemplate();
    ambulanceCallPackage.setId(UUID.randomUUID().toString().substring(0, 32));
    final OSMPInsertRowBeanPropertyMapper<AmbulanceCallCreatePackageDTO> sqlMapper = new OSMPInsertRowBeanPropertyMapper<>("T_AMBULANCECALL", ambulanceCallPackage, createColumnMap());
    final String query = sqlMapper.createInsertSqlWithNamedParameters();
    jdbcTemplate.update(query, new BeanPropertySqlParameterSource(ambulanceCallPackage));

    if (!CollectionUtils.isEmpty(ambulanceCallPackage.getComplicationList())) {

      saveComplications(ambulanceCallPackage.getId(), ambulanceCallPackage.getComplicationList());

    }
    return ambulanceCallPackage.getId();
  }

  @Override
  public AmbulanceCallRowDTO getCall(String id) {
    final NamedParameterJdbcTemplate jdbcTemplate = getJdbcTemplate();
    final HashMap<String, Object> paramMap = new HashMap<>();
    paramMap.put("id", id);
    String query = "SELECT " +
        "      amb.id                        id, " +
        "      amb.code                      code, " +
        "      amb.calldatetime              calldatetime, " +
        "      amb.CALLSTATUS                callstatus," +
        "      amb.CALLADDRESS               callAddress," +
        "      amb.CALLLOCATION              callLocation," +
        "      amb.contactphone              contactphone, " +
        "      amb.WHOCALL                   whoCall," +
        "      amb.CALLREASON                callReason," +
        "      amb.CALLTYPE                  callType," +
        "      amb.CALLPASSDATETIME          callPassDateTime," +
        "      amb.CALLPASSTYPE              callPasstype, " +
        "      amb.DEPARTURETEAMNUMBER       departureTeamNumber," +
        "      amb.DEPARTUREDATETIME         departureDateTime," +
        "      amb.DEPARTURECARNUMBER        departureCarNumber," +
        "      amb.PATIENTARRIVALDATETIME    patientArrivalDatetime," +
        "      amb.PATIENT_AMBULANCECALL_1   patientAmbulanceCall," +
        "      amb.PATIENTCOMPLAINT          patientComplaint," +
        "      amb.DIAGNOSISMKB              diagnosisMKB," +
        "      amb.ACCIDENTREASON            accidentReason, " +
        "      amb.PATIENTRENDERINGHELP      patientRenderingHelp," +
        "      amb.MEASURE                   measure," +
        "      amb.EFFICACYSTEPCOMPLICATION  efficacyStepComplication," +
        "      amb.PATIENTSTATEAFTER         patientStateAfter," +
        "      amb.DEPARTURERETURNDATETIME   departureReturnDateTime," +
        "      amb.DEPARTUREDELAYREASON      departureDelayReason," +
        "      amb.DEPARTUREDISTANCE         departureDistance," +
        "      amb.AMBULANCECALLRESULT_A_1   ambulanceCallResult," +
        "      amb.HOSPITALIZATION           hospitalization," +
        "      amb.HOSPITALIZATIONDATETIME   hospitalizationDatetime," +
        "      amb.POLICEREPORTDATETIME      policeReportDateTime," +
        "      amb.POLICEREPORTSEND          policeReportSend," +
        "      amb.POLICEREPORTRECEIVE       policeReportReceive," +
        "      amb.POLICEREPORT              policeReport," +
        "      amb.CURRENTVISITPOLICLINIC    currentVisitPoliclinic," +
        "      amb.STAFFDESIGNATION_AMBU_1   staffDesignation, " +
        "      amb.LPU   lpu" +

        " FROM" +
        "      t_ambulancecall       amb " +
        "  WHERE amb.Id = :id";

    AmbulanceCallRowDTO res = jdbcTemplate.queryForObject(query, paramMap, new OSMPBeanPropertyRowMapper<>(AmbulanceCallRowDTO.class));
    String subQuery = "SELECT DOCBID " +
        " FROM LINK_T_AMBULANCECALL_T_COMPLICATION l" +
        " WHERE  l.DOCAID = :id";

    final HashMap<String, Object> paramMap2 = new HashMap<String, Object>() {{
      put("id", res.getId());
    }};
    res.setComplicationList(jdbcTemplate.query(subQuery, paramMap2, (rs, rowNum) -> rs.getString("DOCBID")));

    return res;
  }
}
