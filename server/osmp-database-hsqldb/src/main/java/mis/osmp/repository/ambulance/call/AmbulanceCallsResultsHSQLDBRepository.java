package mis.osmp.repository.ambulance.call;


import mis.osmp.repository.OSMPNamedJdbcTemplateRepository;
import mis.osmp.repository.ambulance.call.dto.AmbulanceCallResultDTO;
import mis.osmp.repository.map.OSMPBeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

@Repository
public class AmbulanceCallsResultsHSQLDBRepository extends OSMPNamedJdbcTemplateRepository implements AmbulanceCallsResultsRepository {
  @Override
  public List<AmbulanceCallResultDTO> getResults() {
    final String query = "SELECT ID, WITHPATIENT, NAME, CODE FROM T_AMBULANCECALLRESULT";
    return getJdbcTemplate().query(query, Collections.emptyMap(), new OSMPBeanPropertyRowMapper<>(AmbulanceCallResultDTO.class));
  }
}
