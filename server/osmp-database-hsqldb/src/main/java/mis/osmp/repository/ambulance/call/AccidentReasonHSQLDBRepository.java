package mis.osmp.repository.ambulance.call;


import mis.osmp.repository.OSMPNamedJdbcTemplateRepository;
import mis.osmp.repository.ambulance.call.dto.AccidentReasonDTO;
import mis.osmp.repository.map.OSMPBeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

@Repository
public class AccidentReasonHSQLDBRepository extends OSMPNamedJdbcTemplateRepository implements AccidentReasonRepository {
  @Override
  public List<AccidentReasonDTO> getReasons() {
    final String query = "SELECT ID, NAME FROM T_ACCIDENTREASON";
    return getJdbcTemplate().query(query, Collections.emptyMap(), new OSMPBeanPropertyRowMapper<>(AccidentReasonDTO.class));
  }
}
