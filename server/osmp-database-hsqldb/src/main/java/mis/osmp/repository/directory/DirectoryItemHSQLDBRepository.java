package mis.osmp.repository.directory;


import mis.osmp.repository.OSMPNamedJdbcTemplateRepository;
import mis.osmp.repository.directory.dto.DirectoryItemCreatePackageDTO;
import mis.osmp.repository.directory.dto.DirectoryItemDTO;
import mis.osmp.repository.directory.dto.DirectoryItemUpdatePackageDTO;
import mis.osmp.repository.directory.filter.DirectoryFilter;
import mis.osmp.repository.map.OSMPBeanPropertyRowMapper;
import mis.osmp.repository.map.OSMPInsertRowBeanPropertyMapper;
import mis.osmp.repository.map.OSMPUpdateRowBeanPropertyMapper;
import mis.osmp.repository.pagination.Page;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Repository
public class DirectoryItemHSQLDBRepository extends OSMPNamedJdbcTemplateRepository implements DirectoryItemRepository {
  @Override
  public List<DirectoryItemDTO> getItems(final String alias, final DirectoryFilter filter, final Page page) {
    final NamedParameterJdbcTemplate jdbcTemplate = getJdbcTemplate();
    final Map<String, Object> paramMap = new HashMap<String, Object>() {{
      put("startIndex", page.getStartIndex());
      put("pageSize", page.getRecordsOnPage());
      put("alias", alias);
    }};
    final String query = "SELECT id, name AS value" +
        " FROM  VDENUMITEMS " +
        " LEFT JOIN VDENUMTYPES ON VDENUMITEMS.enumtypeid = VDENUMTYPES.id " +
        " WHERE VDENUMTYPES.alias = :alias" +
        " LIMIT :pageSize OFFSET :startIndex";

    return jdbcTemplate.query(query, paramMap, new OSMPBeanPropertyRowMapper<>(DirectoryItemDTO.class));
  }

  @Override
  @Transactional
  public void deleteItem(final String alias, final String id) {
    final Map<String, Object> paramMap = new HashMap<String, Object>() {{
      put("alias", alias);
      put("id", id);
    }};
    final String query = "DELETE FROM VDENUMITEMS " +
        " WHERE VDENUMITEMS.enumtypeid IN " +
        "   (SELECT id FROM VDENUMTYPES WHERE alias = :alias )" +
        " AND VDENUMITEMS.id = :id";


    getJdbcTemplate().update(query, paramMap);
  }

  @Override
  @Transactional
  public DirectoryItemDTO createItem(final String alias, final DirectoryItemCreatePackageDTO packageDTO) {
    packageDTO.setId(UUID.randomUUID().toString().substring(0, 32));
    final OSMPInsertRowBeanPropertyMapper<DirectoryItemCreatePackageDTO> sqlMapper =
        new OSMPInsertRowBeanPropertyMapper<>("VDENUMITEMS", packageDTO, new HashMap<String, String>() {{
          put("value", "name");
        }});

    final String enumTypeId = getJdbcTemplate().queryForObject("SELECT id FROM VDENUMTYPES WHERE alias = :alias", new HashMap<String, Object>() {{
      put("alias", alias);
    }}, String.class);

    packageDTO.setEnumTypeId(enumTypeId);
    final String query = sqlMapper.createInsertSqlWithNamedParameters();
    getJdbcTemplate().update(query, new BeanPropertySqlParameterSource(packageDTO));
    DirectoryItemDTO result = new DirectoryItemDTO();
    result.setId(packageDTO.getId());
    result.setValue(packageDTO.getValue());
    return result;
  }

  @Override
  @Transactional
  public void updateItem(String id, DirectoryItemUpdatePackageDTO packageDTO) {
    final OSMPUpdateRowBeanPropertyMapper<DirectoryItemUpdatePackageDTO> sqlMapper =
        new OSMPUpdateRowBeanPropertyMapper<>("VDENUMITEMS", packageDTO, new HashMap<String, String>() {{
          put("value", "name");
        }});

    final StringBuilder updateQuery = sqlMapper.createUpdateSqlWithNamedParameters();
    updateQuery.append(" WHERE id= :id");
    getJdbcTemplate().update(updateQuery.toString(), new BeanPropertySqlParameterSource(packageDTO.withId(id)));
  }
}
