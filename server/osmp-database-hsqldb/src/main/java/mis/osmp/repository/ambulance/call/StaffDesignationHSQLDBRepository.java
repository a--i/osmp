package mis.osmp.repository.ambulance.call;

import mis.osmp.repository.OSMPNamedJdbcTemplateRepository;
import mis.osmp.repository.ambulance.call.dto.StaffDesignationDTO;
import mis.osmp.repository.ambulance.call.filter.StaffDesignationFilter;
import mis.osmp.repository.map.OSMPBeanPropertyRowMapper;
import mis.osmp.repository.pagination.Page;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;

@Repository
public class StaffDesignationHSQLDBRepository extends OSMPNamedJdbcTemplateRepository implements StaffDesignationRepository {

  @Override
  public List<StaffDesignationDTO> getStaffDesignations(StaffDesignationFilter filter, Page page) {

    final NamedParameterJdbcTemplate jdbcTemplate = getJdbcTemplate();
    final int startIndex = page.getPageNumber() * page.getRecordsOnPage();
    final HashMap<String, Object> paramMap = new HashMap<>();
    paramMap.put("startIndex", startIndex);
    paramMap.put("pageSize", page.getRecordsOnPage());
    String query = "select t.*" +
        " from " +
        "  (select s.id, p.LASTNAME + ' ' + p.FIRSTNAME + ' ' + p.MIDDLENAME + ' (' + pos.POSITIONNAME + ')'  as value" +
        " from T_STAFFDESIGNATION s" +
        " INNER JOIN T_EMPLOYEE e on e.ID = s.EMPLOYEE_STAFFDESIGNA_1" +
        " INNER JOIN T_PERSON p on p.ID = e.PERSON" +
        " inner join T_STAFF st ON st.ID = s.STAFF_STAFFDESIGNATIO_1" +
        " inner join T_POSITION pos on st.POSITION_STAFFS_1 = pos.ID) t" +
        "  where 1=1";

    if (!StringUtils.isEmpty(filter.getValue())) {
      query += " and UPPER(t.value) like :value";
      paramMap.put("value", "%" + filter.getValue().toUpperCase() + "%");
    }

    query += " LIMIT :pageSize OFFSET :startIndex";

    return jdbcTemplate.query(query, paramMap, new OSMPBeanPropertyRowMapper<>(StaffDesignationDTO.class));
  }

  @Override
  public StaffDesignationDTO getStaffDesignation(String id) {
    final NamedParameterJdbcTemplate jdbcTemplate = getJdbcTemplate();
    final HashMap<String, Object> paramMap = new HashMap<>();
    paramMap.put("id", id);
    String query = "select t.*" +
        " from " +
        "  (select s.id, p.LASTNAME + ' ' + p.FIRSTNAME + ' ' + p.MIDDLENAME + ' (' + pos.POSITIONNAME + ')'  as value" +
        " from T_STAFFDESIGNATION s" +
        " INNER JOIN T_EMPLOYEE e on e.ID = s.EMPLOYEE_STAFFDESIGNA_1" +
        " INNER JOIN T_PERSON p on p.ID = e.PERSON" +
        " inner join T_STAFF st ON st.ID = s.STAFF_STAFFDESIGNATIO_1" +
        " inner join T_POSITION pos on st.POSITION_STAFFS_1 = pos.ID) t" +
        " WHERE t.Id = :id";

    return jdbcTemplate.queryForObject(query, paramMap, new OSMPBeanPropertyRowMapper<>(StaffDesignationDTO.class));
  }
}
