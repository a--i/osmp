package mis.osmp.repository.mkb10;


import mis.osmp.repository.OSMPNamedJdbcTemplateRepository;
import mis.osmp.repository.map.OSMPBeanPropertyRowMapper;
import mis.osmp.repository.mkb10.dto.Mkb10DTO;
import mis.osmp.repository.mkb10.filter.Mkb10Filter;
import mis.osmp.repository.pagination.Page;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class Mkb10HSQLRepository extends OSMPNamedJdbcTemplateRepository implements Mkb10Repository {
  @Override
  public List<Mkb10DTO> getItems(Mkb10Filter filter, Page page) {
    final NamedParameterJdbcTemplate jdbcTemplate = getJdbcTemplate();
    final Map<String, Object> paramMap = new HashMap<String, Object>() {{
      put("startIndex", page.getStartIndex());
      put("pageSize", page.getRecordsOnPage());
    }};
    String query = "SELECT ID, CODEXX AS CODEX, CODEY, CODE, DIAGNOSENAME, SYMBOL" +
        " FROM  T_REFMKBDIAGNOSIS ";

    if (!StringUtils.isEmpty(filter.getCode())) {
      query += " where UPPER(Code) like :code";
      paramMap.put("code", filter.getCode().toUpperCase() + "%");
    }
     query += " ORDER BY Code LIMIT :pageSize OFFSET :startIndex";

    return jdbcTemplate.query(query, paramMap, new OSMPBeanPropertyRowMapper<>(Mkb10DTO.class));
  }

  @Override
  public Mkb10DTO getItem(String id) {
    final NamedParameterJdbcTemplate jdbcTemplate = getJdbcTemplate();
    final Map<String, Object> paramMap = new HashMap<String, Object>(){{
      put("id", id);
    }};
    String query = "SELECT ID, CODEXX AS CODEX, CODEY, CODE, DIAGNOSENAME, SYMBOL" +
        " FROM  T_REFMKBDIAGNOSIS " +
        " WHERE ID = :id";

    return jdbcTemplate.queryForObject(query, paramMap, new OSMPBeanPropertyRowMapper<>(Mkb10DTO.class));
  }
}
