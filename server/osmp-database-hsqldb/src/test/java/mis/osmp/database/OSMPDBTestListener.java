package mis.osmp.database;

import mis.osmp.database.annotation.OSMPMethodPackageData;
import mis.osmp.database.annotation.OSMPPackageData;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.support.AbstractTestExecutionListener;

import java.lang.annotation.Annotation;
import java.util.logging.Logger;

public class OSMPDBTestListener extends AbstractTestExecutionListener {
  public static final Logger LOGGER = Logger.getLogger("OSMPDBTestListener");

  @Override
  public void beforeTestMethod(TestContext testContext) throws Exception {
    if (!(testContext.getTestInstance() instanceof OSMPDBTest)) {
      LOGGER.warning("OSMPMethodPackageData annotation may use only in OSMPDBTest");
      return;
    }
    final OSMPDBTest instance = (OSMPDBTest) testContext.getTestInstance();
    final String packageName = testContext.getTestInstance().getClass().getPackage().getName();
    final String methodName = testContext.getTestMethod().getName();

    final Annotation[] annotations = testContext.getTestMethod().getAnnotations();
    for (final Annotation annotation : annotations) {
      if (annotation instanceof OSMPMethodPackageData) {
        final String scriptLocation = packageName + "/" + methodName;
        instance.populateDatabase(scriptLocation);
      } else if (annotation instanceof OSMPPackageData) {
        instance.populateDatabase(packageName);
      }
    }
  }
}
