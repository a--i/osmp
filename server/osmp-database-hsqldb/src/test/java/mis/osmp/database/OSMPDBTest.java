package mis.osmp.database;


import com.googlecode.flyway.core.Flyway;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.TransactionManagementConfigurer;

import javax.sql.DataSource;


public class OSMPDBTest implements TransactionManagementConfigurer {
  @Bean(destroyMethod = "close")
  public DataSource dataSource() {
    final HikariDataSource dataSource = new HikariDataSource();
    dataSource.setDriverClassName("org.hsqldb.jdbcDriver");
    dataSource.setJdbcUrl("jdbc:hsqldb:mem:db/data");
    dataSource.setUsername("sa");
    dataSource.setPassword("");
    return dataSource;
  }

  @Bean
  public NamedParameterJdbcTemplate jdbcTemplate() {
    return new NamedParameterJdbcTemplate(dataSource());
  }

  protected void populateDatabase(String... scriptLocations) {
    Flyway flyway = new Flyway();
    flyway.setDataSource(dataSource());
    flyway.setLocations(scriptLocations);
    flyway.clean();
    flyway.migrate();
  }

  @Bean
  public DataSourceTransactionManager dataSourceTransactionManager() {
    return new DataSourceTransactionManager(dataSource());
  }


  @Override
  public PlatformTransactionManager annotationDrivenTransactionManager() {
    return dataSourceTransactionManager();
  }
}
