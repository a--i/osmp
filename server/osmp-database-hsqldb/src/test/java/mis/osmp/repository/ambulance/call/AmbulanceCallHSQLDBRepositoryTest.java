package mis.osmp.repository.ambulance.call;

import mis.osmp.database.OSMPDBTest;
import mis.osmp.database.OSMPDBTestListener;
import mis.osmp.database.annotation.OSMPPackageData;
import mis.osmp.repository.ambulance.call.dto.AmbulanceCallCreatePackageDTO;
import mis.osmp.repository.ambulance.call.dto.AmbulanceCallDTO;
import mis.osmp.repository.ambulance.call.dto.AmbulanceCallRowDTO;
import mis.osmp.repository.ambulance.call.filter.AmbulanceCallsFilter;
import mis.osmp.repository.pagination.Page;
import mis.osmp.repository.pagination.Sort;
import mis.osmp.util.date.DateFormatter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AmbulanceCallHSQLDBRepositoryTest.class})
@TestExecutionListeners(listeners = {
    DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    OSMPDBTestListener.class
})
@Configuration
public class AmbulanceCallHSQLDBRepositoryTest extends OSMPDBTest {

  @Bean
  public AmbulanceCallHSQLDBRepository ambulanceCallHSQLDBRepository() {
    return new AmbulanceCallHSQLDBRepository();
  }

  @Autowired
  private AmbulanceCallHSQLDBRepository ambulanceCallHSQLDBRepository;

  @Test
  @DirtiesContext
  @OSMPPackageData
  public void testGetCalls() throws Exception {

    AmbulanceCallsFilter filtr = new AmbulanceCallsFilter();
    filtr.setFirstName("Иван");
    filtr.setLastName("Хороший");
    filtr.setMiddleName("Иванович");
    filtr.setCode("7");
    filtr.setDateFrom(new java.sql.Timestamp(new SimpleDateFormat().parse("20.02.2008 00:00:00").getTime()));
    filtr.setDateTo(new java.sql.Timestamp(new SimpleDateFormat().parse("26.02.2008 00:00:00").getTime()));
    filtr.setAmbulanceCallResult("3");

    Sort sort = new Sort();
    sort.setFieldName("ID,State");
    sort.setOrder("asc,desc");
    Page page = new Page();
    page.setSort(sort);

    final List<AmbulanceCallDTO> calls = ambulanceCallHSQLDBRepository.getCalls(filtr, page);

    assertNotNull(calls);
    assertFalse(calls.isEmpty());
    assertEquals(1, calls.size());
    final AmbulanceCallDTO ambulanceCallDTO = calls.get(0);
    assertNotNull(ambulanceCallDTO);

    assertEquals("Иван", ambulanceCallDTO.getFirstName());
    assertEquals("Хороший", ambulanceCallDTO.getLastName());
    assertEquals("Иванович", ambulanceCallDTO.getMiddleName());

    assertEquals("1", ambulanceCallDTO.getId());
    assertEquals("1", ambulanceCallDTO.getUserId());
    assertEquals("1", ambulanceCallDTO.getRwUserId());
    assertEquals(Integer.valueOf(0), ambulanceCallDTO.getState());
    assertEquals("743", ambulanceCallDTO.getCode());
    assertEquals("02/25/2008 20:25:20", DateFormatter.format(ambulanceCallDTO.getCallDatetime()));
    assertEquals("02/25/2008 21:26:18", DateFormatter.format(ambulanceCallDTO.getCallPassDatetime()));
    assertEquals("Хороший Иван Иванович (01.01.2008 г.р.)", ambulanceCallDTO.getPatient());
    assertEquals("КОНТРОЛЬ АД", ambulanceCallDTO.getCallReason());
    assertEquals("79123321123", ambulanceCallDTO.getContactPhone());
    assertEquals("CALLTYPE", ambulanceCallDTO.getCallType());
    assertEquals("CALLSTATUS", ambulanceCallDTO.getCallStatus());
    assertEquals("элемент справочника", ambulanceCallDTO.getDepartureTeamNumber());
    assertEquals("CALLPASSTYPE", ambulanceCallDTO.getCallPassType());
    assertEquals("3", ambulanceCallDTO.getResultId());
    assertEquals("1DEDF7F8F6DC2F43BE31762904BBA7E8", ambulanceCallDTO.getDiagnosisId());
    assertEquals("1DEDF7F8F6DC2F43BE31762904BBA7E8", ambulanceCallDTO.getRwDiagnosisId());
    assertEquals("Последствия термических и химических ожогов и отморожений", ambulanceCallDTO.getDiagnosis());
    assertEquals("T95", ambulanceCallDTO.getMkbCode());
    assertEquals("1", ambulanceCallDTO.getPatientWalking());
      }

  @Test
  @DirtiesContext
  @OSMPPackageData
  public void testRow() throws Exception {

    final AmbulanceCallRowDTO ambulanceCallDTO = ambulanceCallHSQLDBRepository.getCall("1");

    assertNotNull(ambulanceCallDTO);

    assertEquals("1", ambulanceCallDTO.getId());
    assertEquals("743", ambulanceCallDTO.getCode());
    assertEquals("6", ambulanceCallDTO.getCallStatus());
    assertEquals("02/25/2008 20:25:20", DateFormatter.format(ambulanceCallDTO.getCallDatetime()));
    assertEquals("ПСМП", ambulanceCallDTO.getCallAddress());
    assertEquals("2", ambulanceCallDTO.getCallLocation());
    assertEquals("79123321123", ambulanceCallDTO.getContactPhone());

    assertEquals("Сам", ambulanceCallDTO.getWhoCall());
    assertEquals("КОНТРОЛЬ АД", ambulanceCallDTO.getCallReason());

    assertEquals("5", ambulanceCallDTO.getCallType());
    assertEquals("02/25/2008 21:26:18", DateFormatter.format(ambulanceCallDTO.getCallPassDatetime()));

    assertEquals("7", ambulanceCallDTO.getCallPassType());
    assertEquals("1", ambulanceCallDTO.getDepartureTeamNumber());
    assertEquals("8", ambulanceCallDTO.getDepartureCarNumber());
    assertEquals("3", ambulanceCallDTO.getDepartureDelayReason());
    assertEquals("14", ambulanceCallDTO.getPatientAmbulanceCall());
    assertEquals("Жалоб нет. Хронические заболевания отрицает. Состояние удовлеворительное. Сознание ясное.неврологический статус в норме.А/Д - 135/80 mm Hg. Ps – 76 в 1 мин. t -  36.5 °C. Со стороны внутренних органов без патологии", ambulanceCallDTO.getPatientComplaint());
    assertEquals("1DEDF7F8F6DC2F43BE31762904BBA7E8", ambulanceCallDTO.getDiagnosisMKB());
    assertEquals("555", ambulanceCallDTO.getAccidentReason());
    assertEquals("4", ambulanceCallDTO.getEfficacyStepComplication());
    assertEquals("3", ambulanceCallDTO.getAmbulanceCallResult());
    assertEquals("POLICEREPORTSEND", ambulanceCallDTO.getPoliceReportSend());
    assertEquals("POLICEREPORTRECEIVE", ambulanceCallDTO.getPoliceReportReceive());
    List<String> complicationDTOList = ambulanceCallDTO.getComplicationList();
    assertEquals(2, complicationDTOList.size());
   // assertEquals("766", complicationDTOList.get(0).getId());
   // assertEquals("767", complicationDTOList.get(1).getId());
  }

  @Test
  @DirtiesContext
  @OSMPPackageData
  public void testCreateCall() throws Exception {
    final AmbulanceCallCreatePackageDTO callPackage = new AmbulanceCallCreatePackageDTO();
    callPackage.setState(1);
    final String call = ambulanceCallHSQLDBRepository.createCall(callPackage);
    assertNotNull(call);
    assertFalse(StringUtils.isEmpty(call));
    assertEquals(32, call.length());
  }

  @Test
  @DirtiesContext
  @OSMPPackageData
  public void testCreateCallTransaction() throws Exception {
    final AmbulanceCallCreatePackageDTO callPackage = new AmbulanceCallCreatePackageDTO();
    callPackage.setState(0);
    final String call = ambulanceCallHSQLDBRepository.createCall(callPackage);
    assertNotNull(call);
    assertFalse(StringUtils.isEmpty(call));
    assertEquals(32, call.length());

    final String addedRowId = jdbcTemplate().queryForObject("SELECT id FROM T_AMBULANCECALL WHERE id = :id", new HashMap<String, Object>() {{
      put("id", call);
    }}, String.class);
    assertEquals(call, addedRowId);

    final List<AmbulanceCallDTO> calls = ambulanceCallHSQLDBRepository.getCalls(new AmbulanceCallsFilter(), new Page());
    assertEquals(2, calls.size());
  }
}