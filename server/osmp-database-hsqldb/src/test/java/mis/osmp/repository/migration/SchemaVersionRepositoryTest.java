package mis.osmp.repository.migration;

import mis.osmp.database.OSMPDBTest;
import mis.osmp.repository.ambulance.call.AmbulanceCallHSQLDBRepositoryTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SchemaVersionRepositoryTest.class})
@Configuration
public class SchemaVersionRepositoryTest extends OSMPDBTest {

  @Bean
  public SchemaVersionRepository schemaVersionRepository(){
    return new SchemaVersionRepository();
  }

  @Autowired
  public SchemaVersionRepository schemaVersionRepository;

  @Test
  @DirtiesContext
  public void testGetExecutedScriptNames() throws Exception {
    populateDatabase(AmbulanceCallHSQLDBRepositoryTest.class.getPackage().getName());
    final List<String> executedScriptNames = schemaVersionRepository.getExecutedScriptNames();
    assertNotNull(executedScriptNames);
    assertEquals(2, executedScriptNames.size());
    assertEquals("V1__SCHEMA.sql", executedScriptNames.get(0));
    assertEquals("V2__DATA.sql", executedScriptNames.get(1));
  }
}