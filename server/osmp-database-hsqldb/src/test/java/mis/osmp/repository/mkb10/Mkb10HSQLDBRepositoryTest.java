package mis.osmp.repository.mkb10;

import mis.osmp.database.OSMPDBTest;
import mis.osmp.database.OSMPDBTestListener;
import mis.osmp.database.annotation.OSMPMethodPackageData;
import mis.osmp.repository.mkb10.dto.Mkb10DTO;
import mis.osmp.repository.mkb10.filter.Mkb10Filter;
import mis.osmp.repository.pagination.Page;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {Mkb10HSQLDBRepositoryTest.class})
@TestExecutionListeners(listeners = {
    DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    OSMPDBTestListener.class
})
@Configuration
public class Mkb10HSQLDBRepositoryTest extends OSMPDBTest {

  @Bean
  public Mkb10HSQLRepository mkb10HSQLDBRepository() {
    return new Mkb10HSQLRepository();
  }

  @Autowired
  private Mkb10HSQLRepository mkb10HSQLDBRepository;

  @Test
  @DirtiesContext
  @OSMPMethodPackageData
  public void testGetItems() throws Exception {
    Mkb10Filter filter = new Mkb10Filter();
    filter.setCode("T");
    final List<Mkb10DTO> items = mkb10HSQLDBRepository.getItems(filter,  new Page());
    assertNotNull(items);
    assertEquals(1, items.size());

    final Mkb10DTO dto = items.get(0);
    assertNotNull(dto);
    assertEquals("1DEDF7F8F6DC2F43BE31762904BBA7E8", dto.getId());
    assertEquals("Последствия термических и химических ожогов и отморожений", dto.getDiagnoseName());
    assertEquals("T95", dto.getCode());
  }

  @Test
  @DirtiesContext
  @OSMPMethodPackageData
  public void testGetItem() throws Exception {
    final Mkb10DTO item = mkb10HSQLDBRepository.getItem("1DEDF7F8F6DC2F43BE31762904BBA7E8");
    assertNotNull(item);
    assertEquals("1DEDF7F8F6DC2F43BE31762904BBA7E8", item.getId());
    assertEquals("Последствия термических и химических ожогов и отморожений", item.getDiagnoseName());
    assertEquals("T95", item.getCode());
  }
}