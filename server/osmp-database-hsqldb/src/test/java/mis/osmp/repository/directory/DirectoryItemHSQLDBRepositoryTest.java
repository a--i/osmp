package mis.osmp.repository.directory;

import mis.osmp.database.OSMPDBTest;
import mis.osmp.database.OSMPDBTestListener;
import mis.osmp.database.annotation.OSMPMethodPackageData;
import mis.osmp.repository.directory.dto.DirectoryItemCreatePackageDTO;
import mis.osmp.repository.directory.dto.DirectoryItemDTO;
import mis.osmp.repository.directory.dto.DirectoryItemUpdatePackageDTO;
import mis.osmp.repository.directory.filter.DirectoryFilter;
import mis.osmp.repository.pagination.Page;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DirectoryItemHSQLDBRepositoryTest.class})
@TestExecutionListeners(listeners = {
    DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    OSMPDBTestListener.class
})
@Configuration
public class DirectoryItemHSQLDBRepositoryTest extends OSMPDBTest {

  @Bean
  public DirectoryItemHSQLDBRepository directoryItemHSQLDBRepository() {
    return new DirectoryItemHSQLDBRepository();
  }

  @Autowired
  private DirectoryItemHSQLDBRepository directoryItemHSQLDBRepository;

  @Test
  @DirtiesContext
  @OSMPMethodPackageData
  public void testGetItems() throws Exception {

    final List<DirectoryItemDTO> items = directoryItemHSQLDBRepository.getItems("DIRECTORY_ALIAS", new DirectoryFilter(), new Page());
    assertNotNull(items);
    assertEquals(1, items.size());

    final DirectoryItemDTO dto = items.get(0);
    assertNotNull(dto);
    assertEquals("1", dto.getId());
    assertEquals("элемент справочника", dto.getValue());
  }

  @Test
  @DirtiesContext
  @OSMPMethodPackageData
  public void testDeleteItem() throws Exception {
    final List<DirectoryItemDTO> items = directoryItemHSQLDBRepository.getItems("DIRECTORY_ALIAS", new DirectoryFilter(), new Page());
    assertNotNull(items);
    assertEquals(2, items.size());

    directoryItemHSQLDBRepository.deleteItem("DIRECTORY_ALIAS", "1");
    final List<DirectoryItemDTO> resultItems = directoryItemHSQLDBRepository.getItems("DIRECTORY_ALIAS", new DirectoryFilter(), new Page());
    assertNotNull(resultItems);
    assertEquals(1, resultItems.size());

  }

  @Test
  @DirtiesContext
  @OSMPMethodPackageData
  public void testCreateItem() throws Exception {
    final List<DirectoryItemDTO> items = directoryItemHSQLDBRepository.getItems("DIRECTORY_ALIAS", new DirectoryFilter(), new Page());
    assertNotNull(items);
    assertTrue(items.isEmpty());

    final DirectoryItemCreatePackageDTO packageDTO = new DirectoryItemCreatePackageDTO();
    packageDTO.setValue("SAMPLE_DIRECTORY_ITEM_NAME");
    packageDTO.setAlias("SAMPLE_DIRECTORY_ITEM_ALIAS");
    directoryItemHSQLDBRepository.createItem("DIRECTORY_ALIAS", packageDTO);
    final List<DirectoryItemDTO> directoryItems = directoryItemHSQLDBRepository.getItems("DIRECTORY_ALIAS", new DirectoryFilter(), new Page());
    assertNotNull(directoryItems);
    assertEquals(1, directoryItems.size());
    final DirectoryItemDTO item = directoryItems.get(0);
    assertNotNull(item);
    assertEquals(packageDTO.getId(), item.getId());
    assertEquals(packageDTO.getValue(), item.getValue());
  }

  @Test
  @DirtiesContext
  @OSMPMethodPackageData
  public void testUpdateItem() throws Exception {

    final List<DirectoryItemDTO> items = directoryItemHSQLDBRepository.getItems("DIRECTORY_ALIAS", new DirectoryFilter(), new Page());
    assertNotNull(items);
    assertEquals(1, items.size());
    assertEquals("элемент справочника", items.get(0).getValue());

    final DirectoryItemUpdatePackageDTO packageDTO = new DirectoryItemUpdatePackageDTO();
    packageDTO.setValue("SOME_NAME");
    directoryItemHSQLDBRepository.updateItem("1", packageDTO);

    final List<DirectoryItemDTO> updatedItems = directoryItemHSQLDBRepository.getItems("DIRECTORY_ALIAS", new DirectoryFilter(), new Page());
    assertNotNull(updatedItems);
    assertEquals(1, updatedItems.size());
    assertEquals("SOME_NAME", updatedItems.get(0).getValue());

  }
}