package mis.osmp.repository.ambulance.call;

import mis.osmp.database.OSMPDBTest;
import mis.osmp.database.OSMPDBTestListener;
import mis.osmp.database.annotation.OSMPPackageData;
import mis.osmp.repository.ambulance.call.dto.AmbulanceCallResultDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AmbulanceCallsResultsHSQLDBRepositoryTest.class})
@TestExecutionListeners(listeners = {
    DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    OSMPDBTestListener.class
})
@Configuration
public class AmbulanceCallsResultsHSQLDBRepositoryTest extends OSMPDBTest {

  @Bean
  public AmbulanceCallsResultsHSQLDBRepository ambulanceCallsResultsHSQLDBRepository() {
    return new AmbulanceCallsResultsHSQLDBRepository();
  }

  @Autowired
  private AmbulanceCallsResultsHSQLDBRepository ambulanceCallsResultsHSQLDBRepository;

  @Test
  @DirtiesContext
  @OSMPPackageData
  public void testGetResults() throws Exception {

    final List<AmbulanceCallResultDTO> results = ambulanceCallsResultsHSQLDBRepository.getResults();
    assertNotNull(results);

    assertEquals(1, results.size());

    final AmbulanceCallResultDTO result = results.get(0);
    assertNotNull(result);
    assertEquals("3", result.getId());
    assertEquals("anme", result.getName());
    assertEquals(Integer.valueOf(23), result.getCode());
    assertEquals(Integer.valueOf(12), result.getWithPatient());
  }
}