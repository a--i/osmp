package mis.osmp.repository.ambulance.call;

import mis.osmp.database.OSMPDBTest;
import mis.osmp.database.OSMPDBTestListener;
import mis.osmp.database.annotation.OSMPPackageData;
import mis.osmp.repository.ambulance.call.dto.StaffDesignationDTO;
import mis.osmp.repository.ambulance.call.filter.StaffDesignationFilter;
import mis.osmp.repository.pagination.Page;
import mis.osmp.repository.pagination.Sort;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {StaffDesignationHSQLDBRepositoryTest.class})
@TestExecutionListeners(listeners = {
    DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    OSMPDBTestListener.class
})
@Configuration
public class StaffDesignationHSQLDBRepositoryTest extends OSMPDBTest {

  @Bean
  public StaffDesignationHSQLDBRepository staffDesignationHSQLDBRepository() {
    return new StaffDesignationHSQLDBRepository();
  }

  @Autowired
  private StaffDesignationHSQLDBRepository staffDesignationHSQLDBRepository;

  @Test
  @DirtiesContext
  @OSMPPackageData
  public void testGetStaffDesignations() throws Exception {

    StaffDesignationFilter filtr = new StaffDesignationFilter();
    filtr.setValue("Иван");

    Sort sort = new Sort();
    sort.setFieldName("Caption");
    sort.setOrder("asc");
    Page page = new Page();
    page.setSort(sort);

    final List<StaffDesignationDTO> calls = staffDesignationHSQLDBRepository.getStaffDesignations(filtr, page);

    assertNotNull(calls);
    assertFalse(calls.isEmpty());
    assertEquals(1, calls.size());
    final StaffDesignationDTO staffDesignationDTO = calls.get(0);
    assertNotNull(staffDesignationDTO);

    assertEquals("Хороший Иван Иванович (Врач - хирург)", staffDesignationDTO.getValue());
  }

  @Test
  @DirtiesContext
  @OSMPPackageData
  public void testGetStaffDesignation() throws Exception {
    final StaffDesignationDTO staffDesignationDTO = staffDesignationHSQLDBRepository.getStaffDesignation("STAFFDESIGNATION");
    assertNotNull(staffDesignationDTO);
    assertEquals("Хороший Иван Иванович (Врач - хирург)", staffDesignationDTO.getValue());
  }
}