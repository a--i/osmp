package mis.osmp.repository.ambulance.call;

import mis.osmp.database.OSMPDBTest;
import mis.osmp.database.OSMPDBTestListener;
import mis.osmp.database.annotation.OSMPPackageData;
import mis.osmp.repository.ambulance.call.dto.AccidentReasonDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AccidentReasonHSQLDBRepositoryTest.class})
@TestExecutionListeners(listeners = {
    DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    OSMPDBTestListener.class
})
@Configuration
public class AccidentReasonHSQLDBRepositoryTest extends OSMPDBTest {

  @Bean
  public AccidentReasonHSQLDBRepository accidentReasonHSQLDBRepository() {
    return new AccidentReasonHSQLDBRepository();
  }

  @Autowired
  private AccidentReasonHSQLDBRepository accidentReasonHSQLDBRepository;

  @Test
  @DirtiesContext
  @OSMPPackageData
  public void testGetReasons() throws Exception {

    final List<AccidentReasonDTO> reasons = accidentReasonHSQLDBRepository.getReasons();
    assertNotNull(reasons);

    assertEquals(1, reasons.size());

    final AccidentReasonDTO result = reasons.get(0);
    assertNotNull(result);
    assertEquals("555", result.getId());
    assertEquals("ACCIDENTREASON", result.getName());
  }
}