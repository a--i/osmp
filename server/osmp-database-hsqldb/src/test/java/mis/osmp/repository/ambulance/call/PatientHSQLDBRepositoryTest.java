package mis.osmp.repository.ambulance.call;

import mis.osmp.database.OSMPDBTest;
import mis.osmp.database.OSMPDBTestListener;
import mis.osmp.database.annotation.OSMPPackageData;
import mis.osmp.repository.ambulance.call.dto.PatientDTO;
import mis.osmp.repository.ambulance.call.filter.PatientFilter;
import mis.osmp.repository.pagination.Page;
import mis.osmp.repository.pagination.Sort;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {PatientHSQLDBRepositoryTest.class})
@TestExecutionListeners(listeners = {
    DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    OSMPDBTestListener.class
})
@Configuration
public class PatientHSQLDBRepositoryTest extends OSMPDBTest {

  @Bean
  public PatientHSQLDBRepository patientHSQLDBRepository() {
    return new PatientHSQLDBRepository();
  }

  @Autowired
  private PatientHSQLDBRepository patientHSQLDBRepository;

  @Test
  @DirtiesContext
  @OSMPPackageData
  public void testGetPatients() throws Exception {

    PatientFilter filtr = new PatientFilter();
    filtr.setFirstName("Иван");
    filtr.setLastName("Хороший");
    filtr.setMiddleName("Иванович");

    Sort sort = new Sort();
    sort.setFieldName("Firstname");
    sort.setOrder("asc");
    Page page = new Page();
    page.setSort(sort);

    final List<PatientDTO> calls = patientHSQLDBRepository.getPatients(filtr, page);

    assertNotNull(calls);
    assertFalse(calls.isEmpty());
    assertEquals(1, calls.size());
    final PatientDTO patientDTO = calls.get(0);
    assertNotNull(patientDTO);

    assertEquals("Иван", patientDTO.getPersonDto().getFirstName());
    assertEquals("Хороший", patientDTO.getPersonDto().getLastName());
    assertEquals("Иванович", patientDTO.getPersonDto().getMiddleName());
  }

  @Test
  @DirtiesContext
  @OSMPPackageData
  public void testGetPatient() throws Exception {
    final PatientDTO patientDTO = patientHSQLDBRepository.getPatient("14");
    assertNotNull(patientDTO);

    assertEquals("Иван", patientDTO.getPersonDto().getFirstName());
    assertEquals("Хороший", patientDTO.getPersonDto().getLastName());
    assertEquals("Иванович", patientDTO.getPersonDto().getMiddleName());
    assertEquals("1400", patientDTO.getPersonDto().getPassportDto().getPassSeries());
    assertEquals("123456", patientDTO.getPersonDto().getPassportDto().getPassNumber());
    assertEquals("Бельдяжки", patientDTO.getPersonDto().getPassportDto().getIssuePlace());
    assertEquals("14", patientDTO.getPersonDto().getPassportDto().getDocType());
  }
}