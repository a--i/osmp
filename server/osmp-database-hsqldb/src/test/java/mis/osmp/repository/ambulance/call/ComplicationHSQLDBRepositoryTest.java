package mis.osmp.repository.ambulance.call;

import mis.osmp.database.OSMPDBTest;
import mis.osmp.database.OSMPDBTestListener;
import mis.osmp.database.annotation.OSMPPackageData;
import mis.osmp.repository.ambulance.call.dto.ComplicationDTO;
import mis.osmp.repository.ambulance.call.filter.ComplicationFilter;
import mis.osmp.repository.pagination.Page;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ComplicationHSQLDBRepositoryTest.class})
@TestExecutionListeners(listeners = {
    DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    OSMPDBTestListener.class
})
@Configuration
public class ComplicationHSQLDBRepositoryTest extends OSMPDBTest {

  @Bean
  public ComplicationHSQLDBRepository complicationHSQLDBRepository() {
    return new ComplicationHSQLDBRepository();
  }

  @Autowired
  private ComplicationHSQLDBRepository complicationHSQLDBRepository;

  @Test
  @DirtiesContext
  @OSMPPackageData
  public void testGetPatients() throws Exception {

    ComplicationFilter filtr = new ComplicationFilter();
    filtr.setName("Ко");

    final List<ComplicationDTO> calls = complicationHSQLDBRepository.getComplications(filtr, new Page());

    assertNotNull(calls);
    assertFalse(calls.isEmpty());
    assertEquals(1, calls.size());
    final ComplicationDTO сomplicationDTO = calls.get(0);
    assertNotNull(сomplicationDTO);

    assertEquals("Кома", сomplicationDTO.getName());
  }

  @Test
  @DirtiesContext
  @OSMPPackageData
  public void testGetPatient() throws Exception {
    final ComplicationDTO complicationDTO = complicationHSQLDBRepository.getComplication("766");
    assertNotNull(complicationDTO);

    assertEquals("Кома", complicationDTO.getName());
  }
}