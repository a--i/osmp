CREATE TABLE VDENUMTYPES
(
  ID    VARCHAR(32) PRIMARY KEY NOT NULL,
  NAME  VARCHAR(128)            NOT NULL,
  ALIAS VARCHAR(64)             NOT NULL
);

CREATE TABLE VDENUMITEMS
(
  ID         VARCHAR(32) PRIMARY KEY                   NOT NULL,
  NAME       VARCHAR(128)                              NOT NULL,
  enumtypeid VARCHAR(32) REFERENCES VDENUMTYPES (ID) NOT NULL
);

INSERT INTO VDENUMTYPES (ID, NAME, ALIAS)
VALUES ('1', 'тип справочника', 'DIRECTORY_ALIAS');

INSERT INTO VDENUMITEMS (ID, NAME, enumtypeid)
VALUES ('1', 'элемент справочника', '1');

INSERT INTO VDENUMITEMS (ID, NAME, enumtypeid)
VALUES ('2', 'элемент справочника 2', '1');
