package mis.osmp.security.config;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.*;
import org.springframework.security.config.annotation.authentication.builders.*;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.*;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.www.DigestAuthenticationFilter;
import org.springframework.security.web.authentication.www.DigestAuthenticationEntryPoint;



@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity( jsr250Enabled = true, prePostEnabled = true)
public class SecurityConfig extends  WebSecurityConfigurerAdapter {



  public DigestAuthenticationFilter createDigestAuthenticationFilter  (DigestAuthenticationEntryPoint entryPoint) throws Exception
  {
    DigestAuthenticationFilter flt = new DigestAuthenticationFilter();

    flt.setAuthenticationEntryPoint(entryPoint);
    flt.setUserDetailsService(userDetailsServiceBean());
    return flt;
  }

  @Bean
  public DigestAuthenticationEntryPoint digestEntryPoint()
  {
    DigestAuthenticationEntryPoint ep = new DigestAuthenticationEntryPoint();
    ep.setRealmName("hospitalization");
    ep.setKey("dlinnsqkey");
    return ep;
  }

  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    auth
        .inMemoryAuthentication()
        .withUser("user").password("password").roles("USER")
        .and()
        .withUser("doctor").password("password").roles("USER","DOCTOR");

  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
        .csrf().disable()
        .exceptionHandling()
            .authenticationEntryPoint(digestEntryPoint())
        .and()
        .authorizeRequests()
          .anyRequest().authenticated()
          .anyRequest().hasRole("USER")
          .and().addFilter(createDigestAuthenticationFilter(digestEntryPoint()));


  }

}
