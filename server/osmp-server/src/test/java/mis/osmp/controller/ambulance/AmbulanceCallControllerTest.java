package mis.osmp.controller.ambulance;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import mis.osmp.config.AppConfig;
import mis.osmp.config.WebConfig;
import mis.osmp.controller.exception.GlobalExceptionHandler;
import mis.osmp.repository.ambulance.call.*;
import mis.osmp.repository.ambulance.call.dto.AmbulanceCallCreatePackageDTO;
import mis.osmp.repository.ambulance.call.dto.AmbulanceCallDTO;
import mis.osmp.repository.ambulance.call.dto.AmbulanceCallRowDTO;
import mis.osmp.repository.directory.DirectoryItemRepository;
import mis.osmp.repository.mkb10.Mkb10Repository;
import mis.osmp.util.date.DateFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.ByteArrayInputStream;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {AmbulanceCallControllerTest.class, AppConfig.class, WebConfig.class})
public class AmbulanceCallControllerTest {

  private MockMvc mockMvc;
  final private Timestamp timestampSample;
  private ObjectMapper objectMapper;

  public AmbulanceCallControllerTest() throws ParseException {
    timestampSample = new Timestamp(new SimpleDateFormat().parse("10.10.2010 10:10:10").getTime());
  }

  @Bean
  public AmbulanceCallRepository ambulanceCallRepository() {
    final AmbulanceCallRowDTO ambulanceCallDTO = new AmbulanceCallRowDTO();
    ambulanceCallDTO.setId("1");
    ambulanceCallDTO.setCallDatetime(timestampSample);
    final AmbulanceCallRepository ambulanceCallRepository = Mockito.mock(AmbulanceCallRepository.class);
    Mockito.when(ambulanceCallRepository.getCall("1")).thenReturn(ambulanceCallDTO);

    Mockito.when(ambulanceCallRepository.createCall(Mockito.any(AmbulanceCallCreatePackageDTO.class))).thenReturn("1");
    return ambulanceCallRepository;
  }

  @Bean
  public DirectoryItemRepository directoryItemRepository() {
    return Mockito.mock(DirectoryItemRepository.class);
  }

  @Bean
  public AmbulanceCallsResultsRepository ambulanceCallsResultsRepository() {
    return Mockito.mock(AmbulanceCallsResultsRepository.class);
  }

  @Bean
    public ComplicationRepository complicationRepository() {
    return Mockito.mock(ComplicationRepository.class);
  }

  @Bean
  public StaffDesignationRepository staffDesignationRepository() {
    return Mockito.mock(StaffDesignationRepository.class);
  }

  @Bean
  public Mkb10Repository mkb10Repository() {
    return Mockito.mock(Mkb10Repository.class);
  }

  @Bean
  public AccidentReasonRepository accidentReasonRepository() {
    return Mockito.mock(AccidentReasonRepository.class);
  }

  @Bean
  public PatientRepository patientRepository() {
    return Mockito.mock(PatientRepository.class);
  }

  final String expectedContentType = "application/json;charset=utf-8";

  @Autowired
  private WebApplicationContext wac;

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    final ArrayList<HttpMessageConverter<?>> converters = new ArrayList<>();
    new WebConfig().configureMessageConverters(converters);
    final MappingJackson2HttpMessageConverter httpMessageConverter = (MappingJackson2HttpMessageConverter) converters.get(0);
    objectMapper = httpMessageConverter.getObjectMapper();
    mockMvc = MockMvcBuilders.webAppContextSetup(wac)
        .build();
  }

  @Test
  public void testGetRow() throws Exception {
    final MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/ambulance/calls/1"))
        .andExpect(MockMvcResultMatchers.content().contentType(expectedContentType))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andReturn();

    final byte[] contentAsByteArray = mvcResult.getResponse().getContentAsByteArray();
    final AmbulanceCallDTO result = objectMapper.reader(AmbulanceCallDTO.class).readValue(contentAsByteArray);
    assertNotNull(result);
    assertEquals("1", result.getId());
    assertEquals(timestampSample, result.getCallDatetime());

    final JsonNode jsonNode = objectMapper.reader().readTree(new ByteArrayInputStream(contentAsByteArray));
    final JsonNode birthDateNode = jsonNode.get("callDatetime");
    assertNotNull(birthDateNode);
    assertEquals(DateFormatter.format(timestampSample), birthDateNode.asText());
  }

  @Test
  public void testCreateAmbulanceCallValidation() throws Exception {
    final MvcResult mvcResult = mockMvc.perform(
        MockMvcRequestBuilders.post("/ambulance/calls")
            .contentType(expectedContentType)
            .content(objectMapper.writeValueAsString(new AmbulanceCallCreatePackageDTO()))
    )
        .andExpect(MockMvcResultMatchers.content().contentType(expectedContentType))
        .andExpect(MockMvcResultMatchers.status().isBadRequest())
        .andReturn();

    assertNotNull(mvcResult);
    final byte[] contentAsByteArray = mvcResult.getResponse().getContentAsByteArray();
    final JsonNode jsonNode = objectMapper.readTree(contentAsByteArray);
    assertNotNull(jsonNode);
    final JsonNode errorNode = jsonNode.get("error");
    assertNotNull(errorNode);
    final JsonNode codeNode = errorNode.get("code");
    assertEquals(GlobalExceptionHandler.VALIDATION_ERROR_CODE, codeNode.asText());
    final JsonNode fieldExceptionsNode = errorNode.get("fieldExceptions");
    assertNotNull(fieldExceptionsNode);
    assertTrue(fieldExceptionsNode instanceof ArrayNode);
    final ArrayNode arrayNode = (ArrayNode) fieldExceptionsNode;
    assertEquals(4, arrayNode.size());
    final JsonNode fieldExceptionNode = arrayNode.get(0);
    assertNotNull(fieldExceptionNode);
    final JsonNode descriptionNode = fieldExceptionNode.get("description");
    assertNotNull(descriptionNode);
    assertEquals("\"Поле не может быть пустым\"", descriptionNode.asText());
  }

  @Test
  public void testCreateAmbulanceCall() throws Exception {
    final AmbulanceCallCreatePackageDTO value = new AmbulanceCallCreatePackageDTO();
    value.setCallDatetime(timestampSample);
    value.setCode("1");
    value.setCallStatus("123");
    value.setStaffDesignation("asddas");
    mockMvc.perform(
        MockMvcRequestBuilders.post("/ambulance/calls")
            .contentType(expectedContentType)
            .content(objectMapper.writeValueAsString(value))
    )
        .andExpect(MockMvcResultMatchers.content().contentType(expectedContentType))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andReturn();
  }
}