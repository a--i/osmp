package mis.osmp.config;


import com.typesafe.config.Config;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

import javax.sql.DataSource;


@Configuration
public class AppConfig {


  @Bean(destroyMethod = "close")
  public DataSource dataSource() {
    final Config config = OsmpConfig.getConfig();
    final HikariDataSource dataSource = new HikariDataSource();
    dataSource.setDriverClassName(config.getString("database.driver"));
    dataSource.setJdbcUrl(config.getString("database.url"));
    dataSource.setUsername(config.getString("database.user"));
    dataSource.setPassword(config.getString("database.password"));
    return dataSource;
  }

  @Bean
  public MessageSource messageSource(){
    final ResourceBundleMessageSource resourceBundleMessageSource = new ResourceBundleMessageSource();
    resourceBundleMessageSource.setDefaultEncoding("utf-8");
    resourceBundleMessageSource.setBasename("i18n/messages");
    resourceBundleMessageSource.setUseCodeAsDefaultMessage(true);
    return resourceBundleMessageSource;
  }
}
