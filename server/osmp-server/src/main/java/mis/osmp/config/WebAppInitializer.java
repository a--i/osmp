package mis.osmp.config;

import mis.osmp.cors.CORSFilter;
import mis.osmp.database.migration.config.DatabaseMigrationConfiguration;
import mis.osmp.repository.config.RepositoryConfiguration;
import mis.osmp.security.config.SecurityConfig;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;


import javax.servlet.Filter;
import javax.servlet.ServletRegistration;
import java.nio.charset.StandardCharsets;


public class WebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

  @Override
  protected void customizeRegistration(ServletRegistration.Dynamic registration) {
    registration.setInitParameter("dispatchOptionsRequest", OsmpConfig.isAllowCORS() + "");
    registration.setAsyncSupported(true);
  }

  @Override
  protected Class<?>[] getRootConfigClasses() {
    return new Class<?>[]{
        SecurityConfig.class,
        AppConfig.class,
        RepositoryConfiguration.class,
        DatabaseMigrationConfiguration.class
    };
  }

  @Override
  protected Class<?>[] getServletConfigClasses() {
    return new Class<?>[]{ WebConfig.class };
  }

  @Override
  protected String[] getServletMappings() {
    return new String[]{"/"};
  }

  @Override
  protected Filter[] getServletFilters() {
    CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
    characterEncodingFilter.setEncoding(StandardCharsets.UTF_8.name());
    characterEncodingFilter.setForceEncoding(true);
    if (OsmpConfig.isAllowCORS())
      return new Filter[]{characterEncodingFilter, new CORSFilter()};
    return new Filter[]{characterEncodingFilter};
  }
}