package mis.osmp.controller.staffdesignation;


import mis.osmp.repository.ambulance.call.StaffDesignationRepository;
import mis.osmp.repository.ambulance.call.dto.StaffDesignationDTO;
import mis.osmp.repository.ambulance.call.filter.StaffDesignationFilter;
import mis.osmp.repository.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/staff")
public class StaffDesignationController {

  @Autowired
  private StaffDesignationRepository staffDesignationRepository;

  @RequestMapping(value = "designations/{id}", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
  @ResponseBody
  public StaffDesignationDTO getPatient(@PathVariable(value = "id") String id) {
    return staffDesignationRepository.getStaffDesignation(id);
  }

  @RequestMapping(value = "designations", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
  @ResponseBody
  public List<StaffDesignationDTO> filterPatient(StaffDesignationFilter filter, Page page) {
    return staffDesignationRepository.getStaffDesignations(filter, page);
  }
}
