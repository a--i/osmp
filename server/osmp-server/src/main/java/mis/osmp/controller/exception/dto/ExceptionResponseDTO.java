package mis.osmp.controller.exception.dto;

import lombok.Data;

@Data
public class ExceptionResponseDTO {
  private ExceptionDTO error;
}
