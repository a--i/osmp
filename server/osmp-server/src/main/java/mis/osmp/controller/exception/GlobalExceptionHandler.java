package mis.osmp.controller.exception;

import mis.osmp.config.OsmpConfig;
import mis.osmp.controller.exception.dto.ExceptionDTO;
import mis.osmp.controller.exception.dto.ExceptionResponseDTO;
import mis.osmp.controller.exception.dto.FieldValidationExceptionDTO;
import mis.osmp.controller.exception.dto.ValidationExceptionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@ControllerAdvice
public class GlobalExceptionHandler {

  public static final String VALIDATION_ERROR_CODE = "validation";

  private MessageSource messageSource;

  @Autowired
  public GlobalExceptionHandler(MessageSource messageSource) {
    this.messageSource = messageSource;
  }

  @ExceptionHandler(Throwable.class)
  @ResponseBody
  public ExceptionResponseDTO handleAllThrowable(HttpServletResponse response, Exception ex) {
    response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
    if (ex instanceof HttpRequestMethodNotSupportedException) {
      if (((HttpRequestMethodNotSupportedException) ex).getMethod().equals(RequestMethod.OPTIONS.name())) {
        if (OsmpConfig.isAllowCORS()) {
          response.setStatus(HttpStatus.OK.value());
          return null;
        }
      }
    }
    final ExceptionDTO exceptionDTO = new ExceptionDTO();
    exceptionDTO.setCode(ex.getClass().getName());
    exceptionDTO.setDescription(ex.getMessage());
    final ExceptionResponseDTO exceptionResponseDTO = new ExceptionResponseDTO();
    exceptionResponseDTO.setError(exceptionDTO);
    return exceptionResponseDTO;
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ExceptionResponseDTO handleValidationExceptions(HttpServletRequest req, MethodArgumentNotValidException ex) {
    final ExceptionResponseDTO exceptionResponseDTO = new ExceptionResponseDTO();
    exceptionResponseDTO.setError(createValidationException(ex.getBindingResult().getFieldErrors()));
    return exceptionResponseDTO;
  }

  private ValidationExceptionDTO createValidationException(List<FieldError> fieldErrors) {
    final ValidationExceptionDTO exceptionDTO = new ValidationExceptionDTO();
    exceptionDTO.setCode(VALIDATION_ERROR_CODE);
    final ArrayList<FieldValidationExceptionDTO> fieldExceptions = new ArrayList<>();
    for (FieldError fieldError : fieldErrors) {
      final FieldValidationExceptionDTO fieldValidationExceptionDTO = new FieldValidationExceptionDTO();
      fieldValidationExceptionDTO.setField(fieldError.getField());
      fieldValidationExceptionDTO.setDescription(resolveLocalizedErrorMessage(fieldError));
      fieldExceptions.add(fieldValidationExceptionDTO);
    }
    exceptionDTO.setFieldExceptions(fieldExceptions);
    return exceptionDTO;
  }

  private String resolveLocalizedErrorMessage(FieldError fieldError) {
    Locale currentLocale = LocaleContextHolder.getLocale();
    String localizedErrorMessage = messageSource.getMessage(fieldError, currentLocale);
    if (localizedErrorMessage.equals(fieldError.getDefaultMessage())) {
      String[] fieldErrorCodes = fieldError.getCodes();
      localizedErrorMessage = fieldErrorCodes[0];
    }
    return localizedErrorMessage;
  }

}
