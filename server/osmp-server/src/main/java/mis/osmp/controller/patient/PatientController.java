package mis.osmp.controller.patient;


import mis.osmp.repository.ambulance.call.PatientRepository;
import mis.osmp.repository.ambulance.call.dto.PatientDTO;
import mis.osmp.repository.ambulance.call.filter.PatientFilter;
import mis.osmp.repository.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/ambulance")
public class PatientController {

  @Autowired
  private PatientRepository patientRepository;

  @RequestMapping(value = "patients/{id}", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
  @ResponseBody
  public PatientDTO getPatient(@PathVariable(value = "id") String id) {
    return patientRepository.getPatient(id);
  }

  @RequestMapping(value = "patients", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
  @ResponseBody
  public List<PatientDTO> filterPatient(PatientFilter filter, Page page) {
    return patientRepository.getPatients(filter, page);
  }
}
