package mis.osmp.controller.directory;

import mis.osmp.repository.ambulance.call.AccidentReasonRepository;
import mis.osmp.repository.ambulance.call.AmbulanceCallsResultsRepository;
import mis.osmp.repository.ambulance.call.ComplicationRepository;
import mis.osmp.repository.ambulance.call.dto.AccidentReasonDTO;
import mis.osmp.repository.ambulance.call.dto.AmbulanceCallResultDTO;
import mis.osmp.repository.ambulance.call.dto.ComplicationDTO;
import mis.osmp.repository.ambulance.call.filter.ComplicationFilter;
import mis.osmp.repository.directory.DirectoryItemRepository;
import mis.osmp.repository.directory.dto.DirectoryItemCreatePackageDTO;
import mis.osmp.repository.directory.dto.DirectoryItemDTO;
import mis.osmp.repository.directory.dto.DirectoryItemUpdatePackageDTO;
import mis.osmp.repository.directory.filter.DirectoryFilter;
import mis.osmp.repository.mkb10.Mkb10Repository;
import mis.osmp.repository.mkb10.dto.Mkb10DTO;
import mis.osmp.repository.mkb10.filter.Mkb10Filter;
import mis.osmp.repository.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/directory")
public class DirectoryController {

  @Autowired
  private DirectoryItemRepository directoryItemRepository;
  @Autowired
  private AmbulanceCallsResultsRepository ambulanceCallsResultsRepository;
  @Autowired
  private Mkb10Repository mkbRepository;
  @Autowired
  private AccidentReasonRepository accidentReasonRepository;
  @Autowired
  private ComplicationRepository complicationRepository;

  @RequestMapping(value = "{alias}", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
  @ResponseBody
  public List<DirectoryItemDTO> getDirectoryItems(@PathVariable(value = "alias") String alias,
                                                  final DirectoryFilter filter, final Page page) {
    return directoryItemRepository.getItems(alias, filter, page);
  }

  @RequestMapping(value = "{alias}/{id}", method = RequestMethod.DELETE, produces = "application/json;charset=utf-8")
  @ResponseStatus(HttpStatus.OK)
  public void deleteDirectoryItem(final @PathVariable(value = "alias") String alias,
                                  final @PathVariable(value = "id") String id) {
    directoryItemRepository.deleteItem(alias, id);
  }

  @RequestMapping(value = "{alias}", method = RequestMethod.POST,
      produces = "application/json;charset=utf-8", consumes = "application/json;charset=utf-8")
  @ResponseBody
  public DirectoryItemDTO createDirectoryItem(final @PathVariable(value = "alias") String alias,
                                              final @Valid @RequestBody DirectoryItemCreatePackageDTO packageDTO) {
    packageDTO.setAlias(alias);
    return directoryItemRepository.createItem(alias, packageDTO);
  }

  @RequestMapping(value = "{alias}/{id}", method = RequestMethod.PUT, consumes = "application/json;charset=utf-8")
  @ResponseStatus(HttpStatus.OK)
  public void updateDirectoryItem(final @PathVariable(value = "alias") String alias,
                                  final @PathVariable(value = "id") String id,
                                  final @Valid @RequestBody DirectoryItemUpdatePackageDTO packageDTO) {
    directoryItemRepository.updateItem(id, packageDTO);
  }


  @RequestMapping(value = "ambulance/callsresults", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
  @ResponseBody
  public List<AmbulanceCallResultDTO> getAmbulanceCallsResults() {
    return ambulanceCallsResultsRepository.getResults();
  }

  @RequestMapping(value = "mkb-10", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
  @ResponseBody
  public List<Mkb10DTO> getMkb10List(final Mkb10Filter filter, final Page page) {
    return mkbRepository.getItems(filter, page);
  }

  @RequestMapping(value = "mkb-10/{id}", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
  @ResponseBody
  public Mkb10DTO getMkb10(@PathVariable(value = "id") String id) {
    return mkbRepository.getItem(id);
  }

  @RequestMapping(value = "accidentreasons", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
  @ResponseBody
  public List<AccidentReasonDTO> getAccidentReasons() {
    return accidentReasonRepository.getReasons();
  }

  @RequestMapping(value = "complication/{id}", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
  @ResponseBody
  public ComplicationDTO getComplication(@PathVariable(value = "id") String id) {
    return complicationRepository.getComplication(id);
  }

  @RequestMapping(value = "complication", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
  @ResponseBody
  public List<ComplicationDTO> filterComplication(ComplicationFilter filter, Page page) {
    return complicationRepository.getComplications(filter, page);
  }
}
