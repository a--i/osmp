package mis.osmp.controller.exception.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class ValidationExceptionDTO extends ExceptionDTO {
  private List<FieldValidationExceptionDTO> fieldExceptions;
}
