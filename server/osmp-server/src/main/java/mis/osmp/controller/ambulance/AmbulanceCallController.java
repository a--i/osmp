package mis.osmp.controller.ambulance;


import mis.osmp.repository.ambulance.call.AmbulanceCallRepository;
import mis.osmp.repository.ambulance.call.dto.AmbulanceCallCreatePackageDTO;
import mis.osmp.repository.ambulance.call.dto.AmbulanceCallDTO;
import mis.osmp.repository.ambulance.call.dto.AmbulanceCallRowDTO;
import mis.osmp.repository.ambulance.call.filter.AmbulanceCallsFilter;
import mis.osmp.repository.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.annotation.security.RolesAllowed;
import java.util.List;

import org.springframework.http.HttpStatus;


@Controller
@RequestMapping("/ambulance")
public class AmbulanceCallController {

  @Autowired
  private AmbulanceCallRepository ambulanceCallRepository;

  @RequestMapping(value = "calls", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
  @ResponseBody
  public List<AmbulanceCallDTO> filter(AmbulanceCallsFilter filter, Page page) {
    return ambulanceCallRepository.getCalls(filter, page);
  }


  @RequestMapping(value = "calls", method = RequestMethod.POST,
      produces = "application/json;charset=utf-8", consumes = {"application/json;charset=utf-8"})
  @ResponseBody
  @RolesAllowed("ROLE_DOCTOR")
  public String createAmbulanceCall(@Valid @RequestBody AmbulanceCallCreatePackageDTO ambulanceCallPackage) throws MethodArgumentNotValidException {
    return ambulanceCallRepository.createCall(ambulanceCallPackage);
  }

  @RequestMapping(value = "calls/{id}", method = RequestMethod.PUT,
      consumes = {"application/json;charset=utf-8"})
  @ResponseStatus(HttpStatus.OK)
  public void updateAmbulanceCall(@Valid @RequestBody AmbulanceCallRowDTO ambulanceCallPackage) throws MethodArgumentNotValidException {
    ambulanceCallRepository.updateCall(ambulanceCallPackage);
  }

  @RequestMapping(value = "calls/{id}", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
  @ResponseBody
  public AmbulanceCallRowDTO getRow(@PathVariable(value = "id") String id) {
    return ambulanceCallRepository.getCall(id);
  }

}
