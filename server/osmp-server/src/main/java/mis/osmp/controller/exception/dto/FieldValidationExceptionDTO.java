package mis.osmp.controller.exception.dto;

import lombok.Data;

@Data
public class FieldValidationExceptionDTO {
  private String field;
  private String description;
}
