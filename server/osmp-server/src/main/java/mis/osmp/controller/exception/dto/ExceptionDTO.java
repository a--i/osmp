package mis.osmp.controller.exception.dto;

import lombok.Data;

@Data
public class ExceptionDTO {
  private String code;
  private String description;
}
