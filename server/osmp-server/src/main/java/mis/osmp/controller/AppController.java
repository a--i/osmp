package mis.osmp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class AppController {

  @RequestMapping(method = RequestMethod.GET, produces = "text/html;charset=utf-8")
  public String index() {
    return "index";
  }
}
