package mis.osmp.util.date;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;


public class DateFormatter {
  public static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

  public static String format(Timestamp timestamp) {
    if (timestamp == null) {
      return null;
    }
    return simpleDateFormat.format(timestamp);
  }

  public static Timestamp parse(String source) throws ParseException {
    if (source == null)
      return null;
    return new Timestamp(simpleDateFormat.parse(source).getTime());
  }
}
