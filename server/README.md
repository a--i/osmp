###Запуск сервера:

1.   в папке server выполнить команду
>mvn clean install
2.   в папке server/osmp-server выполнить команду
>mvn jetty:run

###Разработка сервера
#####Окружение сервера:
* Java 8

#####Используемые библиотеки
1. Spring framework
>DT, spring jdbc, spring mvc
2. Log4j
>Логгирование
3. Jackson
>Сериализация в JSON
4. Typesafe Config
>Конфигурация приложения
5. Lombock
>Удобство написание простых типов (например, не нужно писать getter-ы и setter-ы)
6. Servlet 3.1
7. Google Guava
>Удобство написания кода
8. HikariCP
>Пул соединения к базе (http://brettwooldridge.github.io/HikariCP/)
9. hsqldb
>Драйвер базы
10. flyway
>Миграции данных

#####Окружение разработчика
1. Tomcat 8
2. Плагин lombok для IDE
>Установка плагина http://projectlombok.org/download.html

#####Конфигурация сервера
Файл osmp.conf содержит конфигурацию севера
1. database
> * driver - драйвер базы данных
> * url - url базы данных
> * user - пользователь базы данных
> * password - пароль пользователя базы данных