package mis.osmp.repository.ambulance.call.dto;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class AccidentReasonDTO {
  private String id;
  private Integer state;
  private String code;
  private String name;
  private Integer recount;
  private Integer versionId;
  private Timestamp actionDate;
  private Timestamp createDate;
}
