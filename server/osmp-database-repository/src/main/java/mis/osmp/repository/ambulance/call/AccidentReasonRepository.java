package mis.osmp.repository.ambulance.call;


import mis.osmp.repository.ambulance.call.dto.AccidentReasonDTO;

import java.util.List;

public interface AccidentReasonRepository {
  List<AccidentReasonDTO> getReasons();
}
