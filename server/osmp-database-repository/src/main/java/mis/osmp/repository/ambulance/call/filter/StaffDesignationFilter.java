package mis.osmp.repository.ambulance.call.filter;

import lombok.Data;

@Data
public class StaffDesignationFilter {
  private String value;
}
