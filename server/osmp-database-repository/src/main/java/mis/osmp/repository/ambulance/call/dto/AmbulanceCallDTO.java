package mis.osmp.repository.ambulance.call.dto;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class AmbulanceCallDTO {
  private String id;
  private String userId;
  private String rwUserId;
  private Integer state;
  private String code;
  private Timestamp callDatetime;
  private Timestamp callPassDatetime;
  private Timestamp departureDatetime;
  private Timestamp patientArrivalDatetime;
  private Timestamp departureReturnDatetime;
  private Timestamp departureResultDatetime;
  private Integer unidentified;
  private String patient;
  private String lastName;
  private String firstName;
  private String middleName;
  private String callReason;
  private String contactPhone;
  private String dispatcher;
  private String callType;
  private String callStatus;
  private String departureTeamNumber;
  private String callPassType;
  private String resultId;
  private String rwResultId;
  private String result;
  private String diagnosisId;
  private String rwDiagnosisId;
  private String diagnosis;
  private String mkbCode;
  private String hospitalization;
  private String patientWalking;
  private String sgnId;
  }