package mis.osmp.repository.mkb10;

import mis.osmp.repository.mkb10.dto.Mkb10DTO;
import mis.osmp.repository.mkb10.filter.Mkb10Filter;
import mis.osmp.repository.pagination.Page;

import java.util.List;

public interface Mkb10Repository {
  List<Mkb10DTO> getItems(Mkb10Filter filter, Page page);
  Mkb10DTO getItem(String id);
}