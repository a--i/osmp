package mis.osmp.repository.directory.dto;

import lombok.Data;

@Data
public class DirectoryItemDTO {
  private String id;
  private String value;
}
