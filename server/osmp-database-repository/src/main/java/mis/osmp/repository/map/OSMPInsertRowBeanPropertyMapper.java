package mis.osmp.repository.map;


import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.PropertyAccessorFactory;
import org.springframework.util.StringUtils;

import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class OSMPInsertRowBeanPropertyMapper<T> {
  private final String table;
  private final T item;
  private final HashMap<String, String> mapFieldsToColumns;


  public OSMPInsertRowBeanPropertyMapper(String table, T item) {
    this(table, item, new HashMap<>());
  }

  public OSMPInsertRowBeanPropertyMapper(String table, T item, HashMap<String, String> mapFieldsToColumns) {
    this.item = item;
    this.mapFieldsToColumns = mapFieldsToColumns;
    if (!item.getClass().getPackage().getName().startsWith("mis.osmp"))
      throw new IllegalArgumentException("Mapper only for classes in mis.osmp package");
    this.table = table.toUpperCase();
  }

  public String createInsertSqlWithNamedParameters() {
    StringBuilder builder = new StringBuilder("INSERT INTO ");
    builder.append(table);

    final List<String> properties = new ArrayList<>();
    BeanWrapper bw = PropertyAccessorFactory.forBeanPropertyAccess(item);
    for (PropertyDescriptor descriptor : BeanUtils.getPropertyDescriptors(item.getClass())) {
      final String propertyName = descriptor.getName();
      if (descriptor.getWriteMethod() != null
          && descriptor.getReadMethod() != null
          && bw.getPropertyValue(propertyName) != null
          ) {
        properties.add(propertyName);
      }
    }
    if (!properties.isEmpty()) {
      builder.append("(");
      final List<String> columns = properties.stream()
          .map(p -> mapFieldsToColumns.containsKey(p) ? mapFieldsToColumns.get(p) : p)
          .filter(p -> !StringUtils.isEmpty(p))
          .collect(Collectors.toList());
      builder.append(StringUtils.collectionToDelimitedString(columns, ", "));
      builder.append(")");
      builder.append(" VALUES(");
      final List<String> values = properties.stream().map(s ->
      {
        String field = mapFieldsToColumns.containsKey(s) ? mapFieldsToColumns.get(s) : s;
        return field == null ? null : ":" + s;
      }).filter(p -> p != null).collect(Collectors.toList());
      builder.append(StringUtils.collectionToDelimitedString(values, ", "));
      builder.append(")");
    }
    return builder.toString();
  }
}
