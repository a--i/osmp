package mis.osmp.repository.pagination;


import lombok.Data;

@Data
public class Page {
  private int pageNumber = 0;
  private int recordsOnPage = 50;
  private Sort sort;

  public int getStartIndex() {
    return getPageNumber() * getRecordsOnPage();
  }
}
