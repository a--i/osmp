package mis.osmp.repository.ambulance.call;


import mis.osmp.repository.ambulance.call.dto.AmbulanceCallResultDTO;

import java.util.List;

public interface AmbulanceCallsResultsRepository {
  List<AmbulanceCallResultDTO> getResults();
}
