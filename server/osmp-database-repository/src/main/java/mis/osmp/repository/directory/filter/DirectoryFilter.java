package mis.osmp.repository.directory.filter;


import lombok.Data;

@Data
public class DirectoryFilter {
  private String value;
}
