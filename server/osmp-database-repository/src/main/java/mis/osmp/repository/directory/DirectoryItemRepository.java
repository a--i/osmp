package mis.osmp.repository.directory;


import mis.osmp.repository.directory.dto.DirectoryItemCreatePackageDTO;
import mis.osmp.repository.directory.dto.DirectoryItemDTO;
import mis.osmp.repository.directory.dto.DirectoryItemUpdatePackageDTO;
import mis.osmp.repository.directory.filter.DirectoryFilter;
import mis.osmp.repository.pagination.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface DirectoryItemRepository {
  List<DirectoryItemDTO> getItems(String alias, DirectoryFilter filter, Page page);

  @Transactional
  void deleteItem(String alias, String id);

  @Transactional
  DirectoryItemDTO createItem(String alias, DirectoryItemCreatePackageDTO packageDTO);

  @Transactional
  void updateItem(String id, DirectoryItemUpdatePackageDTO packageDTO);
}
