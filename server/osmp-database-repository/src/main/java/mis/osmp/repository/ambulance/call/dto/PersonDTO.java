package mis.osmp.repository.ambulance.call.dto;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class PersonDTO {
  private String id;
  private Timestamp birthDate;
  private PassportDTO passportDto;
  private String sex;
  private String mobilePhone;
  private String homePhone;
  private String workPhone;
  private String firstName;
  private String lastName;
  private String middleName;
  private String email;
}
