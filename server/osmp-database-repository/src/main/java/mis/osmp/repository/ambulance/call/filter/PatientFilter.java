package mis.osmp.repository.ambulance.call.filter;

import lombok.Data;

@Data
public class PatientFilter {
  private String lastName;
  private String firstName;
  private String middleName;
}
