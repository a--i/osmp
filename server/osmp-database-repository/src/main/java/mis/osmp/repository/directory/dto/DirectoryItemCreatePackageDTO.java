package mis.osmp.repository.directory.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class DirectoryItemCreatePackageDTO {
  private String id;
  @NotNull
  private String value;
  private String alias;
  private Integer style = 0;
  private String enumTypeId;
  private Integer itemOrder = 0;
}
