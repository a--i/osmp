package mis.osmp.repository.ambulance.call;


import mis.osmp.repository.ambulance.call.dto.AmbulanceCallCreatePackageDTO;
import mis.osmp.repository.ambulance.call.dto.AmbulanceCallDTO;
import mis.osmp.repository.ambulance.call.dto.AmbulanceCallRowDTO;
import mis.osmp.repository.ambulance.call.filter.AmbulanceCallsFilter;
import mis.osmp.repository.pagination.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface AmbulanceCallRepository {
  List<AmbulanceCallDTO> getCalls(AmbulanceCallsFilter filter, Page page);

  AmbulanceCallRowDTO getCall(String id);

  @Transactional
  String createCall(AmbulanceCallCreatePackageDTO ambulanceCallPackage);

  @Transactional
  void updateCall(AmbulanceCallRowDTO ambulanceCallPackage);
}
