package mis.osmp.repository.ambulance.call.filter;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class AmbulanceCallsFilter {
  private String id;
  private String signatureStatus;
  private String code;
  private String lastName;
  private String firstName;
  private String middleName;
  private String ambulanceCallResult;
  private Timestamp dateFrom;
  private Timestamp dateTo;
}
