package mis.osmp.repository.map;


import org.springframework.beans.*;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.util.Assert;

import java.beans.PropertyDescriptor;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class OSMPBeanPropertyRowMapper<T> extends BeanPropertyRowMapper<T> {

  private Class<T> mappedClass;
  private final String propertySeparator = "_";

  public OSMPBeanPropertyRowMapper(Class<T> mappedClass) {
    super(mappedClass);
    Assert.state(this.mappedClass != null, "Mapped class was not specified");
  }


  @Override
  protected void initialize(Class<T> mappedClass) {
    this.mappedClass = mappedClass;
  }

  @Override
  public T mapRow(ResultSet rs, int rowNumber) throws SQLException {
    T mappedObject = BeanUtils.instantiate(this.mappedClass);
    BeanWrapper bw = PropertyAccessorFactory.forBeanPropertyAccess(mappedObject);

    final ResultSetMetaData rsmd = rs.getMetaData();
    int columnCount = rsmd.getColumnCount();

    for (int index = 1; index <= columnCount; index++) {
      String column = JdbcUtils.lookupColumnName(rsmd, index).toLowerCase();
      mapColumnValue(rs, bw, index, column);
    }
    return mappedObject;
  }

  private void mapColumnValue(ResultSet rs, BeanWrapper bw, int index, String column) throws SQLException {
    if (isNestedPropertyColumn(column)) {
      mapNestedPropertyValue(rs, bw, index, column);
    } else {
      mapPropertyValue(rs, bw, index, column);
    }
  }

  private void mapNestedPropertyValue(ResultSet rs, BeanWrapper bw, int index, String column) throws SQLException {
    final String columnPropertyName = getPropertyName(column);
    final PropertyDescriptor pd = getPropertyDescriptorByName(bw, columnPropertyName);
    final String propertyName = pd.getName();
    final String nestedPropertyName = getNestedPropertyName(column);
    final PropertyDescriptor nestedPropertyDescriptor = getPropertyDescriptorByName(pd.getPropertyType(), nestedPropertyName);
    if (nestedPropertyDescriptor != null) {
      if (bw.getPropertyValue(propertyName) == null)
        bw.setPropertyValue(propertyName, BeanUtils.instantiate(pd.getPropertyType()));
      final Object nestedProperty = bw.getPropertyValue(propertyName);
      final BeanWrapper propertyWrapper = PropertyAccessorFactory.forBeanPropertyAccess(nestedProperty);
      mapColumnValue(rs, propertyWrapper, index, getNestedPropertyFullName(column));
    }
  }


  private void mapPropertyValue(final ResultSet rs, final BeanWrapper bw, int index, final String column) throws SQLException {

    final PropertyDescriptor pd = getPropertyDescriptorByName(bw, column);
    if (pd == null)
      return;
    try {
      final Object columnValue = getColumnValue(rs, index, pd);
      try {
        bw.setPropertyValue(pd.getName(), columnValue);
      } catch (TypeMismatchException e) {
        if (columnValue != null) {
          throw e;
        }
      }
    } catch (NotWritablePropertyException ex) {
      throw new DataRetrievalFailureException(
          "Unable to map column " + column + " to property " + pd.getName(), ex);
    }
  }

  private PropertyDescriptor getPropertyDescriptorByName(final BeanWrapper beanWrapper, final String propertyName) {
    final PropertyDescriptor[] propertyDescriptors = beanWrapper.getPropertyDescriptors();
    for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
      if (propertyDescriptor.getName().equalsIgnoreCase(propertyName))
        return propertyDescriptor;
    }
    return null;
  }

  private PropertyDescriptor getPropertyDescriptorByName(final Class type, final String propertyName) {
    final PropertyDescriptor[] propertyDescriptors = BeanUtils.getPropertyDescriptors(type);
    for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
      if (propertyDescriptor.getName().equalsIgnoreCase(propertyName))
        return propertyDescriptor;
    }
    return null;
  }

  private String getNestedPropertyName(String column) {
    return column.split(propertySeparator)[1];
  }

  private String getNestedPropertyFullName(String column) {
    return column.substring(column.indexOf(propertySeparator) + propertySeparator.length());
  }

  private String getPropertyName(String column) {
    return column.substring(0, column.indexOf(propertySeparator));
  }

  private boolean isNestedPropertyColumn(String column) {
    return column.contains(propertySeparator);
  }
}
