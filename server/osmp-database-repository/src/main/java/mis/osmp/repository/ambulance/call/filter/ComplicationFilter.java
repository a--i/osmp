package mis.osmp.repository.ambulance.call.filter;

import lombok.Data;

@Data
public class ComplicationFilter {
  private String name;
}
