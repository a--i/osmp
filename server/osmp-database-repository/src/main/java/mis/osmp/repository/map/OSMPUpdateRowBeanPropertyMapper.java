package mis.osmp.repository.map;


import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.PropertyAccessorFactory;
import org.springframework.util.StringUtils;

import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class OSMPUpdateRowBeanPropertyMapper<T> {

  private final String table;
  private final T item;
  private final HashMap<String, String> mapFieldsToColumns;


  public OSMPUpdateRowBeanPropertyMapper(String table, T item) {
    this(table, item, new HashMap<>());
  }

  public OSMPUpdateRowBeanPropertyMapper(String table, T item, HashMap<String, String> mapFieldsToColumns) {
    this.item = item;
    this.mapFieldsToColumns = mapFieldsToColumns;
    if (!item.getClass().getPackage().getName().startsWith("mis.osmp"))
      throw new IllegalArgumentException("Mapper only for classes in mis.osmp package");
    this.table = table.toUpperCase();
  }


  public StringBuilder createUpdateSqlWithNamedParameters() {
    StringBuilder builder = new StringBuilder("UPDATE ");
    builder.append(table);

    final List<String> properties = new ArrayList<>();
    BeanWrapper bw = PropertyAccessorFactory.forBeanPropertyAccess(item);
    for (PropertyDescriptor descriptor : BeanUtils.getPropertyDescriptors(item.getClass())) {
      final String propertyName = descriptor.getName();
      if (descriptor.getWriteMethod() != null
          && descriptor.getReadMethod() != null
          && bw.getPropertyValue(propertyName) != null) {
        properties.add(propertyName);
      }
    }
    if (!properties.isEmpty()) {
      builder.append(" SET ");
      final List<String> values = properties.stream()
          .map(s -> (getColumnByField(s) != null) ? getColumnByField(s) + "= :" + s : null)
          .filter(p -> p != null).collect(Collectors.toList());
      builder.append(StringUtils.collectionToDelimitedString(values, ", "));
    }
    return builder;
  }

  private String getColumnByField(String fieldName) {
    return mapFieldsToColumns.containsKey(fieldName) ? mapFieldsToColumns.get(fieldName) : fieldName;
  }
}
