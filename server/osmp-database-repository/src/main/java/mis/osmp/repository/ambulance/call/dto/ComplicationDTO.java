package mis.osmp.repository.ambulance.call.dto;
import lombok.Data;

@Data
public class ComplicationDTO {
  private String id;
  private String name;
}
