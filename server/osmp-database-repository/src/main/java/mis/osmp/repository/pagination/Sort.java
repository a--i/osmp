package mis.osmp.repository.pagination;

import lombok.Data;

@Data
public class Sort {
  private String fieldName;
  private String order;
}
