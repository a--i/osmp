package mis.osmp.repository.ambulance.call.dto;

import lombok.Data;

import java.sql.Date;

@Data
public class PassportDTO {
  private String id;
  private String passSeries;
  private String issuePlace;
  private String passNumber;
  private Date issueDate;
  private String docType;
  private Date dateTo;
}
