package mis.osmp.repository.mkb10.filter;


import lombok.Data;

@Data
public class Mkb10Filter {
  private String code;
}
