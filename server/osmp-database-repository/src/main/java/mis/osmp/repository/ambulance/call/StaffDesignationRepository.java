package mis.osmp.repository.ambulance.call;

import mis.osmp.repository.ambulance.call.dto.StaffDesignationDTO;
import mis.osmp.repository.ambulance.call.filter.StaffDesignationFilter;
import mis.osmp.repository.pagination.Page;

import java.util.List;

public interface StaffDesignationRepository {
  List<StaffDesignationDTO> getStaffDesignations(StaffDesignationFilter filter, Page page);
  StaffDesignationDTO getStaffDesignation(String id);

}
