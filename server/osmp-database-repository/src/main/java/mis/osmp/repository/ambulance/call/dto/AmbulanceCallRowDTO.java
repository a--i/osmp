package mis.osmp.repository.ambulance.call.dto;

import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

@Data
public class AmbulanceCallRowDTO {
  private String id;
  private String code;
  private Timestamp callDatetime;
  private String callStatus;
  private String callAddress;
  private String callLocation;
  private String contactPhone;
  private String whoCall;
  private String callReason;
  private String callType;
  private Timestamp callPassDatetime;
  private String callPassType;
  private String departureTeamNumber;
  private Timestamp departureDatetime;
  private String departureCarNumber;
  private String departureDelayReason;
  private Timestamp patientArrivalDatetime;
  private String patientAmbulanceCall;
  private String patientComplaint;
  private String diagnosisMKB;
  private String accidentReason;
  private String currentVisitPoliclinic;
  private String patientRenderingHelp;
  private String measure;
  private String efficacyStepComplication;
  private String patientStateAfter;
  private Timestamp departureReturnDatetime;
  private String departureDistance;
  private String ambulanceCallResult;
  private String hospitalization;
  private String hospitalizationDatetime;
  private String lpu;
  private Timestamp policeReportDatetime;
  private String policeReportSend;
  private String policeReportReceive;
  private String policeReport;
  private String staffDesignation;
  private List<String> complicationList;
}
