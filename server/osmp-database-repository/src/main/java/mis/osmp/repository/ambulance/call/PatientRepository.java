package mis.osmp.repository.ambulance.call;

import mis.osmp.repository.ambulance.call.dto.PatientDTO;
import mis.osmp.repository.ambulance.call.filter.PatientFilter;
import mis.osmp.repository.pagination.Page;

import java.util.List;

public interface PatientRepository {
  List<PatientDTO> getPatients(PatientFilter filter, Page page);

  PatientDTO getPatient(String id);
}
