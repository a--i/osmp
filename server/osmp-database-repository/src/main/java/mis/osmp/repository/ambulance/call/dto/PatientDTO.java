package mis.osmp.repository.ambulance.call.dto;
import lombok.Data;

@Data
public class PatientDTO {
  private String id;
  private String snils;
  private Integer isVip;
  private String medicalCard;
  private PersonDTO personDto;
  private String socialState;
  private Integer unidentified;
  private String defaultInsurancePolice;
  private String blood;
  private String diabetes;
  private String inn;
  private String kpp;
  private String category;
  private String warmanCode;
  private Integer isVillage;
  private PassportDTO privilegeDocumentDto;
  private String webUserId;
  private Integer countryOKSM;
  private String healthGroup;

}
