package mis.osmp.repository.ambulance.call.dto;

import lombok.Data;

@Data
public class AmbulanceCallResultDTO {
  private String id;
  private Integer withPatient;
  private String name;
  private Integer code;
}
