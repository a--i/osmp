package mis.osmp.repository.ambulance.call.dto;


import lombok.Data;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

@Data
public class AmbulanceCallCreatePackageDTO {
  private String id;
  private Integer state = 0;
  private Timestamp actionDate = new Timestamp(Calendar.getInstance().getTimeInMillis());
  private Timestamp createDate = new Timestamp(Calendar.getInstance().getTimeInMillis());
  @NotNull
  private String code;
  @NotNull
  private Timestamp callDatetime;
  @NotNull
  private String callStatus;
  private String callAddress;
  private String callLocation;
  private String contactPhone;
  private String whoCall;
  private String callReason;
  private String callType;
  private Timestamp callPassDatetime;
  private String departureTeamNumber;
  private Timestamp departureDatetime;
  private String departureCarNumber;
  private String departureDelayReason;
  private Timestamp patientArrivalDatetime;
  private String patientAmbulanceCall;
  private String patientComplaint;
  private String diagnosisMKB;
  private String accidentReason;
  private String currentVisitPoliclinic;
  private String patientRenderingHelp;
  private String measure;
  private String efficacyStepComplication;
  private String patientStateAfter;
  private Timestamp departureReturnDatetime;
  private String departureDistance;
  private String ambulanceCallResult;
  private int hospitalization;
  private Timestamp hospitalizationDatetime;
  private String lpu;
  private Timestamp policeReportDatetime;
  private String policeReportSend;
  private String policeReportReceive;
  private String policeReport;
  @NotNull
  private String staffDesignation;
  private List<String> complicationList;
}
