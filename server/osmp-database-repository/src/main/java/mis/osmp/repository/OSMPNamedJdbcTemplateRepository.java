package mis.osmp.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public abstract class OSMPNamedJdbcTemplateRepository {

  private NamedParameterJdbcTemplate jdbcTemplate;

  @Autowired
  public void setJdbcTemplate(NamedParameterJdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  protected NamedParameterJdbcTemplate getJdbcTemplate() {
    return jdbcTemplate;
  }
}
