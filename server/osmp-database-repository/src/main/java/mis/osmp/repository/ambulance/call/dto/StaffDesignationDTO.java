package mis.osmp.repository.ambulance.call.dto;

import lombok.Data;

@Data
public class StaffDesignationDTO {
  private String id;
  private String value;
}
