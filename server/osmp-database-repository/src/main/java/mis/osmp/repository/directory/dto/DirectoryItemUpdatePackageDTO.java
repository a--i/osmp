package mis.osmp.repository.directory.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class DirectoryItemUpdatePackageDTO {
  private String id;
  @NotNull
  private String value;

  public DirectoryItemUpdatePackageDTO withId(String id) {
    final DirectoryItemUpdatePackageDTO dto = new DirectoryItemUpdatePackageDTO();
    dto.setValue(value);
    dto.setId(id);
    return dto;
  }
}
