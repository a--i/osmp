package mis.osmp.repository.mkb10.dto;

import lombok.Data;

@Data
public class Mkb10DTO {
  private String id;
  private String codex;
  private String codey;
  private String diagnoseName;
  private String name;
  private String symbol;
  private String code;
}
