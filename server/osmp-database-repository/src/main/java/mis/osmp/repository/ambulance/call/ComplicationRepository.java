package mis.osmp.repository.ambulance.call;


import mis.osmp.repository.ambulance.call.dto.ComplicationDTO;
import mis.osmp.repository.ambulance.call.filter.ComplicationFilter;
import mis.osmp.repository.pagination.Page;

import java.util.List;

public interface ComplicationRepository {
  List<ComplicationDTO> getComplications(ComplicationFilter filter, Page page);
  ComplicationDTO getComplication(String id);
}
