package mis.osmp.repository.ambulance.call.dto;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class MedicalRecordDTO {
  private String id;
  private Integer state;
  private Timestamp actionDate;
  private Timestamp dateTo;
  private Integer versionId;
}
