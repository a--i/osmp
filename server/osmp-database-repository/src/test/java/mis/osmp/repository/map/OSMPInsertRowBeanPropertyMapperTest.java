package mis.osmp.repository.map;

import lombok.Data;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.*;

public class OSMPInsertRowBeanPropertyMapperTest {

  @Test(expected = IllegalArgumentException.class)
  public void testCreateInsertSqlWithNamedParametersWithException() throws Exception {
    new OSMPInsertRowBeanPropertyMapper<>("SOME_TABLE", "");
  }

  @Test
  public void testCreateInsertSqlWithNamedParameters() throws Exception {
    final SomeData packageDTO = new SomeData();
    final OSMPInsertRowBeanPropertyMapper mapper = new OSMPInsertRowBeanPropertyMapper<>("SOME_TABLE", packageDTO);
    final String query = mapper.createInsertSqlWithNamedParameters();
    assertNotNull(query);
    assertFalse(query.isEmpty());
    assertEquals("INSERT INTO SOME_TABLE(id, state) VALUES(:id, :state)", query);
  }

  @Test
  public void testCreateInsertSqlWithNamedParametersAndColumnsMaps() throws Exception {
    final SomeData packageDTO = new SomeData();
    final OSMPInsertRowBeanPropertyMapper mapper = new OSMPInsertRowBeanPropertyMapper<>("SOME_TABLE", packageDTO, new HashMap<String, String>(){{
      put("state", "column_state");
    }});
    final String query = mapper.createInsertSqlWithNamedParameters();
    assertNotNull(query);
    assertFalse(query.isEmpty());
    assertEquals("INSERT INTO SOME_TABLE(id, column_state) VALUES(:id, :state)", query);
  }

  @Data
  class SomeData {
    private String id = "1";
    private String state = "12";
  }
}