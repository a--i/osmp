package mis.osmp.repository.map;

import lombok.Data;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.*;

public class OSMPUpdateRowBeanPropertyMapperTest {

  @Test(expected = IllegalArgumentException.class)
  public void testCreateUpdateSqlWithNamedParametersWithException() throws Exception {
    new OSMPUpdateRowBeanPropertyMapper<>("SOME_TABLE", "");
  }

  @Test
  public void testCreateInsertSqlWithNamedParameters() throws Exception {
    final SomeData packageDTO = new SomeData();
    final OSMPUpdateRowBeanPropertyMapper mapper = new OSMPUpdateRowBeanPropertyMapper<>("SOME_TABLE", packageDTO);
    final StringBuilder query = mapper.createUpdateSqlWithNamedParameters();
    assertNotNull(query);
    assertFalse(query.toString().isEmpty());
    assertEquals("UPDATE SOME_TABLE SET id= :id, state= :state", query.toString());
  }

  @Test
  public void testCreateInsertSqlWithNamedParametersAndColumnMaps() throws Exception {
    final SomeData packageDTO = new SomeData();
    final OSMPUpdateRowBeanPropertyMapper mapper = new OSMPUpdateRowBeanPropertyMapper<>("SOME_TABLE", packageDTO, new HashMap<String, String>(){{
      put("state", "column_state");
    }});
    final StringBuilder query = mapper.createUpdateSqlWithNamedParameters();
    assertNotNull(query);
    assertFalse(query.toString().isEmpty());
    assertEquals("UPDATE SOME_TABLE SET id= :id, column_state= :state", query.toString());
  }

  @Data
  class SomeData {
    private String id = "1";
    private String state = "12";
  }
}