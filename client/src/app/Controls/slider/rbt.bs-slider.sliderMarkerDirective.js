﻿/// <reference path="../../../Scripts/angular.js" />

(function (ng, undefined) {

   
    function loPosition( position, orientation )
    {
        this.init( position, orientation);
   
        this.setX(this.position.position - this.size, this.getlo(), this.gethi());
    }

    loPosition.prototype =
        {
            init: function ( position, orientation) {
                this.orientation = orientation;
                this.position = position;
                 this.start = 0;
                this.size = this.orientation.getSize();
              
                this.x = 0;
            },

            setX: function (x, min, max) {
                if (x < min)
                    this.x = min;
                else
                    if (x > max)
                        this.x = max;
                    else
                        this.x = x;
            },
       
            setPosition: function () { this.orientation.setPosition(this.x); },

            resetPosition: function () {
                this.setX(this.position.position - this.size, this.getlo(), this.gethi());
                this.orientation.setPosition(this.x);
            },

            startDrag: function (event) { this.start = event.pageX - this.x; },

            move: function (event) {
                this.setX(event.pageX - this.start, this.getlo(), this.gethi());
              
                this.orientation.setPosition(this.x);
            },

      
            getlo: function () { return this.position.lo - this.size; },

            gethi: function () { return this.position.hi - this.size; },

            getPosition: function () { return this.x + this.size; }


        }
    
    function hiPosition( position, orientation)
    {

        this.init( position, orientation);
        this.setX(this.position.position, this.lo, this.hi);

    }
   
    hiPosition.prototype = ng.extend({}, loPosition.prototype, {
        resetPosition: function () {
            this.setX(this.position.position, this.getlo(), this.gethi());
            this.orientation.setPosition(this.x);
        },
        getlo: function () {
            return this.position.lo;
        },
        gethi: function () {
            return this.position.hi;
        },
        getPosition : function () { return this.x ; }
    });
    

    

    ng.module("rbt.bs-slider").directive("rbtSliderMarker",["$document", "rbt.Controls.ElementOrientation", function ($document, $orientation) {
        return     {      
            restrict:"E",
            scope : {

                lo: "=",
                hi: "=",
                position: "="
            },
            link:function(scope,element,attr)
            {
                 
                
                var parent = element.parent();
                var position;
                var orientation;
                                
                function init()
                {
                    if ( ng.isDefined(scope.hi) && ng.isDefined(scope.lo) && ng.isDefined(scope.position))
                    {
                        switch (attr.direction) {
                            
                            case "b": {
                                orientation = new ($orientation("vertical"))(element);
                                position = new hiPosition( scope, orientation); break;
                            }
                            case "r": {
                                orientation = new ($orientation("horizontal"))(element);
                                position = new hiPosition( scope, orientation); break;
                            }
                            case "t": {
                                orientation = new ($orientation("vertical"))(element);
                                position = new loPosition( scope, orientation); break;
                            }
                            case "l":
                            default: {
                                orientation = new ($orientation("horizontal"))(element);
                                position = new loPosition( scope, orientation); break;
                            }
                        }
                        position.setPosition();
                        element.on('mousedown', mousedown);
                    }

                  
                }

 
                function mousedown (event) {
                
                    event.preventDefault();

                    position.startDrag(event)
               
                    $document.on('mousemove', mousemove);
                    $document.on('mouseup', mouseup);
                    parent.on("mouseleave", mouseout);
                }

                function mouseout(enent)
                {
                    mouseup();
                }

                function mousemove(event) {
                    position.move(event);
                    scope.position = position.getPosition();
                    scope.$applyAsync( );
                }

                function mouseup() {
                
                    $document.off('mousemove', mousemove);
                    $document.off('mouseup', mouseup);
                    parent.off("mouseleave", mouseout);
                  
                
                }

              

                scope.$on("$desproy", function () {
                    $document.off('mousedown', mousedown);
                    $document.off('mousemove', mousemove);
                    $document.off('mouseup', mouseup);
                    parent.off("mouseleave", mouseout);
                });

               scope.$watch(function () {
                    if (!position)
                        init();
                    
                });
               
               scope.$watch("position",
                   function () {
                       if(position)
                         position.resetPosition();
                   }
               );
            
  
            }
        };
        
    }])

})(window.angular)