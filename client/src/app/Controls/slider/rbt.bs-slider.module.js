﻿'use strict';

(function (ng) {

    ng.module('rbt.bs-slider', ['rbt.Controls'])
    .controller('rbt.TestCnt', ['$scope',function ($scope) {

        $scope.left = {
            left:0,
            right: 500,
            position: 20
        };

        $scope.body = {
            position: 20,
            length: 100
        };

        $scope.right = {
            left: 0,
            right: 800,
            position: 200
        };

        $scope.Employee = {EmplName:'иванов Иван Иванович'};

        $scope.options = { from: 8.3, to: 20.30 };

        $scope.sliderModel = [10.15, 13.20];

        $scope.Coefficient = 0.5;

    }]);

})(window.angular);
