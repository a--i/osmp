﻿/// <reference path="../../../Scripts/angular.js" />

(function (ng, undefined) {

    

    ng.module("rbt.bs-slider")
        .directive("rbtSliderBody", ["rbt.Controls.ElementOrientation", function ($orientation) {
            return {
                restrict: "E",
                scope: {

                    position: "=",
                    length: "@",
                },

                link: function (scope, element, attr) {


                    var orientation; 

                    function init() {

                        if (ng.isDefined(scope.position) && ng.isDefined(scope.length))
                        {
                            switch (attr.orientation) {
                                case "v": {
                              
                                    orientation = new ($orientation("vertical"))(element);
                                    break;
                                }
                                case "h":
                                default:
                                    {
                                        orientation = new ($orientation("horizontal"))(element);
                                        break;
                                    }
                            }

                            orientation.setSize(scope.length);
                            orientation.setPosition(scope.position);
                        }
                    }

                    
                    scope.$watch(function () {
                        if (!orientation) init();
                    })

                    scope.$watch("position", function (newVal, oldVal) {
                        if (newVal != oldVal)
                            orientation.setPosition(scope.position);
                          
                    })

                    scope.$watch("length", function (newVal, oldVal) {
                        if(newVal!=oldVal)
                        {
                            orientation.setSize(scope.length);
                        }
                    })
                }
            };
        }]);

})(window.angular)