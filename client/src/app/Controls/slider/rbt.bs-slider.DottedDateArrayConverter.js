﻿/// <reference path="../../../Scripts/angular.js" />
(function (ng, undefined) {


    ng.module("rbt.bs-slider").config(["rbt.Controls.ValueConverterProvider", function (converterProvider) {

        var converter = new (converterProvider.$get()("DottedDateConverter"))();

        function DottedDateArrayConverter()
        { }
        DottedDateArrayConverter.prototype = ng.extend({}, converterProvider.ConverterPrototype, {
            fromValue: function (value) {
                var ret = [];
                for (var key in value)
                    ret[key] = converter.fromValue(value[key]);
                return ret;
            },
            toValue: function (value) {
                var ret = [];
                for (var key in value)
                    ret[key] = converter.toValue(value[key]);
                return ret;
            }

        });

        converterProvider.Add("DottedDateArrayConverter", DottedDateArrayConverter);


    }]);


})(window.angular)