﻿/// <reference path="../../../Scripts/angular.js" />

(function (ng, Math, undefined) {

    /*
    все что не относилось непосредственно к директиве и м.б использовано в других директивах вынесенно в отдельные сервисы и директивы.
    в частности добавден сервис пересчета координат в экранные, позиционирование элемента вынесено в сервис ориентации 
    конвертирование значений модели вынесено в отдельную директиву и сервис
    */
    ng.module("rbt.bs-slider")
        .directive("rbtSlider", ["$document", "rbt.Controls.Resizer", "rbt.Controls.Round", "rbt.Controls.ElementOrientation", "rbt.Controls.ValueConverter", function ($document, $resizer, $round, $orientation, $valueConverter) {
            return  {
                restrict: "AE",
                scope: {},
                require: "ngModel",
                replace:  true,
                template: ["<div class=\"rbt-slider\" rbt-model-converter=\"DottedDateArrayConverter\">",
                                   "<rbt-slider-marker lo=\"lo\" hi=\"positions[1]\" position=\"positions[0]\" class=\"square rbt-slider-marker-primary\"></rbt-slider-marker>",
                                   "<rbt-slider-marker  lo=\"positions[0]\" hi=\"hi\" position=\"positions[1]\"  direction=\"r\" class=\"square rbt-slider-marker-primary\"></rbt-slider-marker>",
                                   "<rbt-slider-body orientation=\"h\" position=\"positions[0]\" length=\"{{positions[1]-positions[0]}}\" class=\"rbt-slider-body-danger rbt-slider-stripped\" />",
                                 "<div>"

                ].join(""),
                link: function (scope, element, attr, ngModel) {
            
                var converter = new ($valueConverter("DottedDateConverter"))();// new dateTimeNumber();
                  
                var orientation = new ($orientation("horizontal") )(element);
                     
                var currentSize = orientation.getSize();

                var min = converter.fromValue(attr.min);
                var max = converter.fromValue(attr.max);
                var step = attr.step?converter.fromValue(attr.step):undefined;

                var resizer = new $resizer(  min, max, currentSize);



                var round = new $round(attr.precision, step);

                scope.lo=0;
                scope.positions = [0, 0];
                scope.hi =  orientation.getSize();
            
                    /// переходим из экранных координат в координаты модели
                ngModel.$parsers = [].concat(function (val) {
                    var ret = []

                    for (var key in val)
                        ret.push(round.roundToStep(resizer.toValue(val[key])));

                    return ret;
                }, ngModel.$parsers);


                    // переводим число в экранные координаты
                ngModel.$formatters = [].concat (function (val) {

                    var ret =[];
                    for (var key in val) {
                        ret.push(resizer.fromValue(Number(val[key])));
                    }
                
                    return ret;
                }, ngModel.$formatters);


                 
                    /// ngModel.$modelValue  хрянятся значения интервала в начальных величинах
                    /// при изменении значений координат инициируем процесс пересчета, путем пересоздания объекта. 
                    /// реагируем на изменение модели. 
                scope.$watch(function (scope) {
                    return ngModel.$modelValue;
                }, function (value) {
                    var ret= []
                    ng.copy(ngModel.$modelValue, ret);
                    ngModel.$modelValue = ret;
                }, true)


                    /// отслеживаем изменение значений координат
                    /// в ngModel.$viewValue находятся экранные координаты
                    /// при изменении координат обновляем зачение в ngModel.$viewValue 
                    /// инициируя процесс пересчета экранный координат в значения модели
                scope.$watchCollection("positions", function (newPos) {
                    var val = []
                    ng.copy(newPos, val);
                    ngModel.$setViewValue(val, "change");
                });


                    /// отслеживаем изменение размеров элемента
                scope.$watch(function () {
                    var val = orientation.getSize();
                    return val;
                }, function (value) {
                    if (value != currentSize)
                    {
                        resizer.reset(min, max, value);
                        ngModel.$render();
                    }
                })

                    /// подписываемся на событие изменения размеров окна для уведомления об изменениях размеров
                ng.element(window).on("resize", function () { scope.$applyAsync(); });
            
          
                    /// обновление
                ngModel.$render = function () {  
                    var val = ngModel.$viewValue;
                    if (val) {
                        if (ng.isArray(val)) {
                            if (val.length == 0)
                            {
                                ngModel.$setViewValue([0,0], "change");
                            }
                            scope.positions = ng.copy(val, scope.positions);
                 
                        }
                        else throw "notSupported model"; 
                    }
                }

                    /// отписываемся от событий DOM
                scope.$on("destroy", function () {
                    ng.element(window).off("resize", onResize);
                })

            }
            }
        }]
        );
    

})(window.angular, window.Math)