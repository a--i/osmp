/**
 * Created by ppavlov on 09.12.2014.
 */
'use strict';
(function (ng, undefined) {

  var wrapperTpl='<p class="input-group"></p>';

  var  buttonTpl ='<span class="input-group-btn">'+
  '<button type="button" class="btn btn-default" ng-click="Open($event)"><i class="glyphicon glyphicon-calendar"></i></button>'+
  '</span>';


  ng.module('rbt.Controls').directive('rbtDatepickerBtn',['$compile', function($compile){

    return {
      restrict: 'A',
      priority:10,

      compile:function(tElement, tAttr) {

        var el =  ng.element(wrapperTpl);
        var btn = $compile(ng.element(buttonTpl));
        tAttr.$set('isOpen', 'isOpen');

        tElement.removeAttr('rbt-datepicker-btn');
        tElement.attr({'datepicker-append-to-body':true});
        var compiled =  $compile(tElement);

        tElement.replaceWith(el);



        return function (scope, element) {
          var datepicker=compiled(scope);

          element.append(datepicker);
          datepicker.after(btn(scope));
          scope.isOpen = false;
          scope.Open = function (event) {
            event.preventDefault();
            event.stopPropagation();
            scope.isOpen= !scope.isOpen;
          };
        };
      }
    };

  }]);

})(window.angular);
