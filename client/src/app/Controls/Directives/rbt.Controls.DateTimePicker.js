/**
 * Created by ppavlov on 15.12.2014.
 */
'use strict';
(function (ng, undefined) {

  var dropDownWrapTpl = '<div class="dropdown"  dropdown  label-width="4" ></div>';
  var inputGroupTpl = '<div class="input-group"></div>';
  var buttonTpl = '<span class="input-group-addon"><a class="dropdown-toggle"  dropdown-toggle="" id="dropdown2" role="button" data-toggle="dropdown" data-target="#" href="#"><i class="glyphicon glyphicon-calendar"></i></a></span>';
  var calendarWrapTpl = '<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel"></ul>';
  var calendarTpl = '<datetimepicker  data-datetimepicker-config="{minuteStep:1}" />';


  ng.module('rbt.Controls')
    .directive('rbtDatetimePicker', ['$compile', '$injector', function ($compile, $injector) {
      return {
        priority: 5,
        compile: function (tElement, tAttr) {
          tElement.removeAttr('rbt-datetime-picker');
          var modelAttr = tAttr.ngModel;
          var self = this;

          angular.forEach(tAttr.$attr, function (value, attr) {
            var ddo = $injector.has(attr + 'Directive') ? $injector.get(attr + 'Directive')[0] : {};

            if (ddo.priority && ddo.priority >= self.priority) {
              tElement.removeAttr(value);
            }
          });

          var dropDownWrap = ng.element(dropDownWrapTpl);
          var inputGroup = ng.element(inputGroupTpl);
          var button = ng.element(buttonTpl);
          var calendarWrap = ng.element(calendarWrapTpl);
          var calendar = ng.element(calendarTpl);

          calendar.attr({'ng-model': modelAttr});
          calendarWrap.append(calendar);

          dropDownWrap.append(inputGroup);
          var inp = tElement.clone();
          inputGroup.append(inp);

          inp.after(button);

          inputGroup.after(calendarWrap);
          var compiled = $compile(dropDownWrap);


          return function (scope, element) {
            element.replaceWith(compiled(scope));

          };

        }
      };
    }]);


})(window.angular);
