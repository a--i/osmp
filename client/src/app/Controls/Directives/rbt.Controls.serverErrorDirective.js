/**
 * Created by ppavlov on 18.12.2014.
 */
'use strict';
(function (ng, undefined) {

  var directiveName = 'rbtServerError';

  ng.module('rbt.Controls')
    .directive('rbtServerError', ['rbt.Controls.serverError', function (errorsService) {

      return {
        require: ['?ngModel', '?form'],
        link: function (scope, element, attr, controllers) {


          var errorKey = attr[directiveName];
          if (!errorKey) {
            throw 'Need errorKey';
          }

          errorsService.Clean(errorKey);

          var ngModel = controllers[0];
          var ngForm = controllers[1];

          var errors = [];

          function cleanErrors() {
            errorsService.clearError(errorKey, ngModel.$name);
            ng.forEach(errors, function (error) {
              ngModel.$setValidity(error, true);
            });
            errors = [];
          }

          if (ngModel) {


            scope.$watch(function () {
                return ngModel.$viewValue;
              },
              cleanErrors
            );
            scope.$watch(
              function () {
                return errorsService.getErr(errorKey);
              }, function () {
                var error = errorsService.getControlError(errorKey, ngModel.$name);
                if (error) {
                  if (ng.isArray(error)) {
                    ng.forEach(error, function (err) {
                      ngModel.$setValidity(err.description, false);
                      errors.push(err.description);
                    });
                  }
                  else {
                    ngModel.$setValidity(error.description, false);
                    errors.push(error.description);
                  }

                }
                else {
                  cleanErrors();
                }

              }
            );


          }
          else if (ngForm) {
            scope.$watch(
              function () {
                return errorsService.getCommonError(errorKey);
              },
              function (error) {
                if (error) {
                  ngForm.$serverError = error;
                  ngForm.$hasServerError = true;
                }
                else {
                  ngForm.$hasServerError = false;
                }
              }, true);

          }
          else {
            throw 'directive need ngModel or ngForm';
          }
        }
      };

    }]);

})(window.angular);
