'use strict';

(function (ng, undefined) {


  ng.module('rbt.Controls')

    .directive('rbtSelectable', [function () {

      var directiveObject = {
        restrict: 'A',
        scope: {
          rbtSelectable: '&'

        },
        link: function (scope, element, attr) {
          if (!attr.ngRepeat) { return; }

          function setSelected(sel) {
            scope.$parent.$Selected = sel;
            scope.$parent.$apply();
          }


          var multi = attr.rbtMultiselect === 'true';
          scope.$parent.$Selected = false;


          if (!multi) {
            scope.$on('rbtSelectable.select', function (evt, arg) {

              setSelected(arg === scope);

            });
          }

          element.on('click', function () {

            if (multi) {
              setSelected(!scope.$parent.$Selected);
            }
            else {
              var ps = scope.$parent.$parent ? scope.$parent.$parent : scope.$rootScope;
              ps.$broadcast('rbtSelectable.select', scope);
            }
            if (scope.$parent.$Selected) {
              scope.rbtSelectable();
            }
          });

          scope.$on('$destroy', function () { element.off('click'); });
        }
      };

      return directiveObject;

    }]);

})(window.angular);
