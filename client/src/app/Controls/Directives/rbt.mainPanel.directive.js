'use strict';

(function (ng, undefined) {

  ng.module('rbt.Controls')
    .directive('mainPanel', function () {
      return {
        restrict: 'A',
        link: function (scope, element) {


          scope.$on('wnd.resize', function (event, data) {

            element.toggleClass('full', data === 'full');
            element.toggleClass('collapsed', data !== 'full');

          });
          scope.$on('$destroy', function () {
            scope.$off('wnd.resize');
          });

        }
      };
    });

})(window.angular);
