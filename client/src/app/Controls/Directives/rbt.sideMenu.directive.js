'use strict';

(function (ng, undefined) {

  ng.module('rbt.Controls')
    .directive('sideMenu', ['$rootScope', function ($rootScope) {
      return {
        restrict: 'A',
        link: function (scope, element) {

          function show() {
            $rootScope.$broadcast('wnd.resize', 'full');
          }

          function hide() {
            $rootScope.$broadcast('wnd.resize', 'collapsed');
          }

          element.on('mouseover', show);
          element.on('mouseleave', hide);

          scope.$on('$destroy', function () {
            element.off('mouseover', show);
            element.off('mouselive', hide);
          });

        }
      };
    }]);

})(window.angular);
