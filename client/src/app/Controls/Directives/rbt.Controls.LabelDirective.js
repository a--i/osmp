/**
 * Created by ppavlov on 08.12.2014.
 */
'use strict';
(function (ng, undefined) {

  var labelTempl = '<label for="{{scLabel.id}}" ng-class="[\'control-label\', \'col-md-\'+scLabel.labelWidth]">{{scLabel.rbtControlLabel}}</label>';

  var  transcludePlaceholder =  '<div ng-class="[\'col-md-\'+scLabel.controlWidth]">';



   ng.module('rbt.Controls').directive('rbtControlLabel', ['$compile', function($compile){


     return {
       restrict:'A',
       transclude:'element',
       scope:true,
       priority:10,
       compile:function(tElement, tAttr)
       {
         var _scope = {};

         if( ng.isDefined(tAttr.labelControlId))
         {
           _scope.id= tAttr.labelControlId;

         }
         else {
           _scope.id = tAttr.id;
         }
         _scope.rbtControlLabel = tAttr.rbtControlLabel;
         _scope.labelWidth = tAttr.labelWidth;
         _scope.controlWidth = 12-_scope.labelWidth;

         var wrap = $compile(ng.element(transcludePlaceholder));

         tElement.replaceWith(ng.element(labelTempl));


         return function (scope,element,attr, nullCnt, transcludeFn)
         {
           scope.scLabel = _scope;

           $compile(element)(scope);

           transcludeFn(function(clone){
             var wrapped = wrap(scope).append(clone);
             element.after(wrapped);
           });


         };
       }
     };



   }]);

})(window.angular);
