﻿'use strict';

(function (ng, undefined) {

    /// директива добавляющая конвертр значений в контроллер модели
    ng.module('rbt.Controls').directive('rbtModelConverter', ['rbt.Controls.ValueConverter', function ($valueConverter) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attr, ngModel) {

                var converterName = attr.rbtModelConverter;
                if (converterName)
                {
                    var converter = new ($valueConverter(converterName))();

                    ngModel.$formatters.push(converter.fromValue);

                    ngModel.$parsers.push(converter.toValue);
                }

            }
        };
    }]);

})(window.angular);
