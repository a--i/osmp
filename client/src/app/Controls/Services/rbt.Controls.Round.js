﻿/// <reference path="../../../Scripts/angular.js" />
(function (ng, undefined) {


    /// реализация сервиса округления с заданной т очностью

    function Round(precision, step) {
        if (!ng.isUndefined(precision)) {
            if (precision == 0)
                this.sup = 1;
            else
                this.sup = Math.pow(10, precision);
        }
        else
            this.sup = 100;
        if (ng.isDefined(step))
            this.step = step * this.sup;
    }

    Round.prototype = {

        roundNormalize: function (value) {
            if (ng.isDefined(this.sup)) {
                return Math.round(value * this.sup);
            }
            return value;
        },

        denormalize: function (value) {
            if (ng.isDefined(this.sup)) {
                return (value / this.sup);
            }
            return value;
        },

        // округление до precision знаков
        round: function (value) {

            return this.denormalize(this.roundNormalize(value));

        },

        // округление до блидайшего шага
        roundToStep: function (value) {
            var norm = this.roundNormalize(value);
            if (ng.isDefined(this.step)) {

                var d = norm % this.step;
                if (d) {

                    return this.denormalize((d > this.step / 2) ? norm + this.step - d : norm - d);

                }

            }
            return this.denormalize(norm);
        }

    };



    ng.module("rbt.Controls")

        .factory("rbt.Controls.Round", function () {
            return Round;
        });


})(window.angular)