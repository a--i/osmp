﻿/// <reference path="../../../Scripts/angular.js" />
(function (ng, undefinde)
{
    // реадизация сервиса пересчета координат 
    // преобразование натуральных координат в  экранные
    function Resizer(minarg, maxarg, size) {
        this.reset(minarg, maxarg, size);

    }

    Resizer.prototype = {
        reset: function (minarg, maxarg, size) {
            this.size = size;
            this.min = Number(minarg);
            if (isNaN(this.min)) throw "min required";

            this.max = Number(maxarg);
            if (isNaN(this.max)) throw "max required";

            if (this.max < this.min) throw "max< min";

            var length = this.max - this.min;

            this.scale = length / size;
        },

        fromValue: function (value) {

            return Math.round((value - this.min) / this.scale);
        },
        toValue: function (position) {
            return position * this.scale + this.min;
        }
    };

    // сервис пересчета координат
    ng.module("rbt.Controls")
		.factory("rbt.Controls.Resizer", function () {
		    return Resizer;

		})

}
)(window.angular)