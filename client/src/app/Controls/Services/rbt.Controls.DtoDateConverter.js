/**
 * Created by ppavlov on 17.12.2014.
 *
 * зависит от moment.js
 *
 * Конвертация даты,времени из формата дто MM/DD/YYYY HH:mm:ss
 *
 */
'use strict';
(function (ng, moment, undefined) {

  var dtoDateFormat = 'MM/DD/YYYY HH:mm:ss';

  ng.module('rbt.Controls').config(['rbt.Controls.ValueConverterProvider', function (converterProvider) {


    function DtoDateConverter() {

    }

    DtoDateConverter.prototype = ng.extend({}, converterProvider.ConverterPrototype, {
      fromValue: function (date) {
        if (date) {
          return moment(date).format(dtoDateFormat);
        }
        return date;

      },
      toValue: function (string) {
        if (string) {
          return moment(string, dtoDateFormat).toDate();
        }
        return string;
      }

    });

    converterProvider.Add('DtoDateConverter', DtoDateConverter);


  }]);

})(window.angular, window.moment);
