﻿/// <reference path="../../../Scripts/angular.js" />
(function (ng, undefined) {

    /// конвертр даты в формате часы.минуты в Number
    ng.module("rbt.Controls").config(["rbt.Controls.ValueConverterProvider", function (converterProvider) {


        function DottedDateConverter()
        { }
        DottedDateConverter.prototype = ng.extend({}, converterProvider.ConverterPrototype, {
            fromValue: function (number) {
                var parts = (number + "").split(".");
                var min = 0;
                if (parts[1]) {
                    parts[1] = parts[1].substr(0, 2);
                    if (parts[1].length == 1) parts[1] += "0";

                    min = Number(parts[1]) / 60;
                }
                return Number(parts[0]) + min;
            },
            toValue: function (number) {
                var parts = (number + "").split(".");
                var min = 0;
                if (parts[1])
                    min = Math.round(Number("0." + parts[1]) * 60) / 100;
                return Number(parts[0]) + min;
            }

        });
	 
		converterProvider.Add("DottedDateConverter", DottedDateConverter);


	}]);


})(window.angular)