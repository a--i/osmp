﻿/// <reference path="../../../Scripts/angular.js" />
(function (ng, undefined) {

    

    function ValueConverter() { }

    ValueConverter.prototype = {
        fromValue: function (value) { throw "Not implimented"; },
        toValue: function (value) { throw "Not implimented"; }
    }

      /// Провайдер конвертеров значений

    ng.module("rbt.Controls").provider("rbt.Controls.ValueConverter", function () {
        var converters =[]
        return {

         ConverterPrototype: ValueConverter.prototype,   
         
        Add : function (name, converter) {
            if (converters[name]) throw "alredy has converter " + name;
            converters[name] = converter;
        },

        $get : function () {
            
            return function (name) {
                return converters[name];
            }
        }
        }
    });

})(window.angular)