/**
 * Created by ppavlov on 15.12.2014.
 */
'use strict';
(function (ng, moment, undefined) {

  /// конвертр даты в формате часы.минуты в Number
  ng.module('rbt.Controls').config(['rbt.Controls.ValueConverterProvider', function (converterProvider) {


    function DateTimeConverter() {

    }

    DateTimeConverter.prototype = ng.extend({}, converterProvider.ConverterPrototype, {
      fromValue: function (date) {
        if (date) {
          return moment(date).format('DD.MM.YYYY HH:mm');
        }
        return date;

      },
      toValue: function (viewValue) {
        if (viewValue) {
          return moment(viewValue, 'DD.MM.YYYY HH:mm:ss').toDate();
        }
        return viewValue;
      }

    });

    converterProvider.Add('DateTimeConverter', DateTimeConverter);


  }]);

})(window.angular, window.moment);
