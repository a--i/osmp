/**
 * Created by ppavlov on 18.12.2014.
 */
'use strict';
(function (ng, undefined) {


  function ServerError($filter, $q) {

    var errors = {};

    var that = this;

    this.getErr = function (key) {
      return errors[key];
    };

    this.getControlError = function (key, name) {
      if (errors[key]) {

        var errs = $filter('filter')(errors[key].fieldExceptions, {field: name}, true);
        if (errs && errs.length > 0) {
          return errs;
        }
      }
    };

    this.Clean = function (key) {
      delete errors[key];
    };

    this.getCommonError = function (key) {
      if (errors[key]) {
        return {
          code: errors[key].code,
          description: errors[key].description

        };
      }
    };

    this.clearError = function (key, name) {
      if (errors[key]) {
        ng.forEach(errors[key].fieldExceptions, function (item, index) {
          if (item.field === name) {
            delete errors[key].fieldExceptions[index];
          }
        });
      }
    };


    this.setError = function (key, data) {
      if (data.error && data.error.code) {
        errors[key] = data.error;
      }
    };


    this.getInterceptor = function (key) {
      var iKey = key;
      return {
        response: function () {
          that.Clean(iKey);
        },
        responseError: function (response) {

          if (response.data) {

            that.setError(iKey, response.data);

          }

          $q.reject(response);
        }
      };


    };

  }


  ng.module('rbt.Controls')
    .service('rbt.Controls.serverError', ['$filter', '$q', ServerError]);

})(window.angular);
