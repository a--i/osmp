﻿/// <reference path="../../../Scripts/angular.js" />

(function (ng,undefined)
{
  
    

    /// элемент с горизонтальной ориентацией
    function horizontalOrientztion(element) {

        if (!ng.isElement) throw "element must be DOM or wrapped Wrapped dom";
              this.element = element;
    }

    horizontalOrientztion.prototype = {
        
        /// линейный размер
        getSize: function () {
            return this.element[0].clientWidth;
        },

        
        setSize: function (value) {
            this.element.css({ width: value + "px" });
        },

        // линейная координата - лефт
        setPosition: function (value) { this.element.css({ left: value + "px" }); },

        getPosition: function () {
            var ret = this.element.css("left")
            if (!ret)
                ret = this.element[0].offsetLeft - this.element[0].iffsetParent.clientLeft;
            return ret;
        },

        /// перемещение на значение
        move: function (value) {
            this.setPosition(this.getPosition() + value);
        }
    }

    // элемент в вертикальной ориентации
    function verticalOrientation(element)
    {
        horizontalOrientztion(element);
    }

    verticalOrientation.prototype = ng.extend({}, horizontalOrientztion.prototype,
        {
            /// линейный размер -высота
            getSize: function () {
                return this.element[0].clientHeight;
            },

            setSize: function (value) {
                this.element.css({ height: value + "px" });
            },

            /// линейная координата - ТОП
            setPosition: function (value) { this.element.css({ top: value + "px" }); },

            getPosition: function () {
                var ret = this.element.css("top")
                if (!ret)
                    ret = this.element[0].offsetTop - this.element[0].offsetParent.clientTop;
                return ret;
            }
        });
   
    /// фабрика сервисов ориентации 
    ng.module("rbt.Controls").factory("rbt.Controls.ElementOrientation", function () {

        return function(orientation)
        {
            switch(orientation)
            {
                case "horizontal": return horizontalOrientztion;
                case "vertical": return verticalOrientation;
                    default: throw "unknown orientation"
            }
        }

    });

}
)(window.angular)