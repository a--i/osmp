/**
 * Created by Andrey Petkevich on 15.12.2014.
 */
'use strict';
(function (ng, undefined) {

  ng.module('rbt.topnav')
    .controller('rbt.topnav.ctrl', [
    '$scope', 'rbt.common.osmpTabService', 'rbt.common.tabType', 'rbt.topnav.config',
    function ($scope, tabService, TabType, config) {

      $scope.modules = config;
      $scope.Navigate = function (view) {

        if (view) {
          tabService.openTab(new TabType(view));
        }
      };

    }]);
})(window.angular);
