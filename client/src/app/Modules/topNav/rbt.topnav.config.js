/**
 * Created by Andrey Petkevich on 15.12.2014.
 */
'use strict';
(function (ng, undefined) {

  ng.module('rbt.topnav')
    .value('rbt.topnav.config', [
      {
        name: 'Настройки',
        glyph: 'glyphicon-wrench',
        operations: [
          {
            name: 'Справочники',
            glyph: 'glyphicon-list-alt',
            view: {
              id: 'SD',
              title: 'Типовые справочники',
              name: 'DictionariesCtrl',
              tpl: '/app/Modules/dict/rbt.dict.tpl.html'
            }
          },
          {
            name: 'МКБ-10',
            glyph: 'glyphicon-list-alt',
            view: {
              id: 'SD',
              title: 'МКБ-10',
              name: 'DictionariesMkbCtrl',
              tpl: '/app/Modules/mkbDict/rbt.mkbdict.tpl.html'
            }
          }
        ]
      },
      {
        name: 'Рабочие столы',
        glyph: 'glyphicon-briefcase',
        operations: [
          {
            name: 'Основной',
            glyph: 'glyphicon-time'
          },
          {
            name: 'Документ 1',
            glyph: 'glyphicon-time'
          },
          {
            name: 'Документ 2',
            glyph: 'glyphicon-list'
          }
        ]
      },
      {
        name: '',
        glyph: 'glyphicon-question-sign'
      }
    ]
  );

})(window.angular);
