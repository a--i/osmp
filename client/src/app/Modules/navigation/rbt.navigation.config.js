/**
 * Created by ppavlov on 05.12.2014.
 */
'use strict';
(function (ng, undefined) {

  ng.module('rbt.navigation')
    .value('rbt.navigation.config', [

      {
        name: 'Регистратура',
        glyph: 'glyphicon-phone-alt',
        operations: [
          {
            name: 'Запись на прием',
            glyph: 'glyphicon-time'
          },
          {
            name: 'Расписание',
            glyph: 'glyphicon-time'
          },
          {
            name: 'Пациенты',
            glyph: 'glyphicon-list'
          }
        ]
      },
      {
        name: 'ОСМП',
        glyph: 'glyphicon-asterisk',
        view: {
          id: 'OL',
          title: 'Журнал вызовов',
          name: 'osmpList',
          tpl: '/app/Modules/osmp/journal/rbt.osmp.journal.tpl.html'
        }
      }
    ]
  );

})(window.angular);
