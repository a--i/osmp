/**
 * Created by ppavlov on 05.12.2014.
 */
'use strict';
(function (ng, undefined) {

  ng.module('rbt.navigation').controller('rbt.navigation.ctrl', [
    '$scope', 'rbt.common.osmpTabService', 'rbt.common.tabType', 'rbt.navigation.config',
    function ($scope, tabService, TabType, config) {

      $scope.modules = config;
      $scope.Navigate = function (view) {

        if (view) {
          tabService.openTab(new TabType(view));
        }
      };

    }]);
})(window.angular);
