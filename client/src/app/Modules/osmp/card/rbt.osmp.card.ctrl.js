'use strict';

(function (ng, moment, undefined) {


  var carddefs = {
    callDatetime: new Date(),
    callPassDatetime: new Date(),
    departureDatetime: new Date()
  };

  var fakeUser = 'Федулова В';

  function convertFromData(data, dtoDateConverter) {
    var ret = ng.copy(data);

    ret.callDatetime = dtoDateConverter.toValue(ret.callDatetime);
    if (ret.callPassDatetime) {
      ret.callPassDatetime = dtoDateConverter.toValue(ret.callPassDatetime);
    }
    if (ret.departureDatetime) {
      ret.departureDatetime = dtoDateConverter.toValue(ret.departureDatetime);
    }
    if (ret.patientArrivalDatetime) {
      ret.patientArrivalDatetime = dtoDateConverter.toValue(ret.patientArrivalDatetime);
    }
    if (ret.departureReturnDatetime) {
      ret.departureReturnDatetime = dtoDateConverter.toValue(ret.departureReturnDatetime);
    }
    if (ret.hospitalizationDatetime) {
      ret.hospitalizationDatetime = dtoDateConverter.toValue(ret.hospitalizationDatetime);
    }
    if (ret.policeReportDatetime) {
      ret.policeReportDatetime = dtoDateConverter.toValue(ret.policeReportDatetime);
    }

    ret.departureDistance = Number(ret.departureDistance);
    ret.hospitalization = Number(ret.hospitalization);

    return ret;
  }

  function convertToData(card, dtoDateConverter) {
    var ret = ng.copy(card);
    ret.callDatetime = dtoDateConverter.fromValue(ret.callDatetime);
    if (ret.callPassDatetime) {
      ret.callPassDatetime = dtoDateConverter.fromValue(ret.callPassDatetime);
    }
    if (ret.departureDatetime) {
      ret.departureDatetime = dtoDateConverter.fromValue(ret.departureDatetime);
    }
    if (ret.patientArrivalDatetime) {
      ret.patientArrivalDatetime = dtoDateConverter.fromValue(ret.patientArrivalDatetime);
    }
    if (ret.departureReturnDatetime) {
      ret.departureReturnDatetime = dtoDateConverter.fromValue(ret.departureReturnDatetime);
    }
    if (ret.hospitalizationDatetime) {
      ret.hospitalizationDatetime = dtoDateConverter.fromValue(ret.hospitalizationDatetime);
    }
    if (ret.policeReportDatetime) {
      ret.policeReportDatetime = dtoDateConverter.fromValue(ret.policeReportDatetime);
    }
    return ret;
  }

  ng.module('rbt.osmp.module')
    .controller('OsmpCardCtrl', [
      'rbt.common.staffDesignationSrv',
      'rbt.Controls.ValueConverter',
      'rbt.osmp.data',
      'rbt.osmp.patientService',
      'rbt.common.dictionaryCallsResultsSrv',
      '$timeout',
      'rbt.osmp.mkbService',
      'rbt.osmp.complicationService',
      '$log',
      'rbt.common.dictionarySrv',
      'rbt.common.osmpTabService',
      'rbt.common.tabType',
      'rbt.dictAliases',
      '$modal',
      '$scope',
      function (staff, converter, dataService, patientService, callResults, $timeout, MkbService, complicationService, $log, dictionary, tabService, TabType, aliaces, $modal, $scope) {

        var dtoDateConverter = new (converter('DtoDateConverter'))();

        function toId(val) {
          var ret = '';
          for (var i = 0; ng.isDefined(val[i]); i++) {
            ret += val[i];
          }
          return ret;
        }

        function renameTab() {
          var oldTab = {
            'name': 'OsmpCard'

          };
          setId(oldTab);


          var newTab = {
            id: $scope.card.id,
            title: 'Карточка вызова ' + $scope.card.code,
            name: 'OsmpCard',
            params: {id: $scope.card.id}
          };

          tabService.renameTab(oldTab, newTab);
        }

        function loadCard(cardId) {
          var card = dataService.get({id: cardId}, function () {
            $scope.card = convertFromData(card, dtoDateConverter);
            $scope.MKB.setCode($scope.card.diagnosisMKB);
          });
        }

        function createCard() {
          $scope.card = ng.extend($scope.card, carddefs);
        }

        $scope.showValidation = false;

        $scope.card = {};

        $scope.MKB = new MkbService($scope);

        $scope.patient = new patientService($scope);

        $scope.complication = new complicationService($scope);

        $scope.AccStatuses = {

          call: {
            isOpen: true,
            isLoaded: false,
            load: function ($scope, dictionaries, aliaces) {
              if (!this.isLoaded) {
                $scope.statuses = dictionaries.dictionary.query({alias: aliaces.statuses.alias});

                $scope.places = dictionaries.dictionary.query({alias: aliaces.places.alias});


                $scope.calltypes = dictionaries.dictionary.query({alias: aliaces.calltypes.alias});

                $scope.transfers = dictionaries.dictionary.query({alias: aliaces.transfers.alias});

                $scope.brigades = dictionaries.dictionary.query({alias: aliaces.brigades.alias});

                $scope.autonums = dictionaries.dictionary.query({alias: aliaces.autonums.alias});

                $scope.delayreasons = dictionaries.dictionary.query({alias: aliaces.delayreasons.alias});
                this.isLoaded = true;
              }
            },

            hasErrors: function (form) {
              if (ng.isDefined(form.callDatetime) && ng.isDefined(form.callStatus) && form.$dirty) {
                return (form.callDatetime.$invalid || form.callStatus.$invalid);
              }
              return false;
            }
          },

          patient: {
            isOpen: false,
            isLoaded: false,
            load: function ($scope, dictionaries, aliaces) {
              if (!this.isLoaded) {
                $scope.diagnosisKind = dictionaries.dictionary.query({alias: aliaces.diagnosisKind.alias});
                $scope.accidentReason = dictionaries.dictionary.query({alias: 'accidentreasons'});
                this.isLoaded = true;
              }
            },
            hasErrors: function () {
            }

          },
          medcare: {
            isOpen: false,
            isLoaded: false,
            load: function ($scope, dictionaries, aliaces) {
              if (!this.isLoaded) {
                $scope.difficulty = dictionaries.dictionary.query({alias: aliaces.difficulty.alias});
                $scope.efficacyStepComplication = dictionaries.dictionary.query({alias: aliaces.efficacyStepComplication.alias});
                this.isLoaded = true;
              }
            },
            hasErrors: function (form) {
            }
          },
          result: {
            isOpen: false,
            isLoaded: false,
            load: function ($scope, dictionaries) {
              if (!this.isLoaded) {
                $scope.departureResult = dictionaries.callResults.query();
                this.isLoaded = true;
              }
            },
            hasErrors: function (form) {
            }
          },
          notify: {
            isOpen: false,
            isLoaded: true,
            load: function () {
            },
            hasErrors: function (form) {
            }
          }
        };

        var dictionaries = {
          dictionary: dictionary,
          callResults: callResults

        };

        $scope.AccStatuses.call.load($scope, dictionaries, aliaces);
        $scope.AccStatuses.patient.load($scope, dictionaries, aliaces);
        $scope.AccStatuses.medcare.load($scope, dictionaries, aliaces);
        $scope.AccStatuses.result.load($scope, dictionaries, aliaces);
        $scope.AccStatuses.notify.load($scope, dictionaries, aliaces);


        $scope.operators = staff.query({value: fakeUser});


        function showValidation() {
          $scope.cardForm.$setDirty();
          $scope.showValidation = true;
        }

        function save() {
          if ($scope.cardForm.$valid) {

            var data = convertToData($scope.card, dtoDateConverter);
            $log.debug(ng.toJson(data));

            if ($scope.card.id) {
              dataService.update(data, ng.noop,
                function () {
                  showValidation();
                }
              );
            }
            else {
              var id = dataService.save(data, function (ret) {
                $scope.card.id = toId(id);
                renameTab();
              }, function () {
                showValidation();
              });
            }

          } else showValidation();

        }

        $scope.Save = save;

        $scope.Sign = function () {
          save();
          $log.debug('sign');
        };

        function setId(view) {

          view.id = tabService.getParams().id;
        }

        $scope.Close = function () {
          var view = {
            'name': 'OsmpCard'

          };
          setId(view);
          tabService.closeTab(new TabType(view));
        };

        var currentID = tabService.getParams().id;

        if (currentID) {
          loadCard(currentID);
        }
        else {
          createCard();
        }


      }]);

}(window.angular, window.moment));
