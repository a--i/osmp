/**
 * Created by ppavlov on 16.12.2014.
 */
'use strict';
(function (ng, undefined) {
  ng.module('rbt.osmp.module').
    controller('rbt.osmp.patientSearch',
    [
      '$scope',
      'rbt.osmp.patient',
      '$modalInstance',
      'rbtPagerService',
      function ($scope, patientService, $modalInstance, pagerService) {

        $scope.filter = {};
        $scope.rows = [];

        $scope.page = {
          pageNumber: 0,
          recordsOnPage: 10
        };

        $scope.pagerPrefix = 'osmpPatientSelect';

        function load() {
          if (!dontLoad && $scope.filterForm.$dirty) {
            $scope.rows = patientService.query(ng.extend($scope.filter, $scope.page));
          }
        }

        $scope.pageChange = function (page) {
          if (page !== undefined) {
            $scope.page = page;
            load();
          }
        };

        var dontLoad = false;

        $scope.search = function () {
          dontLoad = true;
          $scope.page.pageNumber = 0;
          $scope.isDisabled = true;
          dontLoad = false;
          load();
        };


        var selected;
        $scope.select = function (row) {
          selected = row;
          $scope.isDisabled = false;

        };

        $scope.isDisabled = true;

        $scope.ok = function () {
          $modalInstance.close(selected);
        };

        $scope.cancel = function () {
          $modalInstance.dismiss('cancel');
        };

        //destructor
        $scope.$on("$destroy", function () {
          // При уничтожении контроллера нужно удалить соответствующий сервис пейджирования
          pagerService.deletePager($scope.pagerPrefix);
        });


      }]);
})(window.angular);
