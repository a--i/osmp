/**
 * Created by ppavlov on 17.12.2014.
 */
'use strict';
(function (ng, undefined) {

  ng.module('rbt.osmp.module').
    controller('rbt.osmp.patientCtrl',['$scope','rbt.common.dictionarySrv','rbt.osmp.patient','$modalInstance', 'patientID',
      function($scope, dictionary, patientService, $modalInstance,patientID) {

        if (patientID)
        {
          $scope.patient = patientService.get({id:patientID});
        }
        else {
          $scope.patient = {};
          $scope.patient.personDto = {};
          $scope.patient.personDto.passportDto = {};
        }

      $scope.genders = dictionary.query({alias:'Sex'} );

      $scope.doctypes = dictionary.query({alias:'DocumentType'} );

      $scope.ok =function(){
        $modalInstance.close($scope.patient.id);
      };

      $scope.cancel=function(){
        $modalInstance.dismiss('close');
      }

    }]);

})(window.angular);
