/**
 * Created by ppavlov on 17.12.2014.
 */
'use strict';
(function (ng, undefined) {
  ng.module('rbt.osmp.module').
    factory ('rbt.osmp.complicationService',['$filter', 'rbt.common.dictionarySrv','rbt.dictAliases', function($filter, dictionary,aliaces){

      function Complication(scope)
      {
        var that = this;
        this.scope=scope;

        this.selected = "isOn";
        this.name = "name";

        this.input = dictionary.query({alias:aliaces.difficulty.alias});


        scope.$watch(function(){
          return scope.card;
        },function()
          {
            that.output =[];

            ng.forEach(that.scope.card.complicationList, function(id){
              var sel = $filter('filter')(that.input, {id:id},true);
              if (sel.length>0)
              {
                var el = sel[0];
                el[that.selected] = true;
                that.output.push(el);
              }
            })
          }
        );

        scope.$watch( function(){return that.output;}, function(){
             that.scope.card.complicationList =
               that.output.map(function(el){return el.id});
        },true);
      }

     return Complication;

  }]);
})(window.angular);
