/**
 * Created by ppavlov on 17.12.2014.
 */
'use strict';
(function (ng, undefined) {


  ng.module('rbt.osmp.module').
    factory ('rbt.osmp.patientService',['$modal', 'rbt.osmp.patient','rbt.Controls.ValueConverter', '$filter', function($modal, service, converter, $filter) {

    var dtoDateConverter = new (converter('DtoDateConverter'))();


    function Patient(scope)
    {
      var that = this;
      var _scope = scope;

      function getText(data)
      {
        return data.personDto.lastName + ' ' + data.personDto.firstName + ' ' + data.personDto.middleName  + ', ' +
          $filter('date')(dtoDateConverter.toValue(data.personDto.birthDate),'dd.MM.yyyy');
      }

      scope.$watch('card', function(){
        if ( _scope.card.patientAmbulanceCall){

          service.get({id:_scope.card.patientAmbulanceCall}, function(data){
            that.Text = getText(data);
          });

        }

      });


      this.open = function(create){
        var dialogDefinition =  {
          templateUrl: 'app/Modules/osmp/card/patient.tpl.html',
          controller: 'rbt.osmp.patientCtrl',
          size:'lg',
          backdrop:'static'
        };

        if (!create) {
          dialogDefinition.resolve = {
            patientID: function () {
              return _scope.card.patientAmbulanceCall;
            }
          }
        }
        else {
          dialogDefinition.resolve = {
            patientID: undefined
          }
        }

        var modalInstance = $modal.open(dialogDefinition);

        modalInstance.result.then(function(selected){
          that.Text = getText(selected);
          _scope.card.patientAmbulanceCall = selected.id;
        });
      };

      this.select = function()
      {
        var modalInstance = $modal.open({
          templateUrl: 'app/Modules/osmp/card/patientSelect.tpl.html',
          controller: 'rbt.osmp.patientSearch',
          size:'lg',
          backdrop:'static'
        });

        modalInstance.result.then(function(selected){
          that.Text = getText(selected);
          _scope.card.patientAmbulanceCall = selected.id;
        });

      };
      this.Text ='';

    }

    return Patient;

  }]);


})(window.angular);
