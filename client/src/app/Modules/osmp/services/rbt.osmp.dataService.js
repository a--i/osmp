'use strict';

(function (ng, undefined) {

  function DatePrepareSrv(ServiceGet, $q, List) {
    var that = this;
    this.serviceGet = ServiceGet;
    this.list = List;
    this.q = $q;
    this.get = ng.bind(that, this.get);
  }

  DatePrepareSrv.prototype = {
    get: function (qs) {
      var list = this.list;

      var deferred = this.q.defer();
      this.serviceGet.query(qs, function (response) {
        if (ng.isDefined(response)) {
          if (response.length > 0) {
            list.colNames = Object.keys(response[0]);
          }
          list.rows = response;
        } else {
          list.colNames = [];
          list.rows = [];
        }

        deferred.resolve(list);
      });
      return deferred.promise;
    }
  };

  ng.module('rbt.osmp.module')

    .factory('rbt.osmp.data', ['rbt.baseServicePath', 'rbt.Controls.serverError', '$resource', function (basePath, errorService, $resource) {
      var uri = basePath + 'ambulance/calls/:id';
      return $resource(uri,
        {id: '@id'},
        {
          save: {method: 'POST', interceptor: errorService.getInterceptor('cardPost')},
          update: {method: 'PUT', interceptor: errorService.getInterceptor('cardPost')}
        });
    }])
    .factory('rbt.osmp.patient', ['rbt.baseServicePath', '$resource', function (basePath, $resource) {

      var uri = basePath + 'ambulance/patients/:id';
      return $resource(uri,
        {
          id: '@id'
        });

    }])

    .factory('rbt.osmp.dataPrepare',
    [
      'rbt.osmp.data',
      'rbt.DataManager.LocalStorage',
      'columnsLocalStorageName',
      '$q',
      '$log',
      function (serviceGet, localStorage, lsName, $q) {
        var list = {
          columnsDef: [],
          colNames: [],
          rows: []
        };

        var dateFormat = 'dd.MM.yyyy HH:mm';

        var columnsDef = [
          {
            alias: 'code',
            name: 'Код',
            isVisible: true,
            order: 10,
            formatter: null,
            format: null,
            type: null,
            readOnly: true
          },
          {
            alias: 'callDatetime',
            name: 'Принят вызов',
            isVisible: true,
            order: 20,
            formatter: 'date',
            format: dateFormat,
            type: null
            , readOnly: true
          },
          {
            alias: 'callPassDatetime',
            name: 'Передан',
            isVisible: true,
            order: 30,
            formatter: 'date',
            format: dateFormat,
            type: null
            , readOnly: true
          },
          {
            alias: 'departureDatetime',
            name: 'Выезд',
            isVisible: true,
            order: 40,
            formatter: 'date',
            format: dateFormat,
            type: null
            , readOnly: true
          },
          {
            alias: 'patientArrivalDatetime',
            name: 'На месте',
            isVisible: true,
            order: 50,
            formatter: 'date',
            format: dateFormat,
            type: null
            , readOnly: true
          },
          {
            alias: 'departureReturnDatetime',
            name: 'Возвращение',
            isVisible: true,
            order: 60,
            formatter: 'date',
            format: dateFormat,
            type: null
            , readOnly: true
          },
          {
            alias: 'departureResultDatetime',
            name: 'Затрачено',
            isVisible: true,
            order: 70,
            formatter: 'date',
            format: dateFormat,
            type: null
            , readOnly: true
          },
          {
            alias: 'unidentified',
            name: 'Идентифицирован',
            isVisible: true,
            order: 80,
            formatter: null,
            format: null,
            type: 'checkbox'
            , readOnly: true
          },
          {
            alias: 'patient',
            name: 'Пациент',
            isVisible: true,
            order: 90,
            formatter: null,
            format: null,
            type: null,
            readOnly: true
          },
          {
            alias: 'callReason',
            name: 'Повод к вызову',
            isVisible: true,
            order: 100,
            formatter: null,
            format: null,
            type: null
            , readOnly: true
          },
          {
            alias: 'contactPhone',
            name: 'Контактный телефон',
            isVisible: true,
            order: 110,
            formatter: null,
            format: null,
            type: null
            , readOnly: true
          },
          {
            alias: 'dispatcher',
            name: 'Диспетчер',
            isVisible: true,
            order: 120,
            formatter: null,
            format: null,
            type: null
            , readOnly: true
          },
          {
            alias: 'callType',
            name: 'Тип вызова',
            isVisible: true,
            order: 130,
            formatter: null,
            format: null,
            type: null
            , readOnly: true
          },
          {
            alias: 'callStatus',
            name: 'Статус вызова',
            isVisible: true,
            order: 140,
            formatter: null,
            format: null,
            type: null
            , readOnly: true
          },
          {
            alias: 'departureTeamNumber',
            name: 'Бригада',
            isVisible: true,
            order: 150,
            formatter: null,
            format: null,
            type: null
            , readOnly: true
          },
          {
            alias: 'callPassType',
            name: 'Способ передачи',
            isVisible: true,
            order: 160,
            formatter: null,
            format: null,
            type: null
            , readOnly: true
          },
          {
            alias: 'result',
            name: 'Результат выезда',
            isVisible: true,
            order: 170,
            formatter: null,
            format: null,
            type: null
            , readOnly: true
          },
          {
            alias: 'resultId',
            name: 'resultId',
            isVisible: false,
            order: 180,
            formatter: null,
            format: null,
            type: null
            , readOnly: true
          },
          {
            alias: 'patientWalking',
            name: 'Амбулаторный больной',
            isVisible: true,
            order: 190,
            formatter: null,
            format: null,
            type: 'checkbox'
            , readOnly: true
          },
          {
            alias: 'diagnosis',
            name: 'Диагноз МКБ',
            isVisible: true,
            order: 200,
            formatter: null,
            format: null,
            type: null
            , readOnly: true
          },
          {
            alias: 'diagnosisId',
            name: 'diagnosisId',
            isVisible: true,
            order: 210,
            formatter: null,
            format: null,
            type: null
            , readOnly: true
          },
          {
            alias: 'mkbCode',
            name: 'Код МКБ',
            isVisible: true,
            order: 220,
            formatter: null,
            format: null,
            type: null,
            readOnly: true
          },
          {
            alias: 'lastName',
            name: 'Фамилия',
            isVisible: false,
            order: 230,
            formatter: null,
            format: null,
            type: null,
            readOnly: true
          },
          {
            alias: 'firstName',
            name: 'Имя',
            isVisible: false,
            order: 240,
            formatter: null,
            format: null,
            type: null,
            readOnly: true
          },
          {
            alias: 'middleName',
            name: 'Отчество',
            isVisible: false,
            order: 250,
            formatter: null,
            format: null,
            type: null
            , readOnly: true
          },
          {
            alias: 'hospitalization',
            name: 'Госпитализирован',
            isVisible: true,
            order: 260,
            formatter: null,
            format: null,
            type: 'checkbox'
            , readOnly: true
          },
          {
            alias: 'id',
            name: 'id',
            isVisible: false,
            order: 270,
            formatter: null,
            format: null,
            type: null,
            readOnly: true
          },
          {
            alias: 'rwUserId',
            name: 'rwUserId',
            isVisible: false,
            order: 280,
            formatter: null,
            format: null,
            type: null
            , readOnly: true
          },
          {
            alias: 'rwDiagnosisId',
            name: 'rwdiagnosisId',
            isVisible: false,
            order: 290,
            formatter: null,
            format: null,
            type: null
            , readOnly: true
          },
          {
            alias: 'rwResultId',
            name: 'rwresultId',
            isVisible: false,
            order: 300,
            formatter: null,
            format: null,
            type: null
            , readOnly: true
          },
          {
            alias: 'sgnId',
            name: 'sgnId',
            isVisible: false,
            order: 310,
            formatter: null,
            format: null,
            type: null,
            readOnly: true
          },
          {
            alias: 'state',
            name: 'state',
            isVisible: false,
            order: 320,
            formatter: null,
            format: null,
            type: null,
            readOnly: true
          },
          {
            alias: 'userId',
            name: 'userId',
            isVisible: false,
            order: 330,
            formatter: null,
            format: null,
            type: null,
            readOnly: true
          }
        ];

        list.columnsDef = ng.fromJson(localStorage.get(lsName));
        if (!ng.isDefined(list.columnsDef) || list.columnsDef === null) {
          list.columnsDef = columnsDef;
        }

        return new DatePrepareSrv(serviceGet, $q, list);
      }]);


})(window.angular);
