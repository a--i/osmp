/**
 * Created by ppavlov on 11.12.2014.
 */
'use strict';
(function (ng, undefined) {
   ng.module('rbt.osmp.module').
     factory ('rbt.osmp.mkbService',['$filter', 'rbt.common.MkbTen', function($filter, mkbTen){

     function MKB(scope)
     {
       this.scope = scope;
       var that = this;
       this.Change = ng.bind(that, this.Change);
       this.openMKB = ng.bind(that,this.openMKB);
       this.getMKBTen = ng.bind(that,this.getMKBTen);
       this.setCode = ng.bind(that,this.setCode);
     }

     MKB.prototype =  {
       code:'',
       Loading: false,
       getMKBTen: function (value) {

         var data = mkbTen.query({'code':value});

        return  data.$promise.then(function(data){

           var ret =$filter('filter')(data, {code: value});

           return ret;
         });


       },

       setCode:function(id){
          if(id) {
            var that = this;
            mkbTen.get({id: id}, function (data) {
              that.code = data.code;
              that.SelectedText = data.diagnoseName;
            });
          }
       },

       Change:function(item, model, mkb){
         this.scope.card.diagnosisMKB = item.id;
         this.SelectedText = mkb;

       },
       SelectedText: '',
       openMKB: function () {
       }
     };

     return MKB;

   }]);

})(window.angular);
