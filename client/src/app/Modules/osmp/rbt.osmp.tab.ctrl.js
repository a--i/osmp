'use strict';

(function (ng) {

  ng.module('rbt.osmp.module')
    .controller('OsmpTabCtrl', [
      '$scope',
      'rbt.common.osmpTabService',
      function ($scope, tabService) {

        $scope.tabs = tabService.tabs;

        $scope.closeTab = function (tab) {
          tabService.closeTab(tab);
        };

        $scope.$watch(function () {
            return tabService.tabs;

          }, function () {
            $scope.tabs = tabService.tabs;
          }, true
        );


      }]);

}(window.angular));
