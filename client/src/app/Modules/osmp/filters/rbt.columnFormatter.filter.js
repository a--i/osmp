/**
 * Created by Andrey Petkevich on 19.12.2014.
 */
'use strict';

(function (ng, undefined) {

  ng.module('rbt.osmp.module')
    .filter('picker', ['$filter', function ($filter) {
      return function (value, filterName, format) {
        if (!ng.isDefined(filterName) || filterName === null) {
          return value;
        }
        if (ng.isDefined(format) && format !== null) {
          var ret = $filter(filterName)(new Date(value), format);
          return ret;
        }

        return $filter(filterName)(value);
      };
    }]);

})(window.angular);
