'use strict';

(function (ng) {

  var fd = function (d, $filter) {
    return $filter('date')(d, 'MM/dd/yyyy HH:mm:ss');
  };

  function Journal(osmpListData, $log, $filter) {
    this.log = $log;
    this.filter = $filter;
    this.osmpListData = osmpListData;
    var that = this;
    this.getList = ng.bind(that, this.getList);
    this.filterInit = ng.bind(that, this.filterInit);
    this.callBack = ng.bind(that, this.callBack);
    this.changeSortOrder = ng.bind(that, this.changeSortOrder);
  }

  Journal.prototype = {
    List: {},
    Filter: {},
    SortOrder: {},
    CurrentRow: {},
    getList: function (page, toBeg, e) {

      if (e) {
        e.preventDefault();
        e.stopPropagation();
      }

      if (!ng.isDefined(toBeg)) {
        toBeg = false;
      }
      if (toBeg) {
        page.pageNumber = 0;
      }

      var filter = this.Filter.filter;
      var cb = this.callBack;
      //var logger = this.log;

      this.osmpListData.get(
        {
          'pageNumber': page.pageNumber,
          'recordsOnPage': page.recordsOnPage,
          'code': filter.code === '' ? null : filter.code,
          'lastName': filter.lastName === '' ? null : filter.lastName,
          'firstName': filter.firstName === '' ? null : filter.firstName,
          'middleName': filter.middleName === '' ? null : filter.middleName,
          'ambulanceCallResult': filter.ambulanceCallResult,
          'dateFrom': /*filter.dateFrom === '' ? null : filter.dateFrom,*/ fd(filter.dateFrom, this.filter),
          'dateTo': /*filter.dateTo === '' ? null : filter.dateTo,*/fd(filter.dateTo, this.filter),
          'signatureStatus': filter.signatureStatus === '-1' ? null : filter.signatureStatus,
          'sort.fieldName': this.SortOrder.orderByField === '' ? null : this.SortOrder.orderByField,
          'sort.order': this.SortOrder.orderByField === '' || !ng.isDefined(this.SortOrder.orderByField) || this.SortOrder.orderByField === null ? null : (this.SortOrder.reverseSort ? 'asc' : 'desc')

        }).then(function (d) {
          //logger.log('then - d=' + ng.toJson(d));
          cb(d);
        });
    },
    filterInit: function () {
      // очищаем фильтр (заполняем значениями по умолчанию)
      this.Filter = {
        'filter': {}
      };

      this.SortOrder = {};
    },
    callBack: function (d) {
      //this.log.log('d=' + ng.toJson(d));
      if (d.length === 0) {
        this.List = [];
        this.CurrentRow = {};
      } else {
        this.List = d;
        this.CurrentRow = this.List.rows[0];
      }
    },
    changeSortOrder: function (page, col) {
      this.SortOrder.orderByField = col.alias;
      this.SortOrder.reverseSort = !this.SortOrder.reverseSort;
      this.getList(page);
    }
  };

  ng.module('rbt.osmp.module')
    .controller('OsmpListCtrl',
    [
      '$scope',
      'rbt.common.osmpTabService',
      'rbt.common.tabType',
      'rbt.osmp.dataPrepare',
      'rbt.common.dictionaryCallsResultsSrv',
      'rbt.DataManager.LocalStorage',
      'columnsLocalStorageName',
      'rbtPagerService',
      '$filter',
      '$log',
      function ($scope,
                tabService,
                TabType,
                osmpListData,
                dictionaryCallResult,
                localStorage,
                lsName,
                pagerService,
                $filter,
                $log) {

        $scope.dicts = {
          ambulanceCallResults: dictionaryCallResult.query(),
          signatureStatuses: [
            {id: '-1', value: 'Все'},
            {id: '0', value: 'Не подписанные'},
            {id: '1', value: 'Подписанные'}
          ]
        };

        $scope.page = {
          recordsOnPage: 5,
          pageNumber: 0
        };

        $scope.pagerPrefix = 'osmpJournal';

        $scope.serchForm = {
          isOpen: true
        };

        $scope.pager = new Journal(osmpListData, $log, $filter);

        $scope.openCard = function (row) {
          if (!ng.isDefined(row)) {
            row = {code: 'Новая', id: 0};
          }
          var view = {
            id: row.id,
            title: 'Карточка вызова ' + row.code,
            name: 'OsmpCard',
            tpl: '/app/Modules/osmp/card/rbt.osmp.card.tpl.html',
            params: {id: row.id}
          };
          tabService.openTab(new TabType(view));
        };

        $scope.selectRow = function (row) {
          $scope.pager.CurrentRow = row;
        };

        $scope.clearSearchForm = function (e) {
          if (e) {
            e.preventDefault();
            e.stopPropagation();
          }

          $scope.pager.filterInit();
          $log.debug('Очистка формы');
        };

        // RUN
        $scope.pager.filterInit();

        $scope.changePage = function (page){
          $scope.page = page;
          $scope.pager.getList(page);
        };

        //destructor
        $scope.$on("$destroy", function () {
          // При уничтожении контроллера нужно удалить соответствующий сервис пейджирования
          pagerService.deletePager($scope.pagerPrefix);
        });

      }]);


}(window.angular));
