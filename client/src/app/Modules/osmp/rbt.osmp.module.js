'use strict';

(function (ng, undefined) {


  ng.module('rbt.osmp.module', ['rbt.navigation', 'rbt.DataManager', 'ngResource', 'rbt.DataManager'])
    .constant('columnsLocalStorageName','rbt.osmp.list.ambulancecall');

})(window.angular);
