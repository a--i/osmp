/**
 * Created by Andrey Petkevich on 19.12.2014.
 */
'use strict';

(function (ng, undefined) {

  ng.module('rbt.osmp.module')
    .directive('rbtColumnFilter',
    [
      'columnsLocalStorageName',
      'rbt.DataManager.LocalStorage',
      function (lsName, localStorage) {

      return {
        restrict: 'E',
        templateUrl: 'app/Modules/osmp/directives/rbt.columnFilter.directive.tpl.html',
        scope: {
          columns: '='
        },
        link: function (scope) {

          scope.saveColumnsSelection = function () {
            localStorage.put(lsName, ng.toJson(scope.columns));
          };

        }
      };
    }]);

})(window.angular);
