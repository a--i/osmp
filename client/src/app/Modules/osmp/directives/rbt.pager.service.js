/**
 * Created by Andrey Petkevich on 23.12.2014.
 */
'use strict';

(function (ng, undefined) {

  function Pager() {
    var that = this;
    this.onChange = {};
    this.noNext = ng.bind(that, this.noNext);
    this.noPrevious = ng.bind(that, this.noPrevious);
    this.selectPage = ng.bind(that, this.selectPage);
    this.changeItemsPerPage = ng.bind(that, this.changeItemsPerPage);
  }

  Pager.prototype = {
    ItemsPerPageArr: [5, 10, 30, 50],
    Page: {
      recordsOnPage: 5,
      pageNumber: 0
    },
    LastPage: false,
    noNext: function () {
      return this.LastPage === true ? 'disabled' : '';
    },
    noPrevious: function () {
      return this.Page.pageNumber === 0 ? 'disabled' : '';
    },
    selectPage: function (n) {
      if (n >= 0 && !(this.LastPage && this.Page.pageNumber < n)) {
        this.Page.pageNumber = n;
        this.onChange({page: this.Page});
      }
    },
    changeItemsPerPage: function (items) {
      this.Page.recordsOnPage = items;
      this.Page.pageNumber = 0;
      this.onChange({page: this.Page});
    }
  };

  function PagerSrv($filter) {
    var that = this;
    this.$filter = $filter;
    this.pagers = [];
    this.getPager = ng.bind(that, this.getPager);
    this.deletePager = ng.bind(that, this.deletePager);
  }

  PagerSrv.prototype = {
    getPager: function (prefix, onChange) {
      var retPager = {};
      console.log('PagerSrv getPager prefix=' + prefix);
      var pager = this.$filter('filter')(this.pagers, {prefix: prefix})[0];
      if (!ng.isDefined(pager) || pager === null) {
        var newPager = new Pager();
        newPager.onChange = onChange;
        this.pagers.push({prefix: prefix, pager: newPager});
        retPager = newPager;
      } else {
        // Первый запуск - обновляем данные.
        pager.pager.selectPage(0);
        retPager = pager.pager;
      }
      return retPager;
    },

    // этот метод нужно вызывать в деструкторе контроллера, в шаблоне которого
    // используется директива пейджирования
    deletePager: function (prefix) {
      this.pagers = this.pagers.filter(function (element) {
        return element.prefix !== prefix;
      });
    }
  };

  ng.module('rbt.osmp.module')
    .factory('rbtPagerService',
    ['$filter',
      function ($filter) {
        return new PagerSrv($filter);
      }]);

})(window.angular);
