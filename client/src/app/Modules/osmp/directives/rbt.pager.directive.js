'use strict';

(function (ng) {

  ng.module('rbt.osmp.module')
    .directive('rbtPager', ['rbtPagerService', function(PagerSrv){
      return {
        restrict: 'E',
        templateUrl: 'app/Modules/osmp/directives/rbt.pager.directive.tpl.html',
        scope: {
          rowsCount: '=',
          onChange: '&'
        },
        link: function (scope, element, attrs) {

          var prefix = attrs.pagerPrefix || 'root';
          scope.pager = PagerSrv.getPager(prefix, scope.onChange);
          //scope.pager.page = scope.page;

          scope.$watch('rowsCount', function (newValue) {
            scope.pager.LastPage = newValue < scope.pager.Page.recordsOnPage;
          });

        }
      };
    }]);

}(window.angular));
