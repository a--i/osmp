/**
 * Created by Andrey Petkevich on 23.12.2014.
 */
'use strict';

(function (ng, undefined) {

  ng.module('rbt.osmp.module')
    .directive('rbtOsmpInput', ['$compile', function ($compile) {
      return {
        restrict: 'A',
        link: function ( scope, element, attrs ) {
          //тип инпута
          var inputType = attrs.inputType;
          var readOnly = attrs.inputReadOnly || false;

          //значение поля
          var fieldValue = Number(attrs.fieldValue);
          //var fieldValue = !!attrs.fieldValue;
          var template = '';

          switch (inputType) {
            case 'checkbox':
              var checked = fieldValue ? 'checked' : '';
              var readOnlyTpl = readOnly ? ' onclick="return false" ' : '';
              template = '<input type="checkbox" ' + checked + readOnlyTpl + '/>';
              break;
          }

          //check inputType
          if( inputType ) {

            //Rendering template.
            element.html('').append( $compile( template )( scope ) );
          }
        }
      };
    }]);

})(window.angular);
