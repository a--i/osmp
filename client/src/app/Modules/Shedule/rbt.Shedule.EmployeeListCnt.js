﻿/// <reference path="../../../Scripts/angular.js" />

(function (ng, undefined) {

    ng.module("rbt.Shedule").
    controller("rbt.Shedule.EmployeeListCnt", ["$scope", "rbt.Shedule.EmployeeSrv",
        "$rootScope", 
        function ($scope, employeeSrv, $rootScope) {

         

        $scope.Employees = employeeSrv.query();

        $scope.Select = function(Employee)
        {
            $rootScope.$broadcast("Shedule.selectEmployee", Employee)
            
        }

    }])

})(window.angular, undefined)