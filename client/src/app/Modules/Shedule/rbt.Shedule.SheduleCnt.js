﻿/// <reference path="../../../Scripts/angular.js" />
(function (ng, undefined) {


    ng.module("rbt.Shedule")
    .controller("rbt.Shedule.SheduleCnt", ["$scope", "rbt.Shedule.SheduleSrv", function ($scope, sheduleSrv) {

        $scope.options = {
            from: 8.30,
            to: 20.00,
            step: 0.05
        };

        $scope.Employee = {};

        $scope.Shedule = [];

        $scope.Set = function (Employee) {
            $scope.Employee = Employee;

            $scope.Shedule = sheduleSrv.query(Employee.EmplID);

        }

        $scope.Coefficient = 0.5;

        $scope.getText = function (val) {
            var calck = (val[0] + val[1]) * $scope.Coefficient;

            var ret = $scope.Employee.EmplName + "<br>";
            ret += "c " + val[0] + " по " + val[1] + "</br>";
            ret += "что то посчитали " + calck;
            return ret;
        }

        $scope.$on("Shedule.selectEmployee", function (evt, employee) { $scope.Set(employee) });


    }]);


})
(window.angular)