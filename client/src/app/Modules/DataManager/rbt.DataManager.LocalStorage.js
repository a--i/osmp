﻿'use strict';

(function (ng,  undefined) {

    function LocalStorage(ls)
    {
        var that = this;
        this.ls = ls;
        this.get = ng.bind(that, that._get);
        this.put = ng.bind(that, that._put);
    }

    LocalStorage.prototype = {
        _get: function (key) {
            return this.ls.getItem(key);
        },

        _put: function (key, value) {
            this.ls.setItem(key, value);

        }
    };

    ng.module('rbt.DataManager')
        .factory('rbt.DataManager.LocalStorage', ['$window', function ($window) {
            return new LocalStorage($window.localStorage);
        }]);

})(window.angular);
