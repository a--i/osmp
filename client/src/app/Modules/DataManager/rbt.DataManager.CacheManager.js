﻿/// <reference path="../../../Scripts/angular.js" />
(function (ng, undefined)
{
    ng.module("rbt.DataManager")
    .factory("rbt.DataManager.CacheManager", ["$cacheFactory", "rbt.DataManager.PersistentCache", function (cacheFactory, persistentCache) {


        function CachedObject(data, context)
        {
            if (arguments.length > 1) {
                this._data = data;
                this._expiredate;
                if (context && context.action && context.action.duration) {
                    var today = new Date().valueOf();
                    today += context.action.duration * 60 * 1000;
                    this._expiredate = new Date(today);
                }
            }
            else
            {
                this._data = data._data;
                this._expiredate = data._expiredate;
            }

        }

        CachedObject.prototype={
            getData:function(){
                return this._data;
            },

            isExpired:function(){
                return  !this._expiredate || this._expiredate< new Date();
            }


        };



        var L0, L1;

        L0 = cacheFactory("DMCache");
      
        L1 = persistentCache;

        function getKey(context)
        {
            return context.resource.Key + context.name;
        }

        function isCacheble(context)
        {
            return context && context.action && context.action.resultCache &&
                   context.action.resultCache != 'none' && context.action.method == "GET";
        }

        function getCache(context)
        {
            
            
            var ret = context.action.resultCache == "L0" ? L0 : L1;
          
            ret.level = context.action.resultCache;
            
            return ret;
        }

        
              
        return {
            get :function (context) {
                if (isCacheble(context)) 
                {
                    var cache = getCache(context);
                    var key = getKey(context);
                   

                    var obj = cache.get(key);
                  
                    if (obj )
                    {
                        console.debug(obj);
                        if (!(obj instanceof CachedObject)) {

                            obj = new CachedObject(obj);
                        }

                        if (!obj.isExpired() || cache.level == "L1")
                            return obj.getData();
                    }
                }
                return null;
            },
            put: function (context, data) {
                if (isCacheble(context)) {
                    {
                     
                        var cache = getCache(context)
                        var key = getKey(context);
                        console.debug(key);
                        cache.put(key,  new CachedObject(data,context))
                    }
                }
            }
    };

        

    }])

})(window.angular)