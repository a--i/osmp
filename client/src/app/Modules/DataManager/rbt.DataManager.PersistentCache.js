﻿/// <reference path="../../../Scripts/angular.js" />
(function (ng, ls, undefined) {
    
    var defaults = {
        saveInterval: 3,
        persistanceKey: "CachePersistanecKey",
    }

    function PersistentCache(cache)
    {
        var that = this;
        this.cache = cache || {};

        this.get = ng.bind(that, that._get);
        this.put = ng.bind(that, that._put);
        this.toJson = ng.bind(that, that._toJson);
        this.destroyed = false;
    }

    PersistentCache.prototype = {
        _get: function (key) {
            if (this.destroyed) throw "destroyed";
            return this.cache[key]
        },
        _put:function(key, data)
        {
            if (this.destroyed) throw "destroyed";
            this.cache[key] = data;
        },
        _toJson: function () {
            var js = ng.toJson(this.cache);
            return js;
        }
    }

    function CacheProcessor($localStorage, $window, cache,  options) {
        
        var interval;
        var _cache = cache;
        
        this.$destroy = function () {
            save();
            if (interval)
                $window.clearInterval(interval);
        };

        function save() {
            $localStorage.put(options.persistanceKey, _cache.toJson());
        }

        this.start = function () {
            if (!interval)
                interval = $window.setInterval(function () {
                    save();
                }, options.saveInterval * 1000);
        };
    }

    function PersistenceCacheProvider($localStorage, $window)
    {
        var that = this;
        that.options = ng.copy(defaults);

        var saved = $localStorage.get(this.options.persistanceKey);
        var savedObj
        if (saved)
        {
            savedObj = ng.fromJson(saved);
        }

      
        that.cache = new PersistentCache(savedObj);

        this.processor =  new CacheProcessor($localStorage, $window, this.cache, this.options);

        
        
        this.$setOptions = ng.bind(that, that._setOptions);
        this.$get = ["$rootScope",that._get];
        
    }

    PersistenceCacheProvider.prototype = {

        _setOptions : function (options)
        {
            this.options = ng.extend({}, options, defaults)
        },

        _get: function ($rootScope) {
            var that = this;
            $rootScope.$on("$destroy", function () {
                that.cache.destroyed = true;
                that.processor.$destroy()
            });
            this.processor.start();
            return this.cache;
        }

    }

    var provider;

    ng.module("rbt.DataManager")
    .provider("rbt.DataManager.PersistentCache", [ "rbt.DataManager.LocalStorageProvider","$windowProvider",  function ( $localStorage, $window) {
      
            return  new PersistenceCacheProvider($localStorage.$get(), $window.$get());
       

    }]);

})
(window.angular, window.localStorage)
