﻿/// <reference path="../../../Scripts/angular.js" />
/// <reference path="../../../Scripts/angular-resource.js" />

(function (ng, undefined) {

    var $resourceMinErr = ng.$$minErr('$resource');

	ng.module("rbt.DataManager")
	.provider("rbt.DataManager.$resource", function () {

	    var provider = this;
	    var resource;
	    var _$q;

	    var cacheFactory = {};

        /// настройки по умолчанию кэширования и сохранения
	    this.defaults = {
	        actionSettings : 
                {
                    /* режим кэширования
                       none - результат операции не кэшируется
                       L0 - сессионный кэш
                       L1 - постоянный кэш
                    */
                    resultCache: 'none',

                    /* 
                    Время кэширования объекта Мин.
                    */
                    duration: 2 ,

                    /*
                      признак оффлайнового сохранения
                    */
                    offlineSave: false

                    
                },

	    }

        /// применяет умолчания для настроек  кэширования и сохранения
	    function prepareActions(actions)
	    {
	        var prepared = {};
	        ng.forEach(actions, function (action, name) {
	            prepared[name] = ng.extend({}, provider.defaults.actionSettings, action);
	        });
	        return prepared;
	    }

        /// создание метода обработчика
	    function createMethod(context, params, $q)
	    {
	        var innerMethod = context.resource[context.name];


	        return function(a1,a2,a3,a4)
	        {
	            var params = {}, data, success, error;
	            var hasBody = /^(POST|PUT|PATCH)$/i.test(context.action.method);
	            /// перенесено 
	            switch (arguments.length) {
	                case 4:
	                    error = a4;
	                    success = a3;
	                    //fallthrough
	                case 3:
	                case 2:
	                    if (ng.isFunction(a2)) {
	                        if (ng.isFunction(a1)) {
	                            success = a1;
	                            error = a2;
	                            break;
	                        }

	                        success = a2;
	                        error = a3;
	                        //fallthrough
	                    } else {
	                        params = a1;
	                        data = a2;
	                        success = a3;
	                        break;
	                    }
	                case 1:
	                    if (ng.isFunction(a1)) success = a1;
	                    else if (hasBody) data = a1;
	                    else params = a1;
	                    break;
	                case 0: break;
	                default:
	                    throw $resourceMinErr('badargs',
                          "Expected up to 4 arguments [params, data, success, error], got {0} arguments",
                          arguments.length);
	            }


	            
	            var _data = cacheFactory.get(context);
	            if (!_data) {
	                _data = innerMethod(params, data, success, error);

	                _data.$promise.then(function () {
	                    cacheFactory.put(context, _data.toJSON());
	                    return _data;
	                });

	            }
	            else {
	                _data = new context.resource(_data);

	                var deffer = _$q.defer();
	                var promise = deffer.promise;
	                promise.then(function () {
	                    (success || ng.noop)(_data);
	                });
	                _data.$promise = promise;
	                deffer.resolve(_data);
	            }

	            return _data;
	        }
	    }


        // дополняет методы обработчиками кэша и сохранения
	    function resourceFactory(url, paramDefaults, actions, options)
	    {
	        Resource = resource(url, paramDefaults, actions, options);

	        ng.forEach(prepareActions(actions), function (action, name) {

	            Resource[name] = createMethod({name: name, action:action, resource:Resource});
	       })
	       
			return Resource;
	    }



	    this.$get = ["$resource", "rbt.DataManager.CacheManager", "$q", function (resourceProvider, $cahceFactory, $q) {
	        _$q = $q;
	        resource = resourceProvider;
            cacheFactory = $cahceFactory
		    return resourceFactory;
		}];

	})

})
(window.angular)