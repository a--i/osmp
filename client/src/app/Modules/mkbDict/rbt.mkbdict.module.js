/**
 * Created by Andrey Petkevich on 17.12.2014.
 */
'use strict';

(function (ng, undefined) {

  ng.module('rbt.mkbdict.module', ['angularTreeview']);

})(window.angular);
