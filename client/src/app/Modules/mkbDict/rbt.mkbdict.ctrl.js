/**
 * Created by Andrey Petkevich on 17.12.2014.
 */
'use strict';

(function (ng, undefined) {

  ng.module('rbt.mkbdict.module')
    .controller('DictionariesMkbCtrl',
    [
      '$scope',
      'rbt.dictAliases',
      'rbt.common.MkbTen',
      '$modal',
      '$log',
      function ($scope,
                aliases,
                MkbTen,
                $modal,
                $log) {

        $scope.items = [];

        $scope.roleList = [
          { 'roleName' : 'User', 'roleId' : 'role1', 'collapsed' : true, 'children' : [
            { 'roleName' : 'subUser1', 'roleId' : 'role11', 'collapsed' : true, 'children' : [] },
            { 'roleName' : 'subUser2', 'roleId' : 'role12', 'collapsed' : true, 'children' : [
              { 'roleName' : 'subUser2-1', 'roleId' : 'role121', 'children' : [
                { 'roleName' : 'subUser2-1-1', 'roleId' : 'role1211', 'children' : [] },
                { 'roleName' : 'subUser2-1-2', 'roleId' : 'role1212', 'children' : [] }
              ]}
            ]}
          ]},

          { 'roleName' : 'Admin', 'roleId' : 'role2', 'collapsed' : true, 'children' : [
            { 'roleName' : 'subAdmin1', 'roleId' : 'role11', 'collapsed' : true, 'children' : [] },
            { 'roleName' : 'subAdmin2', 'roleId' : 'role12', 'children' : [
              { 'roleName' : 'subAdmin2-1', 'roleId' : 'role121', 'children' : [
                { 'roleName' : 'subAdmin2-1-1', 'roleId' : 'role1211', 'children' : [] },
                { 'roleName' : 'subAdmin2-1-2', 'roleId' : 'role1212', 'children' : [] }
              ]}
            ]}
          ]},

          { 'roleName' : 'Guest', 'roleId' : 'role3', 'collapsed' : true, 'children' : [
            { 'roleName' : 'subGuest1', 'roleId' : 'role11', 'children' : [] },
            { 'roleName' : 'subGuest2', 'roleId' : 'role12', 'collapsed' : true, 'children' : [
              { 'roleName' : 'subGuest2-1', 'roleId' : 'role121', 'children' : [
                { 'roleName' : 'subGuest2-1-1', 'roleId' : 'role1211', 'children' : [] },
                { 'roleName' : 'subGuest2-1-2', 'roleId' : 'role1212', 'children' : [] }
              ]}
            ]}
          ]}
        ];

        $scope.refreshData = function () {

          $scope.items = MkbTen.query();
        };

        $scope.editItem = function (item) {

          var modalInstance = $modal.open({
            templateUrl: 'app/Modules/dict/modalEditItem.html',
            controller: 'ModalEditItemCtrl2',
            //size: size,
            resolve: {
              item: function () {
                return item;
              }
            }
          });

          modalInstance.result.then(function (/*item*/) {

            //dictionaries.update({alias: $scope.selected.selectedDictionary.alias, id:item.id }, item);
            //$scope.items = dictionaries.query({alias: $scope.selected.selectedDictionary.alias});

          }, function (selectedItem) {
            $log.info('answer: ' + selectedItem);
          });
        };

        $scope.refreshData();

      }])
    .controller('ModalEditItemCtrl2',
    [
      '$scope',
      '$modalInstance',
      'item',
      function ($scope, $modalInstance, item) {

        $scope.item = item;

        $scope.ok = function () {
          $modalInstance.close($scope.item);
        };

        $scope.cancel = function () {
          $modalInstance.dismiss('cancel');
        };
      }]);

})(window.angular);
