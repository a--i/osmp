/**
 * Created by Andrey Petkevich on 15.12.2014.
 */
'use strict';

(function (ng, undefined) {

  ng.module('rbt.dict.module')
    .controller('DictionariesCtrl',
    [
      '$scope',
      'rbt.dictAliases',
      'rbt.common.dictionarySrv',
      '$modal',
      '$log',
      function ($scope,
                aliases,
                dictionaries,
                $modal,
                $log) {

        $scope.dictionaries = [];
        $scope.items = [];
        $scope.selected = {
          isSelected: false,
          selectedDictionary: ''
        };

        ng.forEach(aliases, function (item) {
          if (ng.isDefined(item) && item !== '' && item !== null) {
            $scope.dictionaries.push({alias: item.alias, name: item.name});
          }
        });

        $scope.selectionChanged = function () {
          var s = $scope.selected;
          if (ng.isDefined(s.selectedDictionary) && s.selectedDictionary !== '' && s.selectedDictionary !== null) {
            s.isSelected = true;
          }

          $scope.items = dictionaries.query({alias: s.selectedDictionary.alias});
        };

        $scope.editItem = function (item, isDel) {

          if (!ng.isDefined(isDel)) {
            isDel = false;
          }

          var itemModel = {
            isNew: !ng.isDefined(item),
            isDel: isDel,
            header: '',
            okLabel: '',
            item: ng.copy(item)
          };
          itemModel.header = itemModel.isNew ? 'Создание записи' : 'Редактирование записи';
          itemModel.header = itemModel.isDel ? 'Удаление записи' : itemModel.header;
          itemModel.okLabel = itemModel.isNew ? 'Создать' : 'Сохранить';
          itemModel.okLabel = itemModel.isDel ? 'Удалить' : itemModel.okLabel;

          var modalInstance = $modal.open({
            templateUrl: 'app/Modules/dict/modalEditItem.html',
            controller: 'ModalEditDictItemCtrl',
            //size: size,
            resolve: {
              data: function () {
                return itemModel;
              }
            }
          });

          modalInstance.result.then(function (result) {

            if (result.isDel) {
              dictionaries.delete({alias: $scope.selected.selectedDictionary.alias, id: result.item.id},
                function (data) {
                  $log.info('[success]delete=' + ng.toJson(data));
                  $scope.items = $scope.items.filter(function(element){
                    return element.id !== result.item.id;
                  });
                },
                function (data) {
                  $log.error('[error]delete=' + ng.toJson(data));
                });
            } else {
              if (result.isNew) {
                result.item.alias = $scope.selected.selectedDictionary.alias;
                dictionaries.create({alias: $scope.selected.selectedDictionary.alias}, result.item,
                  function (data) {
                    $log.info('[success]create=' + ng.toJson(data) + ' string=' + data.toString());
                    result.item = data;
                    $scope.items.push(result.item);
                  },
                  function (data) {
                    $log.error('[error]create=' + ng.toJson(data));
                  });
              } else {
                dictionaries.update({alias: $scope.selected.selectedDictionary.alias, id: result.item.id}, result.item,
                  function (data) {
                    ng.forEach($scope.items, function(value, key) {
                      if (value.id === result.item.id){ $scope.items[key] = result.item; }
                    });

                    $log.info('[success]update=' + ng.toJson(data));
                  },
                  function (data) {
                    $log.error('[error]update=' + ng.toJson(data));
                  });
              }
            }
          }, function () {
            $log.info('modal cancel');
          });
        };

        $scope.createNew = function () {
          $scope.editItem();
        };

        $scope.deleteItem = function (item) {
          $scope.editItem(item, true);
        };

      }])
    .controller('ModalEditDictItemCtrl',
    [
      '$scope',
      '$modalInstance',
      'data',
      function ($scope, $modalInstance, data) {

        $scope.data = data;

        $scope.ok = function () {
          var result = {
            item: $scope.data.item,
            isNew: $scope.data.isNew,
            isDel: $scope.data.isDel
          };
          $modalInstance.close(result);
        };

        $scope.cancel = function () {
          $modalInstance.dismiss('cancel');
        };
      }]);

})(window.angular);
