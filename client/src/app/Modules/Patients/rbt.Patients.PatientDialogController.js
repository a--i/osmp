﻿(function(ng,undefined)
{
    ng.module("rbt.Patients")
        /*
        Модуль - Пациенты
        Контроллер - rbt.Patients.PatientsDialogController
        управляет формой диалога изменения пациента. 
        Зависит от сервиса данных rbt/Patients.Service
        контроллер загружает список мо из сервиса rbt.Patients.Service


        */
    .controller("rbt.Patients.PatientDialogController",  ["$scope","$modalInstance", "rbt.Patients.Service", "patient", function ($scope, $modalInstance,  service, patient) {

        /// Модель - Пациент (передается в контроллеер из сервиса модальных окон
        $scope.Patient = patient;

        /// список МО
        $scope.MOList = [];

        // обработчик нажатия на кнопку "сохранить"
        $scope.ok = function () {
            $modalInstance.close($scope.Patient);
        }

        // обработчик нажатия на кнопку "отменить"
        $scope.cancel = function () {
            $modalInstance.dismiss("cancel")
        }

        // загрузка списка мо
        var ret = service.query(function () {
            $scope.MOList = ret.mo;
        })




    }]);
})(window.angular)
