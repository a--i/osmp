﻿(function(ng,undefined)
{

    /// URI сервиса данных
    //var serviceUri = "http://med.crvmis.ru/svc/exp/example";
    var serviceUri = "data.json";

    ng.module("rbt.Patients")
    /*
    Модуль - Пациенты
    Сервис - rbt.Patients.Service 
    зависит от сервиса $resource модуля angular-resource.js (поддержка РЕСТ)
    сервис создает и настраивает ресурс РЕСТ
    */
	.factory("rbt.Patients.Service", ["rbt.DataManager.$resource", function (resource) {
		

		var ret = resource(serviceUri, {
			
			}, 
		{
		    query: {  method: 'GET' , resultCache:"L1"},
			
		});

		ret.Key = "PatientsResource";

		return ret;

	}])

})(window.angular)