﻿(function(ng,undefined)
{

    ng.module("rbt.Patients")
    /*
    Модуль - Пациенты
    Контролер - rbt.Patients.MOListController
    управление формой списка пациентов.
    контроллер загружает список мо из сервиса rbt.Patients.Service

    */
    .controller("rbt.Patients.MOListController", ["$scope", "rbt.Patients.Service", function ($scope, service) {

        // список пациентов
        $scope.MOList = [];

        // признак загрузки данных
        $scope.isLoaded = false;

        // обращение к сервису
        var ret = service.query(function () {
            $scope.MOList = ret.mo;
            $scope.isLoaded = true;
        }, function (e) { debugger; })

    }]);

})(window.angular)