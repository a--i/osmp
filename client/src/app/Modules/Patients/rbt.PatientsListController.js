﻿/// <reference path="../../../Scripts/angular.js" />
/// <reference path="../../../Scripts/angular-resource.js" />

(function (ng, undefined) {
    ng.module("rbt.Patients")
    /*
    Модуль - пациенты
    Контроллер - rbt.Patients.ListController
    управляет представлением списка пациентов
    зависит от rbt.Patients.Service откуда загружаются данные списка пациентов и спимка МО
    фильтрация данных списка осуществляется на клиенте.

    */
    .controller("rbt.Patients.ListController", ["$scope", "$modal", "rbt.Patients.Service", function ($scope, $modal, PatientsSvc) {
      
        // список пациентов
        $scope.Patients = [];

        // признак загрузки данных
        $scope.isLoaded = false;

        // список МО (не очень удачно реализован сервис списка. Необходимо раскодировать значения мед учреждения, что может отрицательно сказаться на производительности клиентской части. 
        // в рабочей системе лучше возвращать раскодированные значения, что позволит избезать лишних привязок в строках таблицы)
        $scope.MOList = [];

        // фильтр списка
        $scope.patfilter = null;

        // загрузка данных с сервиса
        function load()
        {
            var loaded = PatientsSvc.query(function () {
                var tst = ng.toJson(loaded);

                var pt = new PatientsSvc(ng.fromJson(tst));

                $scope.Patients = loaded.patients;
                $scope.MOList = loaded.mo;
                $scope.isLoaded = true;
            }, function (e) {
                debugger;
            })
        }

        // создание фильтра списка
        // удаляются пустые свойства и дата преобразуется в строку
        function createFilter(filter)
        {
            var ret = {};
            for( key in filter)
            {
                if (filter[key] != null)
                    ret[key] = filter[key];
            }
            if (ret.reg_date != null)
                ret.reg_date = ret.reg_date.toLocaleString().split(",")[0];

            return ret;
        }

        // обработсик двойного нажатия на строке
        // открывает модальный диалог изменения пациента
        $scope.DblClick = function(Patient)
        {
         
            var modalInstance = $modal.open(
                {
                    templateUrl: "App/Modules/Patients/PatientDialog.html",
                    controller: 'rbt.Patients.PatientDialogController',
                    backdrop:"static",
                    resolve: {
                        patient: function () {
                            return ng.copy(Patient);
                        }
                    }
                }
                );
            modalInstance.result.then(function (pat) {
                ng.copy(pat, Patient);
            }, function (dismiss) {
                //debugger;
            })
        }

        load();

        $scope.refresh = function () {
            load();
        }

        // подписка на событие поиск
        // создается фильтр списка
        $scope.$on("patients.search", function (event, filter) {
            $scope.patfilter = createFilter(filter);
        });

        // подписка на событие сброс фильтра
        // очищается фильтр списка
        $scope.$on("patients.clearfilter", function () { $scope.patfilter = null; })
    
    }]);

})(window.angular)