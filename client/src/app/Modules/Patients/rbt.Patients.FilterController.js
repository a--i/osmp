﻿(
function (ng, undefined) {

    /*
     Модуль rbt.Patients  - пациенты
     Контроллер - rbt.Patients.FilterController
     управление формой фильтра списка пациентов.
     контроллер загружает список мо из сервиса rbt.Patients.Service
     контролер рассылает события сброса фильтра и событие "поиск" аргментом которого является объект фильтр
     
     Взаимодействие между контролерами в этом примере реализовано по шаблону подписчик/получатель - как наиболее общий случай
     хотя, наверное, целесообразно внутри модуля использовать непосредственное взаимодействие контроллеров, а механизм подписчик/получатель 
     для межмодульного взаимодействия, оставляя модули слабо связными и поддерживая связи внутри модуля

     
     
    */
    ng.module("rbt.Patients")
    .controller("rbt.Patients.FilterController", ["$scope", "$rootScope", "rbt.Patients.Service", function ($scope, $root, service) {

        // признак загрузки данных
        $scope.isLoaded = false;

        /// список МО
        $scope.MOList = [];

        // признак открытия окна выбора даты см. комент ниже
        $scope.opened = false;

        function blancFilter()
        {
            return  {
                firstname: null,
                lastname: null,
                mo: null,
                reg_date:null
            };
        }
        
        /// кнопка открыть датапикера (целесообразно реализовать отдельную директиву для поддержания этого функционала)
        $scope.open = function (event)
        {
            event.preventDefault();
            event.stopPropagation();

            $scope.opened = true;
        }

        // очистка поисковой формы. рассылается уведомление о событии используемое для обновления списка
        $scope.Clear = function () {
            $scope.Filter = blancFilter();
            $root.$broadcast("patients.clearfilter")
        };

        // нажатие кнопки поиск рассылается уведомление содержащие заполненый фильтр
        $scope.Search = function () {
            $root.$broadcast("patients.search", $scope.Filter)
        };


        $scope.Clear();
        
        // запрос данных у сервиса 
        // все   было бы чуть проще при нормальной реализации рест сервиса возвращающей список объектов, а не все в одном
        var ret = service.query(function () {
            $scope.MOList = ret.mo;
            $scope.isLoaded = true;
        }, function (e) {
            debugger;
        });

    
    }
    ]
    );
}

)(window.angular)