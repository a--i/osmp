/**
 * Created by ppavlov on 08.12.2014.
 */
'use strict';
(function (ng, undefined) {


  ng.module('rbt.common')
    .factory('rbt.common.dictionarySrv', ['rbt.baseServicePath', '$resource', function (basePath, Resource) {

      var uri = basePath + 'directory/:alias/:id';
      return new Resource(
        uri,
        {
          alias: '@alias'
        },
        {
          query: {method: 'GET', isArray: true},
          create: { method: 'POST' },
          update: { method: 'PUT' },
          delete: { method: 'DELETE' }
        }
      );

    }])
    .factory('rbt.common.dictionaryCallsResultsSrv', ['rbt.baseServicePath', '$resource', function (basePath, Resource) {
      var uri = basePath + 'directory/ambulance/callsresults';
      return new Resource(uri);
    }])
    .factory('rbt.common.MkbTen',['rbt.baseServicePath','$resource', function(basePath,Resource){
      var uri=basePath+'directory/mkb-10/:id';
      return new Resource(uri, {id:'@id'});
    }])
  ;

})(window.angular);
