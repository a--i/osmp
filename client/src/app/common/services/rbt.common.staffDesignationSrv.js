/**
 * Created by ppavlov on 18.12.2014.
 */
'use strict';
(function (ng, undefined) {
  ng.module('rbt.common')
    .factory('rbt.common.staffDesignationSrv',['rbt.baseServicePath','$resource',function(basePath, $resource){
      var uri = basePath +'/staff/designations/:id';

      return $resource(uri, {id:'@id'});

    }]);

})(window.angular);
