/**
 * Created by ppavlov on 05.12.2014.
 */
'use strict';
(function (ng, undefined) {

  function Tab(tab) {
    if (tab) {
      var tb = ng.extend({}, Tab.prototype, tab);
      for (var key in tb) {
        this[key] = tb[key];
      }
    }
  }

  Tab.prototype = {
    id: 0,
    title: '',
    name: '',
    tpl: '',
    params: {}
  };

  function TabController(TabType, $filter) {
    var that = this;
    this.$filter = $filter;
    this.tabType = TabType;
    this.tabs = [];
    this.openTab = ng.bind(that, that.openTab);
    this.closeTab = ng.bind(that, that.closeTab);
    this.getParams = ng.bind(that, that.getParams);
    this.renameTab = ng.bind(that,that.renameTab);
  }


  TabController.prototype = {
    openTab: function (newTab) {
      if (!(newTab instanceof this.tabType)) {
        throw 'incorrect tab type';
      }
      var index = this.tabs.indexOf(newTab);
      var id = this.$filter('filter')(this.tabs, {id: newTab.id, name: newTab.name},true)[0];
      if (index === -1 && (!ng.isDefined(id) || id === null)) {
        this.tabs.push(newTab);
        newTab.active = true;
      } else {
        if (index !== -1 && (ng.isDefined(index) || index !== null)) {
          this.tabs[index].active = true;
        } else {
          id.active = true;
        }
      }
    },
    _reorganizeTabs: function (newActive) {
      var once = true;
      var newTabs = [];
      for (var key in this.tabs) {
        if (ng.isDefined(this.tabs[key])) {
          newTabs.push(this.tabs[key]);
          if (newActive && once) {
            this.tabs[key].active = true;
            once = false;
          }
        }
      }
      this.tabs = newTabs;
    },
    closeTab: function (tab) {
      var index = this.tabs.indexOf(tab);
      delete this.tabs[index];
      this._reorganizeTabs(true);
    },

    renameTab:function(oldTab, newTab){

      var current =  this.$filter('filter')(this.tabs, {id: oldTab.id, name: oldTab.name},true)[0];
      if (ng.isDefined(current)) {

         ng.extend(current, newTab);
        ng.noop();
      }

    },

    getParams: function () {
      return this.$filter('filter')(this.tabs, {active: true})[0].params;
    }

  };


  ng.module('rbt.common')
    .factory('rbt.common.tabType', function () {
      return Tab;
    })
    .factory('rbt.common.osmpTabService', ['rbt.common.tabType', '$filter', function (TabType, $filter) {
      return new TabController(TabType, $filter);
    }]);

})(window.angular);
