﻿'use strict';

(function (ng, undefined) {

  ng.module('main',
    [ 'ngMessages',
      'ui.bootstrap.datetimepicker',
      'ui.bootstrap',
      'multi-select',
      'rbt.common',
      'rbt.topnav',
      'rbt.navigation',
      'rbt.osmp.module',
      'rbt.dict.module',
      'rbt.mkbdict.module',
      'rbt.Controls'
    ]);

})(window.angular);
