/**
 * Created by ppavlov on 08.12.2014.
 */
'use strict';
(function (ng, moment, undefined) {

  ng.module('main')
    .value('rbt.baseServicePath', 'http://localhost:8080/')
    .value('rbt.dictAliases',
    {
      operators: {alias: '', name: ''},
      statuses: {alias: 'CallStatus', name: 'Статусы вызова'},
      places: {alias: 'CallLocation', name: 'Места вызова'},
      calltypes: {alias: 'CallPassType', name: 'CallPassType'},
      transfers: {alias: 'CallType', name: 'Типы вызова'},
      brigades: {alias: 'AmbulanceCallTeamNumber', name: 'Бригады'},
      autonums: {alias: 'DepartureCarNumber', name: 'Номера машин'},
      delayreasons: {alias: 'DepartureDelayReason', name: 'Причины задержки'},
      diagnosisKind: {alias: 'DiagnosisKind', name: 'тип диагноза'},
      efficacyStepComplication: {alias: 'EfficacyStepComplication', name: 'EfficacyStepComplication'},
      difficulty: {alias: 'complication', name: 'Осложнения'},
      departureResult: {alias: 'DepartureResult', name: 'Результаты выезда'}
    })
    .config(['datepickerPopupConfig', function (datepickerConfig) {
      datepickerConfig.datepickerPopup = 'dd.MM.yyyy';
      datepickerConfig.showButtonBar = false;
      datepickerConfig.closeText = 'Закрыть';
      datepickerConfig.clearText = 'Очистить';
      datepickerConfig.currentText = 'Сегодня';
    }])
    .factory('errorHttpResponseInterceptor', ['$q', '$log', function ($q, $log) {
      return {
        responseError: function (rejection) {
          $log.debug('Response Error ',rejection.status, ' ', rejection.statusText);
          return $q.reject(rejection);
        }
      };
    }])
    .config(['$httpProvider', function ($httpProvider) {
      $httpProvider.interceptors.push('errorHttpResponseInterceptor');
      $httpProvider.defaults.useXDomain = true;
      delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }])
    .config(function(){
      moment.locale('ru',{

        longDateFormat : {
          LT : 'HH:mm ',
          LTS : 'LT:ss ',
          L : 'DD.MM.YYYY',
          LL : 'D MMMM YYYY г.',
          LLL : 'D MMMM YYYY г., LT',
          LLLL : 'dddd, D MMMM YYYY г., LT'
        }
      });
    });


})(window.angular, window.moment);
