'use strict';

describe('dictionarySrv', function () {
  var dictionaryBase, dictionaryExtended, httpBackend, basePath;
  var respondArray = [{}, {}];
  beforeEach(module('main'));

  beforeEach(function () {
    module('rbt.common');
    inject(
      [
        'rbt.common.dictionarySrv',
        'rbt.common.dictionaryCallsResultsSrv',
        '$httpBackend',
        'rbt.baseServicePath',
        function ($dictionary, $dictionaryExtended, $httpBackend, $basePath) {
          dictionaryBase = $dictionary;
          dictionaryExtended = $dictionaryExtended;
          httpBackend = $httpBackend;
          basePath = $basePath;
        }]);
  });

  it('получаем базовый url', inject(function () {
    expect(basePath).toBeDefined();
  }));

  it('получаем стандартный справочник справочник', inject(function () {
    var alias = 'mockDict';
    httpBackend.expectGET(basePath + 'directory/'+alias+'/items')
      .respond(respondArray);

    var result = dictionaryBase.query({alias:alias});
    httpBackend.flush();
    expect(result.length).toBe(2);
  }));

  it('получаем справочник callsresults', inject(function () {
    httpBackend.expectGET(basePath + 'directory/ambulance/callsresults')
      .respond(respondArray);

    var result = dictionaryExtended.query();

    httpBackend.flush();
    expect(result.length).toBe(2);
  }));
});
