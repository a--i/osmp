'use strict';

describe('dictionarySrv', function () {
  var service, httpBackend, basePath, dataPrep;
  var respondArray = [{}, {}];
  beforeEach(module('main'));

  beforeEach(function () {
    module('rbt.osmp.module');
    inject(
      [
        'rbt.osmp.data',
        '$httpBackend',
        'rbt.baseServicePath',
        'rbt.osmp.dataPrepare',
        function ($dictionary, $httpBackend, $basePath, $dataPrep) {
          service = $dictionary;
          httpBackend = $httpBackend;
          basePath = $basePath;
          dataPrep = $dataPrep;
        }]);
  });

  it('receive data from rbt.osmp.module (rbt.osmp.data)', inject(function () {
    httpBackend.expectGET(basePath + 'ambulance/calls')
      .respond(respondArray);

    var result = service.query();

    httpBackend.flush();
    expect(result.length).toBe(2);
  }));

  //it('receive data from rbt.osmp.module (rbt.osmp.dataPrepare)', inject(function () {
  //  httpBackend.expectGET(basePath + 'ambulance/calls')
  //    .respond(respondArray);
  //
  //  var result = dataPrep.get();
  //
  //  httpBackend.flush();
  //  expect(result.rows.length).toBe(2);
  //}));
});
