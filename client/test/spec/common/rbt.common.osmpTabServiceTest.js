/**
 * Created by ppavlov on 08.12.2014.
 */
'use strict';

describe('tabService', function () {
  var tab1 = {
    id: 1,
    name: '1',
    title: '1 1',
    tpl: 'tml',
    params: {name: 'name'}
  };
  var tab2 = {
    id: 2,
    name: '2',
    title: '2 2',
    tpl: 'tml2'
  };


  var service;
  var TType;
  beforeEach(function () {
    module('rbt.common');
    inject(['rbt.common.osmpTabService', 'rbt.common.tabType', function ($service, $type) {
      service = $service;
      TType = $type;
    }]);
  });

  it('сервисы экспортируются', function () {
    expect(service).not.toBeUndefined();
    expect(TType).not.toBeUndefined();
  });

  it('закладки добавляются', function () {
    expect(angular.isArray(service.tabs)).toBe(true);
    expect(service.tabs.length).toEqual(0);

    service.openTab(new TType(tab1));
    service.openTab(new TType(tab2));


    expect(service.tabs.length).toEqual(2);

  });

  it('закладки уникальны по Id', function () {
    expect(service.tabs.length).toEqual(0);

    service.openTab(new TType(tab1));
    service.openTab(new TType(tab1));

    expect(service.tabs.length).toEqual(1);
  });

  it('getParam выдает параметры активной закладки', function () {
    expect(service.tabs.length).toEqual(0);

    service.openTab(new TType(tab1));
    service.openTab(new TType(tab2));
    service.openTab(new TType(tab1));

    expect(service.tabs.length).toEqual(2);

    expect(service.getParams()).toEqual(tab1.params);

  });

  it('closeTab закрывает закладку', function () {

    service.openTab(new TType(tab1));
    service.openTab(new TType(tab2));

    expect(service.tabs.length).toEqual(2);

    service.closeTab(new TType(tab1));

    //var cnt = 0;
    //for (var key in service.tabs) {
    //  cnt++;
    //}
    expect(service.tabs.length).toEqual(1);

  });

  it('если удаляется активная вкладка активной становится первая', function () {

    var ttab2 = new TType(tab2);
    service.openTab(new TType(tab1));
    service.openTab(ttab2);
    service.openTab(new TType(tab1));

    service.closeTab(new TType(tab1));

    expect(ttab2.active).toEqual(true);

  });


});
